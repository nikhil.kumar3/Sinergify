package GZProducts.Communities.CommunitiesUtilities.POM;

import CommonUtilities.GZBrowser.GZBrowser;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SFDCLoginPage {

    public SFDCLoginPage(){
        PageFactory.initElements(GZBrowser.getDriver(), this);
    }

    @FindBy(id = "username")
    @CacheLookup
    WebElement txtUserName;

    @FindBy(id = "password")
    @CacheLookup
    WebElement txtpassword;

    @FindBy(id = "Login")
    @CacheLookup
    WebElement btnlogin;

    @FindBy(id = "error")
    @CacheLookup
    WebElement loginerror;

    public void setUserName(String uname) { txtUserName.sendKeys(uname); }

    public void setpassword(String pwd){txtpassword.sendKeys(pwd);}

    public void clickSubmit(){btnlogin.click();}

    public String getLoginError(){return loginerror.getText();}
}
