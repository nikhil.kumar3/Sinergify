package GZProducts.Communities.CommunitiesUtilities.POM;

import CommonUtilities.GZBrowser.GZBrowser;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.Map;

public class SFDCContact {
    public SFDCContact(){
        PageFactory.initElements(GZBrowser.getDriver(), this);
    }

    //Webelements
    @FindBy(xpath = "//*[@id='phSearchInput']")
    private WebElement searchBox;
    @FindBy(xpath = "//*[@id='phSearchButton']")
    private WebElement searchButton;
    //Contact Name element
    public WebElement contactName(String Name){
        return GZBrowser.getDriver().findElement(By.xpath("//td/a[text()='Edit']/following::th/a[text()='" +Name+ "']"));
    }


    //action methods
    public void setSearchforContact(String UserName){searchBox.sendKeys(UserName);}
    public void clickOnSerchButton(){searchButton.click();}
    public void clickOncontact(String Name){contactName(Name).click();}

}
