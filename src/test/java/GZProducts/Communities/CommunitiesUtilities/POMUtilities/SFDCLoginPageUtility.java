package GZProducts.Communities.CommunitiesUtilities.POMUtilities;

import CommonUtilities.GZLogger.GZLogger;
import GZProducts.Sinergify.SinergifyUtilities.POM.SFDCLoginPage;
import com.sun.org.apache.xpath.internal.operations.Bool;

public class SFDCLoginPageUtility {
    SFDCLoginPage lp = null;
    public boolean LogIn(String username, String password){
        lp = new SFDCLoginPage();
        Boolean successLogin=false;
        try{
            lp.setUserName(username);
            lp.setpassword(password);
            successLogin= lp.clickSubmit();
            Thread.sleep(2000);
        }catch (Exception ex){
            GZLogger.ErrorLog(ex, ex.getMessage());
        }
        return successLogin;
    }

    public String InvalidLogIn(String username, String password){
        lp = new SFDCLoginPage();
        try{
            LogIn(username, password);
        }catch (Exception ex){
            GZLogger.ErrorLog(ex, ex.getMessage());
        }
        return lp.getLoginError();
    }
}
