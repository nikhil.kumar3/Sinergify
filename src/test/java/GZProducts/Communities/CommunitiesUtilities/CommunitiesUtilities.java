package GZProducts.Communities.CommunitiesUtilities;

import CommonUtilities.DataProvider.ReadConfig;
import CommonUtilities.GZBrowser.BrowserType;
import CommonUtilities.GZBrowser.GZBrowser;
import CommonUtilities.GZLogger.GZLogger;
import GZProducts.Communities.CommunitiesUtilities.POMUtilities.SFDCContactUtility;
import GZProducts.Sinergify.SinergifyUtilities.POMUtilities.*;

import java.util.Map;

public class CommunitiesUtilities {
    /**
     * Utilities
     */
    GZBrowser gb = new GZBrowser();
    ReadConfig config = new ReadConfig();
    SFDCLoginPageUtility lputility = new SFDCLoginPageUtility();
    SFDCContactUtility comutility= new SFDCContactUtility();

    /**
     * Common Methods
     */
    public void LaunchUrl(String browser) {
        try {
            String baseURl = config.getApplicationPath();
            gb.Launch(baseURl, BrowserType.valueOf(browser));
            GZLogger.ActivityLog("Browser Launched");
        } catch (Exception ex) {
            GZLogger.ErrorLog(ex, "Not able to launch browser");
        }
    }

    public void CloseBrowser() {
        try {
            gb.Close();
            GZLogger.ActivityLog("Browser closed successfully");
        } catch (Exception ex) {
            GZLogger.ErrorLog(ex, "Not able to dispose browser");
        }
    }

    public void LogInIntoCommunity() {
        try {
            String username = config.getUsername();
            String password = config.getPassword();
            lputility.LogIn(username, password);
            GZLogger.ActivityLog("Logged into the application");
        } catch (Exception ex) {
            GZLogger.ErrorLog(ex, "Not able to launch browser");
        }
    }

    public String InvalidLogInIntoSinergify(Map<String, String> map) {
        String error = "";
        try {
            String username = map.get("username");
            String password = map.get("password");
            error = lputility.InvalidLogIn(username, password);
            GZLogger.ActivityLog("Logged into the application");
        } catch (Exception ex) {
            error = "";
            GZLogger.ErrorLog(ex, "Not able to launch browser");
        }
        return error;
    }

    public void searchAndOPenContact(Map<String,String> map){
        try{
            GZLogger.ActivityLog("Value entered in search");
            comutility.enterContactandserach(map.get("Username"));
            GZLogger.ActivityLog("Value entered in search");
            GZLogger.ActivityLog("Click on search button");
            comutility.openSearchedContact(map.get("Username"));
            GZLogger.ActivityLog(" contact opened suucessfully");
        }catch (Exception ex) {
            GZLogger.ErrorLog(ex, "Not able to search contact");
        }
    }
/*
    //xyz()
    {
        searchAndOPenContact()
        ;
    }*/
}
