package GZProducts.Communities.UITest.TestBase;

import CommonUtilities.DataProvider.DataProviderUtility;
import GZProducts.Communities.CommunitiesUtilities.CommunitiesUtilities;
import GZProducts.Sinergify.SinergifyUtilities.SinergifyUtilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import java.lang.reflect.Method;
import java.util.Map;


public class TestBase {

    public static CommunitiesUtilities commutil = null;
    public static DataProviderUtility dputil = null;
    public static Map<String, String> map = null;
    public static Map<String ,Long> mapLong=null;

    @BeforeTest
    public void setup() {
        commutil = new CommunitiesUtilities();
        dputil = new DataProviderUtility();
    }

    @BeforeMethod
    protected void setupMethod(Method method) {
        String testName = method.getName();
        map = dputil.readMap(testName);
    }

    @AfterMethod
    protected void methodTearDown() {
        commutil.CloseBrowser();
        map = null;
    }

    @AfterTest
    public void tearDown(){
    }

}
