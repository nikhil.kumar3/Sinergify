package GZProducts.Communities.UITest.TestScripts;

import CommonUtilities.GZBrowser.GZBrowser;
import GZProducts.Communities.UITest.TestBase.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SFDCLoginPageTest extends TestBase {

    //@Parameters("browser")
    @Test
    public void validSFDCLogin_test() {
        commutil.LaunchUrl(map.get("browser"));
        commutil.LogInIntoCommunity();
        Assert.assertTrue(GZBrowser.getDriver().getTitle().contains("Salesforce"));
    }

//    @Test
//    public void invalidSFDCLogin_test() {
//        commutil.LaunchUrl(map.get("browser"));
//        String error = commutil.InvalidLogInIntoSinergify(map);
//        Assert.assertTrue(error.contains("username and password"));
//    }
}
