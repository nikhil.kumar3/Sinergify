package GZProducts.Sinergify.UITest.TestScripts;

import GZProducts.Sinergify.SinergifyUtilities.POMUtilities.AdminSettingsPageUtility;
import GZProducts.Sinergify.SinergifyUtilities.POMUtilities.FieldConfigPageUtility;
import GZProducts.Sinergify.UITest.TestBase.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;

public class FieldConfigPageTest extends TestBase {
        FieldConfigPageUtility fcpUtil = new FieldConfigPageUtility();

        /*Verify if admin user clicks on the Next button on Projects screen and navigates to the
                Field Configuration - Project Mapping screen*/
        @Test(priority = 0)
        public void verifyIfFieldConfigPageIsAccessible_Test(){
                boolean expectedOutput;
                sgfutil.LaunchUrl(map.get("browser"));
                expectedOutput= sgfutil.LogInIntoSinergify();
                Assert.assertTrue(expectedOutput);
                expectedOutput= sgfutil.addBasicAuthJiraCloud(map);
                Assert.assertTrue(expectedOutput);
                expectedOutput=fcpUtil.checkIfFieldConfigPageIsVisible();
                Assert.assertTrue(expectedOutput);
        }

        /*Verify if admin user see Choose a SFDC Object dropdown on Project mapping screen*/
        /*Verify if admin user see Choose a Jira Project dropdown on Project mapping screen*/

        @Test(dependsOnMethods = "verifyIfFieldConfigPageIsAccessible_Test")
        public void verifyIfLabelsDropdownsOnFieldConfigPageIsVisible_Test(){
                boolean expectedOutput;
                sgfutil.LaunchUrl(map.get("browser"));
                expectedOutput= sgfutil.LogInIntoSinergify();
                Assert.assertTrue(expectedOutput);
                expectedOutput= sgfutil.addBasicAuthJiraCloud(map);
                Assert.assertTrue(expectedOutput);
                expectedOutput=fcpUtil.checkIfFieldConfigPageIsVisible();
                Assert.assertTrue(expectedOutput);
                expectedOutput=fcpUtil.checkIfLabelsOnFieldConfigPageIsVisible();
                Assert.assertTrue(expectedOutput);
                expectedOutput=fcpUtil.checkIfDropdownOnFieldConfigPageIsVisible();
                Assert.assertTrue(expectedOutput);
        }

//        @Test
        /*Out of scope TC as of now*/
        public void verifyIfLajbelsDropdownsOnFieldConfigPageIsVisible_Test(){
                boolean expectedOutput;
                sgfutil.LaunchUrl(map.get("browser"));
                expectedOutput= sgfutil.LogInIntoSinergify();
                Assert.assertTrue(expectedOutput);
                expectedOutput= sgfutil.addBasicAuthJiraCloud(map);
                Assert.assertTrue(expectedOutput);
                expectedOutput=fcpUtil.checkIfFieldConfigPageIsVisible();
                Assert.assertTrue(expectedOutput);
                expectedOutput=fcpUtil.checkIfLabelsOnFieldConfigPageIsVisible();
                Assert.assertTrue(expectedOutput);
                expectedOutput=fcpUtil.checkIfDropdownOnFieldConfigPageIsVisible();
                Assert.assertTrue(expectedOutput);
        }





        @Test
        public void reqFieldsConfiguration_Test(){
                boolean expectedOutput;
                sgfutil.LaunchUrl(map.get("browser"));
                expectedOutput= sgfutil.LogInIntoSinergify();
                Assert.assertTrue(expectedOutput);
                expectedOutput= sgfutil.addBasicAuthJiraCloud(map);
                Assert.assertTrue(expectedOutput);
                expectedOutput=fcpUtil.checkIfFieldConfigPageIsVisible();
                Assert.assertTrue(expectedOutput);
                expectedOutput= fcpUtil.setProjectMapping();
                Assert.assertTrue(expectedOutput);
        }
        
//        @Test
        /*This is obslate now*/
        public void optFieldConfiguration_Test(){
                sgfutil.LaunchUrl(map.get("browser"));
                sgfutil.LogInIntoSinergify();
                sgfutil.addJiraInstance(map);
                sgfutil.saveProject();
                boolean result = sgfutil.setOptField(map);
                Assert.assertTrue(result);
        }

        @Test
        public void verifyDefaultValues_Test(){
                boolean expectedOutput;
                sgfutil.LaunchUrl(map.get("browser"));
                expectedOutput= sgfutil.LogInIntoSinergify();
                Assert.assertTrue(expectedOutput);
                expectedOutput= sgfutil.addBasicAuthJiraCloud(map);
                Assert.assertTrue(expectedOutput);
                expectedOutput=fcpUtil.checkIfFieldConfigPageIsVisible();
                Assert.assertTrue(expectedOutput);
                expectedOutput= fcpUtil.setProjectMapping();
                Assert.assertTrue(expectedOutput);
                expectedOutput=sgfutil.saveDefaultValues();
                Assert.assertTrue(expectedOutput);
        }



        @Test
        public void verifyIfReportingPageIsAccessible_Test(){
                boolean expectedOutput;
                sgfutil.LaunchUrl(map.get("browser"));
                expectedOutput= sgfutil.LogInIntoSinergify();
                Assert.assertTrue(expectedOutput);
                expectedOutput= sgfutil.addBasicAuthJiraCloud(map);
                Assert.assertTrue(expectedOutput);
                expectedOutput=fcpUtil.checkIfReportingPageIsVisible();
                Assert.assertTrue(expectedOutput);
        }

                @Test
        public void verifyIfSaveReportingPageIsSuccess_Test(){
                boolean expectedOutput;
                sgfutil.LaunchUrl(map.get("browser"));
                expectedOutput= sgfutil.LogInIntoSinergify();
                Assert.assertTrue(expectedOutput);
                expectedOutput= sgfutil.addBasicAuthJiraCloud(map);
                Assert.assertTrue(expectedOutput);
                expectedOutput=fcpUtil.checkIfReportingPageIsVisible();
                Assert.assertTrue(expectedOutput);
                expectedOutput=fcpUtil.saveReportingSyncCheckbox();
                Assert.assertTrue(expectedOutput);
        }

        @Test
        public void verifyIfSyncConfigurationPageIsAccessible_Test(){
                boolean expectedOutput;
                sgfutil.LaunchUrl(map.get("browser"));
                expectedOutput= sgfutil.LogInIntoSinergify();
                Assert.assertTrue(expectedOutput);
                expectedOutput= sgfutil.addBasicAuthJiraCloud(map);
                Assert.assertTrue(expectedOutput);
                expectedOutput=fcpUtil.checkIfSyncConfigurationPageIsVisible();
                Assert.assertTrue(expectedOutput);
        }

        @Test
        public void verifyIfSyncConfigurationIsSaved_Test(){
                boolean expectedOutput;
                sgfutil.LaunchUrl(map.get("browser"));
                expectedOutput= sgfutil.LogInIntoSinergify();
                Assert.assertTrue(expectedOutput);
                expectedOutput= sgfutil.addBasicAuthJiraCloud(map);
                Assert.assertTrue(expectedOutput);
                expectedOutput=fcpUtil.checkIfSyncConfigurationPageIsVisible();
                Assert.assertTrue(expectedOutput);
                expectedOutput=fcpUtil.saveSyncConfiguration();
                Assert.assertTrue(expectedOutput);
        }



}
