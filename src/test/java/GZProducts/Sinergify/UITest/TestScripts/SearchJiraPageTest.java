package GZProducts.Sinergify.UITest.TestScripts;

import GZProducts.Sinergify.SinergifyUtilities.SinergifyUtilities;
import GZProducts.Sinergify.UITest.TestBase.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SearchJiraPageTest extends TestBase {

    SinergifyUtilities sgfutil=new SinergifyUtilities();

    /*PRE-CONDITIONS TO RUN THE TESTS
    1. Sinergify app must be installed in the test environment.
    2. Permission and webhook should be configured properly as per the documentation.
    3. Jira instance and Project & Field mapping should be done as per the requirement.
    4. Required & Optional fields should be mapped.
    */

//    @Test(priority = 0)
    public void verifySearchJira_Test(){
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput= sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchMenuItem(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput=sgfutil.searchOpenCase(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput=sgfutil.searchJira(map.get("JiraKey"));
        Assert.assertTrue(expectedOutput);
    }

//    @Test
    public void verifyIfSearchJiraPageIsOpen_Test(){
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput= sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchMenuItem(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput=sgfutil.searchOpenCase(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput=sgfutil.openSearchJira();
        Assert.assertTrue(expectedOutput);
    }

    @Test
    public void verifyIfProjectIsVisibleOnSearchJiraPage_Test(){
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput= sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchMenuItem(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput=sgfutil.searchOpenCase(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput=sgfutil.lookForProject("Bug Manager");
        Assert.assertTrue(expectedOutput);
    }

//    @Test
//    public void verifySearchJiraPageUI_Test(){
//
//    }
//
//    @Test
//    public void verifyBreadcrumbOnSearchJira_Test(){
//
//    }
//
//    @Test
//    public void verifyMenuBarSearchJira_Test(){
//
//    }
//    @Test
    public void verifyJiraInstanceOnSearchJira_Test(){
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput= sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchMenuItem(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput=sgfutil.searchOpenCase(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput=sgfutil.openSearchJiraAndVerifyInstance(map.get("jiraInstanceName"));
        Assert.assertTrue(expectedOutput);
    }

    @Test
    public void verifyIfJiraIssueIsCloned_Test(){
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput = sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchMenuItem(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput=sgfutil.searchOpenCase(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput=sgfutil.searchJira(map.get("JiraKey"));
        Assert.assertTrue(expectedOutput);
        expectedOutput=sgfutil.cloneJiraIssue();
        Assert.assertTrue(expectedOutput);
    }


}
