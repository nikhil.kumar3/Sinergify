package GZProducts.Sinergify.UITest.TestScripts;

import GZProducts.Sinergify.SinergifyUtilities.POMUtilities.RulesetPageUtility;
import GZProducts.Sinergify.SinergifyUtilities.POMUtilities.ValidationPageUtility;
import GZProducts.Sinergify.UITest.TestBase.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;

public class RulesetPageTest extends TestBase {

    RulesetPageUtility rulesetUtil= new RulesetPageUtility();

    @Test(priority = 0)
    public void verifyIfRulesetPageIsAccessible_Test(){
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput= sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput= sgfutil.addBasicAuthJiraCloud(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput=rulesetUtil.ruleSetVisible();
       Assert.assertTrue(expectedOutput);
    }


    @Test(dependsOnMethods = "verifyIfRulesetPageIsAccessible_Test")
    public void verifyIfAddNewRuleFormIsOpen_Test(){
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput= sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput= sgfutil.addBasicAuthJiraCloud(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput=rulesetUtil.openAddRulesetForm();
        Assert.assertTrue(expectedOutput);
    }

    @Test(dependsOnMethods = "verifyIfRulesetPageIsAccessible_Test")
    public void verifyIfRuleIsCreated_Test(){
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput= sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput= sgfutil.addBasicAuthJiraCloud(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput=rulesetUtil.addNewRuleset();
        Assert.assertTrue(expectedOutput);

    }

    @Test(dependsOnMethods = "verifyIfRuleIsCreated_Test")
    public void verifyIfRuleIsEdited_Test(){
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput= sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput= sgfutil.addBasicAuthJiraCloud(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput=rulesetUtil.editRuleset();
        Assert.assertTrue(expectedOutput);
    }

    @Test(dependsOnMethods = "verifyIfRuleIsCreated_Test")
    public void verifyIfRuleIsDeleted_Test(){
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput= sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput= sgfutil.addBasicAuthJiraCloud(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput=rulesetUtil.deleteRuleset();
        Assert.assertTrue(expectedOutput);
    }

}
