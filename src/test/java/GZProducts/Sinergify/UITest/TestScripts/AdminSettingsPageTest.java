package GZProducts.Sinergify.UITest.TestScripts;

import CommonUtilities.GZBrowser.GZBrowser;
import GZProducts.Sinergify.SinergifyUtilities.POMUtilities.AdminSettingsPageUtility;
import GZProducts.Sinergify.UITest.TestBase.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.Map;

public class AdminSettingsPageTest  extends TestBase {

    /* Objects of classes */
    AdminSettingsPageUtility aspUtil = new AdminSettingsPageUtility();

    /*Verify if the admin user is able to search the Sinergify app in the SF org*/

    @Test(groups = {"sanity"})
    public void appInstalled_Test(){
        boolean output;
        sgfutil.LaunchUrl(map.get("browser"));
        output= sgfutil.LogInIntoSinergify();
        Assert.assertTrue(output);
        output = sgfutil.searchAppStore(map);
        Assert.assertTrue(output);
    }

    /*Verify if admin user is able to navigate to the Admin setting page of Sinergify*/

    @Test(dependsOnMethods = {"appInstalled_Test"},groups = {"sanity"})
    public void verifyAdminSettingNavigation_Test(){
        sgfutil.LaunchUrl(map.get("browser"));
        boolean output= sgfutil.LogInIntoSinergify();
        Assert.assertTrue(output);
        boolean result = sgfutil.searchAppStore(map);
        Assert.assertTrue(result);
        sgfutil.navigateAdminSetting();
        Assert.assertTrue(GZBrowser.getDriver().getCurrentUrl().contains("Admin_Setting"));
    }

    /*Verify if admin user see all options (Instances, Configuration settings, Projects,
        Field Configuration,Validation,Ruleset) in the side menu*/

    @Test(dependsOnMethods = {"verifyAdminSettingNavigation_Test"},groups = {"sanity"})
    public void verifyOptionsInSideMenu_Test() {
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput= sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchAppStore(map);
        Assert.assertTrue(expectedOutput);
        sgfutil.navigateAdminSetting();
        Assert.assertTrue(GZBrowser.getDriver().getCurrentUrl().contains("Admin_Setting"));
        expectedOutput = aspUtil.isSideOptionsVisible();
        Assert.assertTrue(expectedOutput);
    }

    /*Verify if admin user see the Logo of Sinergify on the top header of the webpage*/

    @Test(dependsOnMethods = {"verifyOptionsInSideMenu_Test"},groups = "sanity")
    public void verifySinergifyLogo_Test(){
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput= sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchAppStore(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput=aspUtil.checkSinergifyLogo();
        Assert.assertTrue(expectedOutput);
    }

    /*Verify if admin user see the instace change icon*/

    @Test(dependsOnMethods = {"verifySinergifyLogo_Test"},groups = {"sanity"})
    public void verifyChangeInstanceIconVisibility_Test(){
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput= sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchAppStore(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput=aspUtil.checkSinergifyLogo();
        Assert.assertTrue(expectedOutput);
        sgfutil.navigateAdminSetting();
        Assert.assertTrue(GZBrowser.getDriver().getCurrentUrl().contains("Admin_Setting"));
        expectedOutput=aspUtil.checkInstanceChangeIcon();
        Assert.assertTrue(expectedOutput);
    }

    /*Verify if admin user see the language change icon (a flag) on the top right corner of
        the webpage*/

    @Test(dependsOnMethods = {"verifyChangeInstanceIconVisibility_Test"},groups = {"sanity"})
    public void verifyChangeLanguageIconVisibility_Test() {
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput = sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchAppStore(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = aspUtil.checkSinergifyLogo();
        Assert.assertTrue(expectedOutput);
        sgfutil.navigateAdminSetting();
        Assert.assertTrue(GZBrowser.getDriver().getCurrentUrl().contains("Admin_Setting"));
        expectedOutput = aspUtil.checkInstanceChangeIcon();
        Assert.assertTrue(expectedOutput);
        expectedOutput = aspUtil.checkLanguageChangeIcon();
        Assert.assertTrue(expectedOutput);
    }

    /*Verify if admin user see the 5 tiles to add new Jira instance*/

    @Test(dependsOnMethods = {"verifyChangeLanguageIconVisibility_Test"})
    public void verifyJiraTiles_Test(){
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput= sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchAppStore(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput=aspUtil.checkSinergifyLogo();
        Assert.assertTrue(expectedOutput);
        sgfutil.navigateAdminSetting();
        Assert.assertTrue(GZBrowser.getDriver().getCurrentUrl().contains("Admin_Setting"));
        expectedOutput=aspUtil.checkInstanceChangeIcon();
        Assert.assertTrue(expectedOutput);
        expectedOutput=aspUtil.checkLanguageChangeIcon();
        Assert.assertTrue(expectedOutput);
        expectedOutput=aspUtil.checkJiraTiles();
        Assert.assertTrue(expectedOutput);
    }

    /*Verify if admin user clicks on first link to Add New Instance*/
    /*Verify if admin user navigates successfully to Basic authentication tab*/

    @Test()
    public void verifyBasicAuthClickable_Test(){
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput= sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchAppStore(map);
        Assert.assertTrue(expectedOutput);
        sgfutil.navigateAdminSetting();
        Assert.assertTrue(GZBrowser.getDriver().getCurrentUrl().contains("Admin_Setting"));
        aspUtil.clickAddInstanceOption();
        expectedOutput=aspUtil.checkBasicAuthTabClickable();
        Assert.assertTrue(expectedOutput);
    }

    /*Verify if admin user navigates successfully to OAuth authentication tab*/

    @Test()
    public void verifyOAuthClickable_Test(){
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput= sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchAppStore(map);
        Assert.assertTrue(expectedOutput);
        sgfutil.navigateAdminSetting();
        Assert.assertTrue(GZBrowser.getDriver().getCurrentUrl().contains("Admin_Setting"));
        aspUtil.clickAddInstanceOption();
        expectedOutput=aspUtil.isOauthTabVisible();
        Assert.assertTrue(expectedOutput);
    }

    /*Verfiy if admin user is able to add succesfully a Jira cloud with Basic auth*/
    /*Verfiy if admin user is able to add a Jira server with Basic auth succesfully */

    @Test(dependsOnMethods = {"verifyAdminSettingNavigation_Test"})
    public void verifyBasicAuthWithJiraCloud_Test(){
        sgfutil.LaunchUrl(map.get("browser"));
        Assert.assertTrue(sgfutil.LogInIntoSinergify());
        boolean result= sgfutil.addBasicAuthJiraCloud(map);
        Assert.assertTrue(result);
    }

    /*Verfiy if admin user is able to add a Jira cloud with OAuth succesfully*/
    /*Verfiy if admin user is able to add succesfully a Jira server with OAuth*/

    @Test(dependsOnMethods = {"verifyAdminSettingNavigation_Test"}, priority = 0)
    public void verifyOAuthWithJiraCloud_Test(){
        sgfutil.LaunchUrl(map.get("browser"));
        sgfutil.LogInIntoSinergify();
        boolean result= sgfutil.addOauthJiraCloud(map);
        Assert.assertTrue(result);
    }

    /*Verify if admin user is able to click on Webhook help? link on Basic auth screen*/

    @Test(dependsOnMethods = {"verifyAdminSettingNavigation_Test"})
    public void verifyWebhookHelpLink_Test(){
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput= sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchAppStore(map);
        Assert.assertTrue(expectedOutput);
        sgfutil.navigateAdminSetting();
        Assert.assertTrue(GZBrowser.getDriver().getCurrentUrl().contains("Admin_Setting"));
        aspUtil.clickAddInstanceOption();
        expectedOutput=aspUtil.isWebhookHelpClickable();
        Assert.assertTrue(expectedOutput);
        expectedOutput=aspUtil.isWebhookHelpClosed();
        Assert.assertTrue(expectedOutput);
    }

    /*Verify if admin user is able to click on Webhook help? link on Oauth screen*/

    @Test()
    public void verifyWebhookHelpLinkAtOauth_Test() {
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput= sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchAppStore(map);
        Assert.assertTrue(expectedOutput);
        sgfutil.navigateAdminSetting();
        Assert.assertTrue(GZBrowser.getDriver().getCurrentUrl().contains("Admin_Setting"));
        aspUtil.clickAddInstanceOption();
        expectedOutput=sgfutil.openOauthTab();
        Assert.assertTrue(expectedOutput);
        expectedOutput=aspUtil.isWebhookHelpClickable();
        Assert.assertTrue(expectedOutput);
        expectedOutput=aspUtil.isWebhookHelpClosed();
        Assert.assertTrue(expectedOutput);
    }

    /*Verify if admin user successfully copy the webhook URL*/

    @Test()
    public void verifyWebhookCopy_Test(){
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput= sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchAppStore(map);
        Assert.assertTrue(expectedOutput);
        sgfutil.navigateAdminSetting();
        Assert.assertTrue(GZBrowser.getDriver().getCurrentUrl().contains("Admin_Setting"));
        aspUtil.clickAddInstanceOption();
        expectedOutput=sgfutil.openOauthTab();
        Assert.assertTrue(expectedOutput);
        expectedOutput=aspUtil.isWebhookHelpClickable();
        Assert.assertTrue(expectedOutput);
        aspUtil.clickSelectSite();
        expectedOutput=aspUtil.isWebhookHelpClosed();
        Assert.assertTrue(expectedOutput);
    }

    /*Verify if admin user see the added Jira instance on the Instance screen
        with the same Jira name, Status and Jira URL*/
    /*Verify if admin user clicks on Configuration settings and see all 12 options on the screen*/

    @Test(dependsOnMethods = {"verifyAdminSettingNavigation_Test"})
    public void verifyOptionsClickableInSideMenu_Test(){
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput= sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchAppStore(map);
        Assert.assertTrue(expectedOutput);
        sgfutil.navigateAdminSetting();
        Assert.assertTrue(GZBrowser.getDriver().getCurrentUrl().contains("Admin_Setting"));
        expectedOutput=aspUtil.checkOptionsClickableInSideMenu();
        Assert.assertTrue(!expectedOutput);
    }

    /*Verify if admin user enables all toggle buttons on Configuration settings screen and save it successfully*/

    @Test(dependsOnMethods = {"verifyOptionsClickableInSideMenu_Test"})
    public void verifyAllOptionsOnConfigurationSettings_Test() throws InterruptedException {
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput= sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput= sgfutil.addBasicAuthJiraCloud(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput=aspUtil.elementsOnConfigSettings();
        Assert.assertTrue(expectedOutput);
        Assert.assertTrue(aspUtil.setAllToggleEnable());
    }

    /*Verify if admin user enters Comment qualifier and Attachment qualifier on Configuration settings
        screen and save it successfully*/

    @Test()
    public void verifyQualifierOnConfigurationSettings_Test() {
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput= sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput= sgfutil.addBasicAuthJiraCloud(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput=aspUtil.elementsOnConfigSettings();
        Assert.assertTrue(expectedOutput);
        Assert.assertTrue(aspUtil.enterQualifiers(map));
    }


//        @Test(description = "User journey")
    public void basicAuthAdminSettings_Test(){
        sgfutil.LaunchUrl(map.get("browser"));
        sgfutil.LogInIntoSinergify();
        boolean result= sgfutil.addJiraInstance(map);
        //Assertion need to be revisited as there is no active assertion point. so Verifying the error messages for validation.
        Assert.assertTrue(result,"Happy path with Basic authentication was not saved successfully");
    }
//    @Test(testName = "SIG-4")
    public void oAuthAdminSettings_Test() {
        sgfutil.LaunchUrl(map.get("browser"));
        sgfutil.LogInIntoSinergify();
        //Assertion need to be revisited as there is no active assertion point. so Verifying the error messages for validation.
        //Assert.assertTrue(result);
    }

}