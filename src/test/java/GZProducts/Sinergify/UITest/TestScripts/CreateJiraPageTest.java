package GZProducts.Sinergify.UITest.TestScripts;
import GZProducts.Communities.UITest.TestBase.TestBase;
import GZProducts.Sinergify.SinergifyUtilities.POMUtilities.CreateJiraPageUtility;
import GZProducts.Sinergify.SinergifyUtilities.SinergifyUtilities;
import org.testng.Assert;
import org.testng.annotations.Test;
import static GZProducts.Sinergify.UITest.TestBase.TestBase.*;

public class CreateJiraPageTest extends TestBase {

    /*Creating Object of SinergifyUtilities class*/

    SinergifyUtilities sgfutil = new SinergifyUtilities();

    /*PRE-CONDITIONS TO RUN THE TESTS
    1. Sinergify app must be installed in the test environment.
    2. Permission and webhook should be configured properly as per the documentation.
    3. Jira instance and Project & Field mapping should be done as per the requirement.
    4. Required & Optional fields should be mapped.
    */

    @Test
    public void verifyIfCreateJiraBtnIsVisible_Test() {
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput = sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchMenuItem(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchOpenCase(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.checkIfCreateJiraBtnVisible();
        Assert.assertTrue(expectedOutput);
    }

    @Test()
    public void verifyIfCreateJiraFormIsVisible_Test() {
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput = sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchMenuItem(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchOpenCase(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.openCreateJiraForm();
        Assert.assertTrue(expectedOutput);
    }

    @Test()
    public void verifyIfCreateJiraFormIsClosed_Test() {
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput = sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchMenuItem(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchOpenCase(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.openCreateJiraForm();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.closeJiraForm();
        Assert.assertTrue(expectedOutput);
    }

    @Test()
    public void verifyIfJiraIsCreated_Test() {
        boolean expectedOutput;


        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput = sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchMenuItem(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchOpenCase(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.openCreateJiraForm();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.createJiraIssue(map);
        Assert.assertTrue(expectedOutput);

    }


    @Test()
    public void verifyIfJiraIsEdited_Test() {
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput = sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchMenuItem(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchOpenCase(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.openCreateJiraForm();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.createJiraIssue(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.editJiraIssue(map);
        Assert.assertTrue(expectedOutput);
    }

    @Test()
    public void verifyIfPotentialResultsVisible_Test() {
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput = sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchMenuItem(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchOpenCase(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.openCreateJiraForm();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.matchPotentialResults(map);
        Assert.assertTrue(expectedOutput);

    }

    @Test()
    public void verifyIfAddCommentsAndFilesFormIsVisible_Test() {
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput = sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchMenuItem(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchOpenCase(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.openCreateJiraForm();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.verifyElementsOnAddCommentModal(map);
        Assert.assertTrue(expectedOutput);
    }

    @Test()
    public void verifyIfNewCommentIsAddedAtCreateJira_Test() {
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput = sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchMenuItem(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchOpenCase(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.openCreateJiraForm();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.createJiraIssueWithComment(map);
        Assert.assertTrue(expectedOutput);
    }

    @Test()
    public void verifyIfNewFileIsAddedAtCreateJira_Test() {
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput = sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchMenuItem(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchOpenCase(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.openCreateJiraForm();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.createJiraIssueWithFile(map);
        Assert.assertTrue(expectedOutput);
    }

    @Test
    public void verifyCreatedJiraInRelatedList_Test() {
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput = sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchMenuItem(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchOpenCase(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.openCreateJiraForm();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.createJiraIssue(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.openJiraRelatedList(map);
        Assert.assertTrue(expectedOutput);
    }

    @Test
    public void verifyLinkingJira_Test() {
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput = sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchMenuItem(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchOpenCase(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.openCreateJiraForm();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.linkJiraIssue(map);
        Assert.assertTrue(expectedOutput);
    }


    @Test
    public void verifyUnlinkingJira_Test() {
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput = sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchMenuItem(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchOpenCase(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.unlinkJiraIssue(map);
        Assert.assertTrue(expectedOutput);
    }


    /*Preconditions are :
    1. Jira should be authenticated
    2. Jira Transition toggle should be enabled
    3.
    * */
    @Test
    public void verifyUpdatingJiraTransition_Test() {
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput = sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchMenuItem(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchOpenCase(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.updateJiraStatusTransition(map);
        Assert.assertTrue(expectedOutput);
    }




    /*Test Case Related to the Configuration Settings*/


    @Test
    public void verifySalesforceJiraOneOneRelationshipAtCreateJira_Test() {
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput = sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchMenuItem(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchOpenCase(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.createJiraAndVerifyErrorMsg(map.get("ErrorMsg"));
        Assert.assertTrue(expectedOutput);
    }

    @Test
    public void verifySalesforceJiraOneOneRelationshipAtLinkJira_Test() {
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput = sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchMenuItem(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchOpenCase(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.linkJiraAndVerifyErrorMsg(map.get("JiraKey"));
        Assert.assertTrue(expectedOutput);
    }

    @Test
    public void verifySalesforceJiraOneManyRelationshipAtCreateJira_Test() {
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput = sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchMenuItem(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchOpenCase(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.openCreateJiraForm();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.createJiraIssue(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchMenuItem(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchOpenCase(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.openCreateJiraForm();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.createJiraIssue(map);
        Assert.assertTrue(expectedOutput);
    }

    /*Pre-conditions are -
     * Jira instance should be authorized
     * Project & Field mapping should be done
     * At least one Jira must be linked with the case
     * One-Many option should be selected in the Configuration Settings
     * BM-50 should exist in Jira
     * */

    @Test
    public void verifySalesforceJiraOneManyRelationshipAtLinkJira_Test() {
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput = sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchMenuItem(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchOpenCase(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchAndLinkJira(map.get("JiraKey"));
        Assert.assertTrue(expectedOutput);
    }

    @Test
    public void verifyIfSubtaskIsCreated_Test() {
        /*Initialized a variable to take decision about the test step*/
        boolean expectedOutput;
        /*Launching the browser in this method*/
        sgfutil.LaunchUrl(map.get("browser"));
        /*Getting logged in into the application*/
        expectedOutput = sgfutil.LogInIntoSinergify();
        /*Asserting the result from the previous test step*/
        Assert.assertTrue(expectedOutput);
        /*Searching the app from App Launcher & Asserting the result*/
        expectedOutput = sgfutil.searchMenuItem(map);
        Assert.assertTrue(expectedOutput);
        /*Opening a Jira Task and Create a SubTask & Asserting the result*/
        expectedOutput = sgfutil.createSubtask(map);
        Assert.assertTrue(expectedOutput);
    }

    @Test
    public void verifyIfIssueOpenedInJiraInstance_Test(){
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput = sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchMenuItem(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchOpenCase(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput=sgfutil.viewJiraInJiraInstance(map);
        Assert.assertTrue(expectedOutput);
    }

    @Test
    public void verifyIfSendCommentFromCaseToJira_Test(){
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput = sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchMenuItem(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchOpenCase(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput=sgfutil.verifyCaseCommentOnJira(map);
        Assert.assertTrue(expectedOutput);
    }

    @Test
    public void verifyIfSendCommentFromSFJiraToJira_Test(){
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput = sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.searchMenuItem(map);
        Assert.assertTrue(expectedOutput);
//        expectedOutput = sgfutil.searchOpenCase(map);
//        Assert.assertTrue(expectedOutput);
        expectedOutput=sgfutil.saveJiraCommentAtSf(map);
        Assert.assertTrue(expectedOutput);
    }

    @Test
    public void verifyIfSendCommentFromJiraToSFAsCaseComment_Test(){
        boolean expectedOutput;
        sgfutil.LaunchJiraUrl("Chrome","https://atlassianqa.atlassian.net/");
        expectedOutput=sgfutil.loginAtJiraInstance(map.get("jiraUname"),map.get("jiraPasswd"));
        Assert.assertTrue(expectedOutput);
        expectedOutput=sgfutil.addCommentOnJIRA(map.get("jiraComment"),map.get("JiraTaskKey"));
        Assert.assertTrue(expectedOutput);




    }





}
    