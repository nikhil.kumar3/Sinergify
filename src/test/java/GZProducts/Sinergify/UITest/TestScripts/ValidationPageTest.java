package GZProducts.Sinergify.UITest.TestScripts;

import GZProducts.Sinergify.SinergifyUtilities.POMUtilities.FieldConfigPageUtility;
import GZProducts.Sinergify.SinergifyUtilities.POMUtilities.ValidationPageUtility;
import GZProducts.Sinergify.UITest.TestBase.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ValidationPageTest extends TestBase {


    ValidationPageUtility validationUtil= new ValidationPageUtility();


    @Test(priority = 0)
    public void verifyIfValidationPageIsAccessible_Test(){
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput= sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput= sgfutil.addBasicAuthJiraCloud(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput=validationUtil.verifyValidationPage();
        Assert.assertTrue(expectedOutput);
    }

    @Test(dependsOnMethods = {"verifyIfValidationPageIsAccessible_Test"})
    public void verifyIfValidationRuleIsCreated_Test(){
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput= sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput= sgfutil.addBasicAuthJiraCloud(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput=validationUtil.addNewValidationRule();
        Assert.assertTrue(expectedOutput);

    }

    @Test(dependsOnMethods = {"verifyIfValidationRuleIsCreated_Test"})
    public void verifyIfValidationRuleIsEdited_Test(){
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput= sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput= sgfutil.addBasicAuthJiraCloud(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput=validationUtil.editValidationRule();
        Assert.assertTrue(expectedOutput);
    }

    @Test(dependsOnMethods = {"verifyIfValidationRuleIsEdited_Test"})
    public void verifyIfValidationRuleIsDeleted_Test(){
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput= sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput= sgfutil.addBasicAuthJiraCloud(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput=validationUtil.deleteValidationRule();
        Assert.assertTrue(expectedOutput);
    }


}
