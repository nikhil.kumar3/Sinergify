package GZProducts.Sinergify.UITest.TestScripts;

import CommonUtilities.GZBrowser.GZBrowser;
import GZProducts.Sinergify.UITest.TestBase.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;

public class PluginAuthenticationPageTest extends TestBase {

    @Test(threadPoolSize = 1,invocationCount = 2)
    public void verifyLoginInSalesforceWithValidInputs_Test(){
        boolean expectedOutput;
        sgfutil.LaunchJiraUrl("Chrome","https://jirasfdc.grazitti.com:8443/");          /* Launching the browser*/
        expectedOutput= sgfutil.LogInIntoJira();   /* Login into Salesforce org*/
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.manageJiraApps("Sinergify","Jir@43AA$int");
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.authenticationSF(map);
        Assert.assertTrue(expectedOutput);
        expectedOutput = sgfutil.loginInSF(map);
        Assert.assertTrue(expectedOutput);
    }



}
