package GZProducts.Sinergify.UITest.TestScripts;
import GZProducts.Sinergify.SinergifyUtilities.POMUtilities.AdminSettingsPageUtility;
import GZProducts.Sinergify.UITest.TestBase.TestBase;
import org.testng.annotations.Test;
import org.testng.Assert;

public class ProjectsPageTest extends TestBase {

    AdminSettingsPageUtility aspUtil = new AdminSettingsPageUtility();


    /*Verify if admin user clicks on Projects and see projects in the Grid*/

    @Test
    public void verifyProjectsInGrid_Test(){
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput= sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput= sgfutil.addBasicAuthJiraCloud(map);
        Assert.assertTrue(expectedOutput);
        Assert.assertTrue(sgfutil.openProjects());
        expectedOutput=sgfutil.verifyProjects();
        Assert.assertTrue(expectedOutput);
    }

//   @Test
    /*This test was a common test to configure the projects but now we have split it into several tests below*/
   public void verifyProjects_Test(){
       sgfutil.LaunchUrl(map.get("browser"));
       sgfutil.LogInIntoSinergify();
       sgfutil.addJiraInstance(map);
       Boolean result = sgfutil.verifyProjects();
       Assert.assertTrue(result);
   }

    /*Verify if admin user saves a project with Read & Write permission,
        Default issue type =Task, Issue type to sync=All*/

    @Test
    public  void verifySaveProjectWithSyncWritePermission_Test(){
        sgfutil.LaunchUrl(map.get("browser"));
        sgfutil.LogInIntoSinergify();
        sgfutil.addBasicAuthJiraCloud(map);
        Assert.assertTrue(sgfutil.openProjects());
        boolean result = sgfutil.saveSyncWriteProject();
        Assert.assertTrue(result);
    }

    /*Verify if admin user clicks on Project sharing icon*/
    /*Verify if admin is able to add a user successfully for Project sharing setting
        with Read only permission*/

    @Test
    public  void verifySaveProjectSharingSetting_Test(){
        sgfutil.LaunchUrl(map.get("browser"));
        sgfutil.LogInIntoSinergify();
        sgfutil.addBasicAuthJiraCloud(map);
        Assert.assertTrue(sgfutil.openProjects());
        boolean result = sgfutil.saveSyncWriteProject();
        Assert.assertTrue(result);
        Assert.assertTrue(sgfutil.addSharingSetting());
    }

    /*Verify if admin is able to add a user successfully for Project sharing setting
        with Read/Write permission*/

    @Test
    public  void verifySaveProjectSharingSettingWithWritePermission_Test(){
        sgfutil.LaunchUrl(map.get("browser"));
        sgfutil.LogInIntoSinergify();
        sgfutil.addBasicAuthJiraCloud(map);
        Assert.assertTrue(sgfutil.openProjects());
        boolean result = sgfutil.saveSyncWriteProject();
        Assert.assertTrue(result);
        Assert.assertTrue(sgfutil.addWriteSharingSetting());
    }

    /*Verify if admin is able to Edit/Update Project sharing setting*/

    @Test
    public  void verifyEditProjectSharingSetting_Test(){
        sgfutil.LaunchUrl(map.get("browser"));
        sgfutil.LogInIntoSinergify();
        sgfutil.addBasicAuthJiraCloud(map);
        Assert.assertTrue(sgfutil.openProjects());
        boolean result = sgfutil.saveSyncWriteProject();
        Assert.assertTrue(result);
        Assert.assertTrue(sgfutil.addSharingSetting());
    }


    /*Verify if admin user see the Default project dropdown on the Projects screen*/

    @Test
    public void verifyDefaultProjectDropdown_Test(){
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput= sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput= sgfutil.addBasicAuthJiraCloud(map);
        Assert.assertTrue(expectedOutput);
        Assert.assertTrue(sgfutil.openProjects());
        expectedOutput=sgfutil.checkDefaultProjectDropdown();
        Assert.assertTrue(expectedOutput);
    }

    /*Verify if admin user enters a project name in the Search input box on the Projects screen*/

    @Test
    public void verifyEnteringProjectName_Test() throws InterruptedException {
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput= sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput= sgfutil.addBasicAuthJiraCloud(map);
        Assert.assertTrue(expectedOutput);
        Assert.assertTrue(sgfutil.openProjects());
        expectedOutput=sgfutil.searchProjects();
        Assert.assertTrue(expectedOutput);
    }

    /*Verify if admin user is able to click Refresh icon on the Projects screen*/

    @Test
    public void verifyClickingRefreshIconOnProjectPage_Test() throws InterruptedException {
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput= sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput= sgfutil.addBasicAuthJiraCloud(map);
        Assert.assertTrue(expectedOutput);
        Assert.assertTrue(sgfutil.openProjects());
        Assert.assertTrue(sgfutil.refreshAllProjects());
    }

    /*Verify if admin user able to click and select a value in the project per page dropdown on
        the Projects screen*/

    @Test
    public void verifyClickSelectPageNumberOnProjectPage_Test() throws InterruptedException {
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));
        expectedOutput= sgfutil.LogInIntoSinergify();
        Assert.assertTrue(expectedOutput);
        expectedOutput= sgfutil.addBasicAuthJiraCloud(map);
        Assert.assertTrue(expectedOutput);
        Assert.assertTrue(sgfutil.openProjects());
        Assert.assertTrue(sgfutil.changePageNumber());
    }



}
