package GZProducts.Sinergify.UITest.TestScripts;

import GZProducts.Sinergify.SinergifyUtilities.SinergifyUtilities;
import GZProducts.Sinergify.UITest.TestBase.TestBase;
import org.testng.annotations.Test;

public class LinkJiraPageTest extends TestBase {

    SinergifyUtilities sgfutil=new SinergifyUtilities();

    /*PRE-CONDITIONS TO RUN THE TESTS
    1. Sinergify app must be installed in the test environment.
    2. Permission and webhook should be configured properly as per the documentation.
    3. Jira instance and Project & Field mapping should be done as per the requirement.
    4. Required & Optional fields should be mapped.
    */

    @Test
    public void verifyIfJiraIsLinked_Test(){

    }

    @Test
    public void verifyIfJiraIsUnLinked_Test(){

    }

    @Test
    public void verifyIfLinkOptionIsVisible_Test(){

    }

    @Test
    public void verifyIfUnLinkOptionIsVisible_Test(){

    }








}
