package GZProducts.Sinergify.UITest.TestScripts;

import CommonUtilities.GZBrowser.GZBrowser;
import GZProducts.Sinergify.UITest.TestBase.TestBase;
import org.testng.Assert;
import org.testng.annotations.*;


public class SFDCLoginPageTest extends TestBase {

    //@Parameters("browser")
    @Test
    public void validSFDCLogin_test() {
        sgfutil.LaunchUrl(map.get("browser"));
        sgfutil.LogInIntoSinergify();
        Assert.assertTrue(GZBrowser.getDriver().getTitle().contains("Salesforce"));
    }

    @Test
    public void invalidSFDCLogin_test() {
        sgfutil.LaunchUrl(map.get("browser"));
        String error = sgfutil.InvalidLogInIntoSinergify(map);
        Assert.assertTrue(error.contains("username and password"));
    }
}