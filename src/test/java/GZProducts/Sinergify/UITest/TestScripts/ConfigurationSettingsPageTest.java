package GZProducts.Sinergify.UITest.TestScripts;

import CommonUtilities.GZBrowser.GZBrowser;
import GZProducts.Sinergify.UITest.TestBase.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ConfigurationSettingsPageTest extends TestBase {

    /* The pre-conditions are
    * A Jira instance must be authorized
    * Project Mapping must be done properly
    * Field Configuration must be done properly
    * */

    @Test
    public void verifyEnableEditJiraInSalesforce_Test(){
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));          /* Launching the browser*/
        expectedOutput= sgfutil.LogInIntoSinergify();   /* Login into Salesforce org*/
        Assert.assertTrue(expectedOutput);
        expectedOutput=sgfutil.searchAppStore(map);     /*Search and Open Sinergify app*/
        Assert.assertTrue(expectedOutput);
        sgfutil.navigateAdminSetting();                 /*Navigate to the Admin Settings tab*/
        Assert.assertTrue(GZBrowser.getDriver().getCurrentUrl().contains("Admin_Setting"));
        expectedOutput=sgfutil.setEditJiraEnableInConfigurationSettings(map); /* Set Configuration Setting*/
        Assert.assertTrue(expectedOutput);
    }

    @Test
    public void verifyDisableEditJiraInSalesforce_Test(){
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));          /* Launching the browser*/
        expectedOutput= sgfutil.LogInIntoSinergify();   /* Login into Salesforce org*/
        Assert.assertTrue(expectedOutput);
        expectedOutput=sgfutil.searchAppStore(map);     /*Search and Open Sinergify app*/
        Assert.assertTrue(expectedOutput);
        sgfutil.navigateAdminSetting();                 /*Navigate to the Admin Settings tab*/
        Assert.assertTrue(GZBrowser.getDriver().getCurrentUrl().contains("Admin_Setting"));
        expectedOutput=sgfutil.setEditJiraDisableInConfigurationSettings(map); /* Set Configuration Setting*/
        Assert.assertTrue(expectedOutput);
    }

    @Test
    public void verifyEnableJiraTransition_Test(){
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));          /* Launching the browser*/
        expectedOutput= sgfutil.LogInIntoSinergify();   /* Login into Salesforce org*/
        Assert.assertTrue(expectedOutput);
        expectedOutput=sgfutil.searchAppStore(map);     /* Search and Open Sinergify app*/
        Assert.assertTrue(expectedOutput);
        sgfutil.navigateAdminSetting();                 /* Navigate to the Admin Settings tab*/
        Assert.assertTrue(GZBrowser.getDriver().getCurrentUrl().contains("Admin_Setting"));
        expectedOutput=sgfutil.setJiraTransitionEnable(map); /* Set Configuration Setting*/
        Assert.assertTrue(expectedOutput);

    }

    @Test
    public void verifyDisableJiraTransition_Test(){
        boolean expectedOutput;
        sgfutil.LaunchUrl(map.get("browser"));          /* Launching the browser*/
        expectedOutput= sgfutil.LogInIntoSinergify();   /* Login into Salesforce org*/
        Assert.assertTrue(expectedOutput);
        expectedOutput=sgfutil.searchAppStore(map);     /* Search and Open Sinergify app*/
        Assert.assertTrue(expectedOutput);
        sgfutil.navigateAdminSetting();                 /* Navigate to the Admin Settings tab*/
        Assert.assertTrue(GZBrowser.getDriver().getCurrentUrl().contains("Admin_Setting"));
        expectedOutput=sgfutil.setJiraTransitionDisable(map); /* Set Configuration Setting*/
        Assert.assertTrue(expectedOutput);

    }




}
