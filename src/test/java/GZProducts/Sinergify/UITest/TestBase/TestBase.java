package GZProducts.Sinergify.UITest.TestBase;

import CommonUtilities.DataProvider.DataProviderUtility;
import GZProducts.Sinergify.SinergifyUtilities.SinergifyUtilities;
import org.testng.annotations.*;
import java.lang.reflect.Method;
import java.util.Map;


public class TestBase {

    public static SinergifyUtilities sgfutil = null;
    public static DataProviderUtility dputil = null;
    public static Map<String, String> map = null;

    @BeforeTest
    public void setup() {
       sgfutil = new SinergifyUtilities();
       dputil = new DataProviderUtility();
    }

    @BeforeMethod
    protected void setupMethod(Method method) {
        String testName = method.getName();
        map = dputil.readMap(testName);
    }

    @AfterMethod(alwaysRun = true)
    protected void methodTearDown() {
        sgfutil.CloseBrowser();
        map = null;
    }

    @AfterTest
    public void tearDown(){
    }

}
