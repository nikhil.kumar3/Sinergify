package GZProducts.Sinergify.SinergifyUtilities.POMUtilities;

import CommonUtilities.GZLogger.GZLogger;
import GZProducts.Sinergify.SinergifyUtilities.POM.ValidationPage;

public class ValidationPageUtility {
    ValidationPage valPage = null;

    /*Adding top most field to selected field*/
    public boolean addFields() {
        boolean flag=false;
        valPage = new ValidationPage();
        try {
            valPage.selectObjDrpDwn();
            Thread.sleep(3000);
            valPage.addAvailField();
            Thread.sleep(4000);
            valPage.clickSaveBtn();
            Thread.sleep(3000);
            flag=valPage.clickRulesetTab();
            Thread.sleep(3000);
            GZLogger.ActivityLog("Available field is added to Selected field");
        } catch (Exception e) {
            GZLogger.ErrorLog(e,"Not able to add available field to Selected field");
        }
        return flag;
    }
    /*Removing top most field from selected field*/
    public boolean removeFields() {
        boolean flag=false;
        valPage = new ValidationPage();
        try{
            valPage.selectObjDrpDwn();
            valPage.removeReqField();
            Thread.sleep(5000);
            flag=valPage.clickSaveBtn();
            Thread.sleep(5000);
            GZLogger.ActivityLog("Selected field is removed on Validation page");
        }catch (Exception e) {
            GZLogger.ErrorLog(e,"Not able to remove the selected field");
        }
        return flag;
    }

    public boolean verifyValidationPage(){
        ValidationPage vp=new ValidationPage();
        try{
            if (vp.isValidationOptionVisibleInSideMenu())
                if(vp.clickValidationOptionInSideMenu())
                    if (vp.isAddValidationButtonVisible()){
                        GZLogger.PassLog("Validation page is accessible");
                        return true;
                    }
        }catch (Exception e){
            GZLogger.FailLog("Validation page is not accessible");
            return false;
        }
        return false;
    }

    public boolean addNewValidationRule(){
        ValidationPage vp=new ValidationPage();
        try{
            if (vp.isValidationOptionVisibleInSideMenu())
                if(vp.clickValidationOptionInSideMenu())
                    if (vp.isAddValidationButtonVisible())
                        if(vp.clickAddNewValidationButton())
                            if(vp.clickActiveToggleDisable())
                                if(vp.enterDescription())
                                    if(vp.clickSelectProject())
                                        if(vp.clickSelectObject())
                                            if(vp.clickSelectFieldName())
                                                if(vp.clickSelectOperator())
                                                    if(vp.clickValueCheckBox())
                                                        if(vp.enterErrMsg())
                                                            if(vp.enterFilterLogic())
                                                                if(vp.clickSaveBtnValidation())
                                                                    return true;
        }catch (Exception e){
            return false;
        }
        return false;
    }


    public boolean editValidationRule(){
        ValidationPage vp=new ValidationPage();
        try{
            if (vp.isValidationOptionVisibleInSideMenu())
                if(vp.clickValidationOptionInSideMenu())
                    if (vp.clickEditRuleBtn())
                        if(vp.editDescriptionInValidationRule()){
                            GZLogger.PassLog("The validation rule is edited successfully");
                            return true;
                        }
        }catch (Exception e){
            GZLogger.FailLog("The validation rule was not edited successfully");
            return false;
        }
        return false;
    }

    public boolean deleteValidationRule(){
        ValidationPage vp=new ValidationPage();
        try{
            if (vp.isValidationOptionVisibleInSideMenu())
                if(vp.clickValidationOptionInSideMenu())
                    if (vp.clickDeleteRuleBtn())
                        if(vp.clickOkBtnValidationRule()){
                            GZLogger.PassLog("The validation rule is deleted successfully");
                            return true;
                        }
        }catch (Exception e){
            GZLogger.FailLog("The validation rule wa not deleted successfully");
            return false;
        }
        return false;
    }




}

