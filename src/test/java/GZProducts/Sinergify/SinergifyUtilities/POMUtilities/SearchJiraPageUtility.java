package GZProducts.Sinergify.SinergifyUtilities.POMUtilities;

import CommonUtilities.GZCommonFuncs.GZCommonElementFuncs;
import CommonUtilities.GZLogger.GZLogger;
import GZProducts.Communities.CommunitiesUtilities.CommunitiesUtilities;
import GZProducts.Sinergify.SinergifyUtilities.POM.CreateJiraPage;
import GZProducts.Sinergify.SinergifyUtilities.POM.SearchJiraPage;

import java.util.Map;

public class SearchJiraPageUtility extends CreateJiraPage {

    public boolean openSearchJiraPage(){
        SearchJiraPage sjp = new SearchJiraPage();
        try{
            if (sjp.clickJiraIssueTab())
                if(sjp.clickSearchJiraBtn())
                    if(sjp.isSearchJiraTitleVisible())
                        return true;
        }catch (Exception e){
            GZLogger.FailLog("Search Jira page was not opened successfully");
            return false;
        }
        return false;
    }

    public boolean searchJiraIssueKey(String jiraKey) {
        SearchJiraPage sjp = new SearchJiraPage();
        try{
            if (openSearchJiraPage())
                if(sjp.searchJiraKey(jiraKey))
                    return true;
        }catch (Exception e){
            GZLogger.FailLog("Unable to search a Jira issue");
            return false;
        }
        return false;
    }

    public boolean matchJiraInstanceValue(String instance){
        SearchJiraPage sjp = new SearchJiraPage();
        try {
            if (openSearchJiraPage())
                if (sjp.getJiraInstanceText(instance))
                    return true;
            }catch (Exception e){
            return false;
        }
        return false;
    }

    public boolean checkProjectName(String project){
        SearchJiraPage sjp = new SearchJiraPage();
        try {
            if (sjp.getProjectName(project)) /*need to add the map and fetch the Jira project name from project page*/
                return true;
        }catch (Exception e){
            return false;
        }
        return false;
    }

    public boolean linkAtSearchJira(){
        try {
            SearchJiraPage sjp = new SearchJiraPage();
            if (sjp.clickShowMoreActionButton())
                if (sjp.checkIfLinkButtonOptionIsDisabled()){
                    return true;
                }
        }catch (Exception e){
            return false;
        }
        return false;
    }

    public boolean linkJiraIssue(){
        try{
            SearchJiraPage sjp = new SearchJiraPage();
            if (sjp.clickShowMoreActionButton())
                if (sjp.linkJira())
                    return true;
        }catch (Exception e){
            return false;
        }
        return false;
    }

    public boolean viewAndCloneJiraIssue(){
        try{
            SearchJiraPage sjp = new SearchJiraPage();
            if (sjp.clickShowMoreActionButton())
                if (sjp.clickViewDetailOption())
                    if (sjp.clickCloneBtn())
                        if (sjp.clickSaveBtn())
                        return true;
        }catch (Exception e){
            return false;
        }
        return false;
    }





}




