package GZProducts.Sinergify.SinergifyUtilities.POMUtilities;

import CommonUtilities.GZCommonFuncs.GZCommonElementFuncs;
import CommonUtilities.GZLogger.GZLogger;
import GZProducts.Sinergify.SinergifyUtilities.POM.ConfigurationSettingsPage;

public class ConfigurationSettingsPageUtility extends GZCommonElementFuncs {

    public boolean saveEnableEditJiraToggleAsEnabled(){
        try {
            ConfigurationSettingsPage csp = new ConfigurationSettingsPage();
            if (csp.clickConfigSettingOption())
                if (csp.enableEditJiraToggle())
                    if (csp.clickOnSaveBtn())
                        return true;
        }catch (Exception e){
            GZLogger.ActivityLog(e.getMessage());
            return false;
        }
        return false;
    }
    public boolean saveDisableEditJiraToggleAsEnabled(){
        try {
            ConfigurationSettingsPage csp = new ConfigurationSettingsPage();
            if (csp.clickConfigSettingOption())
                if (csp.disableEditJiraToggle())
                    if (csp.clickOnSaveBtn())
                        return true;
        }catch (Exception e){
            GZLogger.ActivityLog(e.getMessage());
            return false;
        }
        return false;
    }



    public boolean verifyEditButton(){
        try{
            ConfigurationSettingsPage csp = new ConfigurationSettingsPage();
            if (csp.isEditButtonVisible())
                return true;
        }catch (Exception e){
            return false;
        }
        return false;
    }

    public boolean enableJiraTransitionAndSave(){
        ConfigurationSettingsPage csp = new ConfigurationSettingsPage();
        if (csp.clickConfigSettingOption())
            if (csp.enableJiraTransition())
                if (csp.clickOnSaveBtn()){
                    GZLogger.ActivityLog("Enabled the Jira Transition and Saved");
                    return true;
                }else {
                    GZLogger.ActivityLog("Couldn't enable the Jira Transition and Saved");
                }
        return false;
    }

    public boolean disableJiraTransitionAndSave(){
        ConfigurationSettingsPage csp = new ConfigurationSettingsPage();
        if (csp.clickConfigSettingOption())
            if (!csp.disableJiraTransition())
                if (csp.clickOnSaveBtn()){
                    GZLogger.ActivityLog("Disabled the Jira Transition and Saved");
                    return true;
                }else {
                    GZLogger.ActivityLog("Couldn't disable the Jira Transition and Saved");
                }
        return false;
    }
    public boolean verifyJiraTransitionButton(){
        try{
            ConfigurationSettingsPage csp = new ConfigurationSettingsPage();
            if (csp.isEditIconVisibleTransition())
                return true;
        }catch (Exception e){
            return false;
        }
        return false;
    }




}
