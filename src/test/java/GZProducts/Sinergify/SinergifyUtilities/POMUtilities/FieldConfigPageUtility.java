package GZProducts.Sinergify.SinergifyUtilities.POMUtilities;

import CommonUtilities.GZCommonFuncs.GZCommonElementFuncs;
import CommonUtilities.GZLogger.GZLogger;
import GZProducts.Sinergify.SinergifyUtilities.POM.FieldConfigurationPage;
import org.apache.commons.math3.geometry.euclidean.twod.DiskGenerator;

import java.util.Map;

public class FieldConfigPageUtility extends GZCommonElementFuncs {

    /*Mapping required fields*/

    public boolean mapReqField(){
        boolean flag=false;
        FieldConfigurationPage fieldConfig=new FieldConfigurationPage();
        try{
            fieldConfig.selectObject();
            Thread.sleep(4000);
            //fieldConfig.selectJiraProject();
            Thread.sleep(5000);
            fieldConfig.clickEdit();
            Thread.sleep(4000);
            fieldConfig.selectObjectField();
            Thread.sleep(4000);
            flag=fieldConfig.clickSaveIcon();
            Thread.sleep(9000);
            fieldConfig.clickValidationTab();
            Thread.sleep(4000);
            GZLogger.PassLog("Required fields mapping is done");
        }catch (Exception ex){
            GZLogger.FailLog("Unable to map the required fields");
        }
        return flag;
    }
    /*Mapping optional fields*/
    public boolean mapOptFields(Map<String,String> map){
        FieldConfigurationPage fieldConfig=new FieldConfigurationPage();
        boolean flag=false;
        try{
            fieldConfig.clickMore();
            Thread.sleep(4000);
            fieldConfig.setLabel(map.get("label"));
            fieldConfig.selectJiraField();
            Thread.sleep(6000);
            fieldConfig.selectObjFieldMore();
            Thread.sleep(6000);
            flag=fieldConfig.clickSaveBtn();
            Thread.sleep(6000);
            fieldConfig.clickValidationTab();
            GZLogger.ActivityLog("Optional field mapping is done");
        }catch (Exception ex){
            GZLogger.ErrorLog(ex,"Not able to complete optional field mapping");
        }
        return flag;
    }

    public boolean checkIfFieldConfigPageIsVisible(){
        boolean status;
        FieldConfigurationPage fieldConfig=new FieldConfigurationPage();
        try{
            status=fieldConfig.clickFieldConfigLink();
            if (status)
                fieldConfig.fieldConfigHeaderVisible();
            return status;
        }catch (Exception e){
            return false;
        }

    }

    public boolean checkIfLabelsOnFieldConfigPageIsVisible(){
        boolean status;
        FieldConfigurationPage fieldConfig=new FieldConfigurationPage();

        try{
            status=fieldConfig.findChooseSFDCObjectLabel();
            if (status){
                status=fieldConfig.findChooseJiraProjectLabel();
                if (status){
                    GZLogger.ActivityLog("Admin is able to see the Labels on the Field Configuration screen");
                    return true;
                }else
                    return false;
            }else
                return false;
        }catch (Exception e){
            GZLogger.FailLog("Admin is not able to see the Labels on the Field Configuration screen");
            return false;
        }
    }

    public boolean checkIfDropdownOnFieldConfigPageIsVisible(){
        boolean status;
        FieldConfigurationPage fieldConfig=new FieldConfigurationPage();

        try{
            status=fieldConfig.findChooseSFDCObjectDropdown();
            if (status){
                status=fieldConfig.findChooseJiraProjectDropdown();
                if (status){
                    GZLogger.ActivityLog("Admin is able to see the dropdown on the Field Configuration screen");
                    return true;
                }else
                    return false;
            }else
                return false;
        }catch (Exception e){
            GZLogger.FailLog("Admin is not able to see the Dropdown on the Field Configuration screen");
            return false;
        }
    }
    public boolean setProjectMapping(){
        FieldConfigurationPage fieldConfig=new FieldConfigurationPage();
        try{
            if(fieldConfig.selectCaseObject())
                if(fieldConfig.selectJiraProject())
                    if(fieldConfig.clickEditIcons())
                        if( fieldConfig.getSetJiraFieldNameFromTable())
                            if(fieldConfig.setCaseFieldValueInTable())
                                if(fieldConfig.clickSaveProjectMapping()){
                                    GZLogger.PassLog("Admin is able to save the Field mapping of required fields");
                                    return true;
                                }
            return false;
        }catch (Exception e){
            GZLogger.FailLog("Admin is not able to save Field mapping with required fields");
            return false;
        }

    }
    public boolean setDefaultValues(){
        FieldConfigurationPage fieldConfig=new FieldConfigurationPage();
        try{
            fieldConfig.GZWait(0);
            fieldConfig.clickDefaultValueMenu();
            fieldConfig.clickDefaultValueEditIcons();
            fieldConfig.setDefaultValues();
            GZLogger.ActivityLog("Saved the Default value");
            return true;
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to save Default values");
            return false;
        }
    }

    public boolean setDefaultValue(){
        FieldConfigurationPage fieldConfig=new FieldConfigurationPage();
        try{
            if(fieldConfig.isGearIconVisibleOnProjectFieldMapping())
                if(fieldConfig.clickEditIconsDefaultValue())
                    if(fieldConfig.setDefaultValue())
                        GZLogger.ActivityLog("Saved the default value");
                    else return false;
                else return false;
            else return false;
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to save the Default value for the project");
            return false;
        }
        return true;
    }


    public boolean checkIfReportingPageIsVisible(){
        FieldConfigurationPage fieldConfig=new FieldConfigurationPage();
        try{
            if(fieldConfig.clickFieldConfigLink())
                if(fieldConfig.clickReportingMenu())
                    if(fieldConfig.reportingPageHeaderVisible())
                        if(fieldConfig.reportingPageLabelsVisible())
                            return true;
        }catch (Exception e){
            return false;
        }
        return false;
    }

    public boolean saveReportingSyncCheckbox(){
        FieldConfigurationPage fieldConfig=new FieldConfigurationPage();
        try{
            if(fieldConfig.clickReportingMenu())
                if(fieldConfig.clickReportingEditIcon())
                    if(fieldConfig.clickReportingSyncCheckbox())
                        if(fieldConfig.clickSaveProjectMapping())
                            if(fieldConfig.clickOKButtonReportingPage())
                                return true;
        }catch (Exception e){
            return false;
        }
        return false;
    }


    public boolean checkIfSyncConfigurationPageIsVisible(){
        FieldConfigurationPage fieldConfig=new FieldConfigurationPage();
        try{
            if(fieldConfig.clickFieldConfigLink())
                if(fieldConfig.clickSyncConfigurationMenu())
                    if(fieldConfig.syncConfigurationPageHeaderVisible())
                        if(fieldConfig.clickSyncConfigurationToggleIcon())
                            return true;
        }catch (Exception e){
            return false;
        }
        return false;
    }

    public boolean saveSyncConfiguration(){
        FieldConfigurationPage fieldConfig=new FieldConfigurationPage();
        try{
            if(fieldConfig.clickSyncConfigurationMenu())
                if(fieldConfig.clickSyncToggleDisable())
                    if(fieldConfig.clickSelectSyncTime())
                        if(fieldConfig.clickSyncButton())
                                return true;
        }catch (Exception e){
            return false;
        }
        return false;
    }









}