package GZProducts.Sinergify.SinergifyUtilities.POMUtilities;

import CommonUtilities.GZLogger.GZLogger;
import GZProducts.Sinergify.SinergifyUtilities.POM.RuleSetPage;
import GZProducts.Sinergify.SinergifyUtilities.POM.ValidationPage;

public class RulesetPageUtility {
    RuleSetPage rulepage = null;

    /*Adding a new rule on the Rule set page*/
    public boolean addNewRule(){
        boolean status=false;
        rulepage = new RuleSetPage();
        try{
            rulepage.clickAddNewRule();
            Thread.sleep(2000);
            rulepage.setSn();
            rulepage.clickActiveBx();
            rulepage.setDes();
            Thread.sleep(4000);
            rulepage.selectProject();
            Thread.sleep(5000);
            rulepage.selectObj();
            Thread.sleep(5000);
            rulepage.selRuleField();
            Thread.sleep(5000);
            rulepage.selRuleOperator();
            Thread.sleep(5000);
            rulepage.setValue();
            status=rulepage.clickSaveBtn();
            Thread.sleep(5000);
            GZLogger.ActivityLog("A new rule is created on Rule set page");
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to create a rule on the Rule set page");
        }
        return status;
    }


    public boolean ruleSetVisible(){
        RuleSetPage rsp= new RuleSetPage();
        if(rsp.rulesetOptionIsVisible())
            if(rsp.clickRulesetOption())
                if(rsp.ifAllElementsVisibleOnRulesetPage())
                    return true;
        return false;
    }

    public boolean openAddRulesetForm(){
        RuleSetPage rsp= new RuleSetPage();
        if(rsp.rulesetOptionIsVisible())
            if(rsp.clickRulesetOption())
                if(rsp.isAddNewRuleButtonVisible())
                    if(rsp.clickAddNewRuleBtn())
                        if(rsp.isAddNewRuleFormVisible()){
                            GZLogger.ActivityLog("Add New Rule form is open");
                            return true;
                        }
        return false;
    }


    public boolean addNewRuleset(){
        RuleSetPage rsp=new RuleSetPage();
        try{
            if (rsp.rulesetOptionIsVisible())
                if(rsp.clickRulesetOption())
                    if (rsp.isAddNewRuleButtonVisible())
                        if(rsp.clickAddNewRuleBtn())
                            if(rsp.clickActiveToggleDisable())
                                if(rsp.enterDescription())
                                    if(rsp.clickSelectProject())
                                        if(rsp.clickSelectObject())
                                            if(rsp.clickSelectFieldName())
                                                if(rsp.clickSelectOperator())
                                                    if(rsp.clickValueCheckBox())
                                                        if(rsp.enterFilterLogic())
                                                            if(rsp.clickSaveBtnRuleset())
                                                                return true;
        }catch (Exception e){
            return false;
        }
        return false;
    }


    public boolean editRuleset(){
        RuleSetPage rsp=new RuleSetPage();
        try{
            if (rsp.rulesetOptionIsVisible())
                if(rsp.clickRulesetOption())
                    if (rsp.clickEditRuleBtn())
                        if(rsp.editDescriptionInRuleset())
                            return true;
        }catch (Exception e){
            return false;
        }
        return false;
    }

    public boolean deleteRuleset(){
       RuleSetPage rsp=new RuleSetPage();
        try{
            if (rsp.rulesetOptionIsVisible())
                if(rsp.clickRulesetOption())
                    if (rsp.clickDeleteRuleBtn())
                        if(rsp.clickOkBtnRuleset())
                            return true;
        }catch (Exception e){
            return false;
        }
        return false;
    }







}
