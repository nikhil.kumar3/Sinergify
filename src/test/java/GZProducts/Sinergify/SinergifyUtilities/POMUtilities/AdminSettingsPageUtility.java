package GZProducts.Sinergify.SinergifyUtilities.POMUtilities;

import CommonUtilities.GZLogger.GZLogger;
import GZProducts.Sinergify.SinergifyUtilities.POM.AdminSettingPage;
//import com.google.inject.spi.StaticInjectionRequest;
import GZProducts.Sinergify.SinergifyUtilities.POM.ProjectsPage;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.openqa.selenium.WebElement;

import java.util.Map;

public class AdminSettingsPageUtility {
    AdminSettingPage asp =new AdminSettingPage();


    public boolean searchAndOpenApp(String appname) {
        asp = new AdminSettingPage();
        Boolean flag=false;
        try {
            asp.clickAppLauncher();
            asp.setAppName(appname);
            flag=asp.clickSinergifyApp();
            if(flag) {
                GZLogger.ActivityLog("Opened App Successfully");
            }
        } catch (Exception e) {
            GZLogger.ActivityLog("Not able to find the app in the org");
        }
        return flag;
    }


    public boolean openAdminSettings() {
        asp = new AdminSettingPage();
        boolean status=false;
        try {
            asp.clickAdminSettings();
            asp.checkIfInstanceActive();
            status=true;
            GZLogger.ActivityLog("clicked on the Admin Settings Tab");
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Not able to click on the Admin Settings tab");
        }
        return status;
    }
    public void fillAddInstanceForm(Map<String, String> map) throws Exception {
        asp = new AdminSettingPage();
        try {
            setAuthentication(map);
           // Thread.sleep(2000);
           // setAdminOptions(map);
           // Thread.sleep(2000);
            asp.clickSaveBtn();
            /*Recall the setFrame method to set the frame to find the element to click on*/
            //asp.setFrmSettings();
            //    asp.checkSuccessMessage(); // just to see if the Success message is visible or not
            /* This is to click on the Next button on the Admin settings page*/
            asp.clickNxtBtn();
            setConfigurationSettings(map);
            asp.clickSaveBtn();
            asp.clickNxtBtn();
            saveProjects();
//            setProjectMapping();
//            setDefaultValues();
//            asp.logout();
        } catch (Exception ex) {
            GZLogger.ErrorLog(ex, ex.getMessage());
            throw ex;
        }
    }
    public void checkToggleStatus(WebElement ele,String toggleKey,Map<String, String> map){
        try{
            String status= ele.getText();
            if(status!=(map.get(toggleKey))){
                ele.click();
                GZLogger.ActivityLog("Clicked on the toggle button");
            }else {
                GZLogger.ActivityLog("Provided test data for toggle button is same as its current status");
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on a toggle");
        }
    }
    public boolean setAuthentication(Map<String, String> map) throws InterruptedException {
        boolean status;
        try {
            if (map.get("authentication").equalsIgnoreCase("basic")) {
                // asp.setFrmSettings();
                status=asp.clickAddNewInstance(); //Click on Add new instance option on Instance screen
                // asp.clickBasicAuth();
                if (!status)
                    return false;
                asp.clearJiraName();
                asp.GZWait(0);
                asp.setJiraName(map.get("jiraInstaceName"));
                asp.clearJiraUrl();
                asp.GZWait(0);
                asp.setUrl(map.get("jiraurl"));
                asp.clearUsername();
                asp.setUname(map.get("jirausername"));
                asp.clearPassword();
                asp.setPassword(map.get("jirapassword"));
                //asp.clickDefaultInstance(); // set this instance as a Default instance
                asp.clickStatusToggle(); //click on status Active toggle
                try {
                    if (map.get("jirasoftware").equalsIgnoreCase("Cloud")) {
                        asp.clickJiraCloud();
                        GZLogger.ActivityLog("Clicked on Jira software as cloud radio button");
                    } else if (map.get("jirasoftware").equalsIgnoreCase("Server")) {
                        asp.clickJiraServer();
                        GZLogger.ActivityLog("Clicked on Jira software as server radio button");
                    } else {
                        GZLogger.ActivityLog("Test data is not found for Jira software");
                    }

                }catch (Exception e) {
                    GZLogger.ErrorLog(e, "Not able to click on Jira software option");
                }
                asp.clickSaveBtn();
                asp.clickNxtBtn();
                GZLogger.ActivityLog("Added the Jira instance with Basic auth type");
            }

        }catch (Exception e) {
            GZLogger.ErrorLog(e,"Not able to add a Jira instance with basic auth");
        }

        try{
        if (map.get("authentication").equalsIgnoreCase("oauth")) {
                //asp.setFrmSettings();
                asp.clickOpenAuth();
                asp.clearJiraUrl();
                asp.setUrl(map.get("jiraurl"));
                asp.clearConsumerKey();
                asp.setConsumerKey(map.get("consumerKey"));
                asp.clearPrivateKey();
                asp.setPrivateKey(map.get("privateKey"));
                asp.clickSaveBtn();
                GZLogger.ActivityLog("Added the Jira instance with OAuth auth type");
            }
        } catch (Exception e) {
            GZLogger.ErrorLog(e,"Not able to the add a Jira instance with OAuth auth type");
        }
        return true;
    }

    public boolean setOauth(Map<String, String> map){
        Boolean status=false;
        try{
            if (map.get("authentication").equalsIgnoreCase("oauth")) {
                //asp.setFrmSettings();
                asp.clickAddNewJiraInstance();
                asp.clickOauthTab();
                asp.clearJiraNameOauth();
                asp.GZWait(0);
                asp.setJiraNameOauth(map.get("jiraInstaceName"));
                asp.clearJiraUrlOauth();
                asp.GZWait(0);
                asp.setUrlOauth(map.get("jiraurl"));
                asp.clearConsumerKey();
                asp.GZWait(0);
                asp.setConsumerKey(map.get("consumerKey"));
                asp.clearPrivateKey();
                asp.setPrivateKey(map.get("privateKey"));
                asp.clickSaveBtn();
                status=asp.verifySaveSuccessMessageOauth(map);
            }
        } catch (Exception e) {
            GZLogger.ErrorLog(e,"Not able to the add a Jira instance with OAuth auth type");
        }
        return status;
    }

    public void setConfigurationSettings(Map<String, String> map) {
        try {
            if (map.get("sfjirarelation").equalsIgnoreCase("Saleforce Record - One Jira Issue")) {
                asp.GZWait(0);
                asp.clickDropdown();
                asp.selectDvalOne();
                GZLogger.ActivityLog("Selected Sf-Jira one to one relationship");
            } else if (map.get("sfjirarelation").equalsIgnoreCase("Saleforce Record - Many Jira Issues")) {
                {
                    asp.GZWait(0);
                    asp.clickDropdown();
                    asp.selectDvalMany();
                    GZLogger.ActivityLog("Selected the SF-Jira many to many relationship");
                }
            } else {
                GZLogger.ActivityLog("Test data is not found for SF-JIRA Dropdown");
            }
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Not able to select Sf Jira relation");
        }

        try {
            if (map.get("enableeditjira").equalsIgnoreCase("Yes")) {
                asp.clickEnableEditJira();
                GZLogger.ActivityLog("Clicked on Enable edit jira in salesforce Yes radio button");
                try {
                    if (map.get("enableStatusTrans_inSalesforce").equalsIgnoreCase("Yes")) {
                        asp.clickEnableStatusTranSfYes();
                        GZLogger.ActivityLog("Clicked on Enable status transition in salesforce Yes radio button");
                    } else if (map.get("enableStatusTrans_inSalesforce").equalsIgnoreCase("No")) {
                        asp.clickEnableStatusTra_inSalesforceNo();
                        GZLogger.ActivityLog("Clicked on Enable status transition in salesforce No radio button");
                    } else {
                        GZLogger.ActivityLog("Test data is not found for Enable status transition in salesforce option");
                    }
                } catch (Exception e) {
                    GZLogger.ErrorLog(e, "Not able to click on radio button Enable status transition in salesforce");
                }
            } else if (map.get("enableeditjira").equalsIgnoreCase("No")) {
                asp.clickEnableEditJiraNo();
                GZLogger.ActivityLog("Clicked on Enable edit jira in salesforce No radio button");
            } else {
                GZLogger.ActivityLog("Test data is not found for Enable Edit Jira option");
            }
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Not able to click on radio button Enable edit jira in salesforce");
        }

        try{
            if(map.get("feedSync").equalsIgnoreCase("Yes")){
                asp.clickFeedSyncYes();
                GZLogger.ActivityLog("Enabled Feed Sync toggle button");
            }
            else if(map.get("feedSync").equalsIgnoreCase("No")){
                asp.clickFeedSyncNo();
                GZLogger.ActivityLog("Disabled Feed Sync toggle");
            }
            else {
                GZLogger.ActivityLog("Test data is not found for Feed Sync");
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on Feed Sync toggle");
        }
        try {
            if (map.get("autoSyncSalesforceComnts").equalsIgnoreCase("Yes")) {
                asp.clickautosynsfcmntsyes();
                GZLogger.ActivityLog("Enabled Auto Sync Salesforce Comments toggle button");
                try {
                    if (map.get("typeofcommentstosync").equalsIgnoreCase("Both")) {
                        asp.clicktypeofcmntsynboth();
                        GZLogger.ActivityLog("Clicked on option Both to Sync Salesforce comments");
                    } else if (map.get("typeofcommentstosync").equalsIgnoreCase("Public")) {
                        asp.clicktypeofcmntsynPublic();
                        GZLogger.ActivityLog("Clicked on option Public to Sync Salesforce comments");
                    } else if (map.get("typeofcommentstosync").equalsIgnoreCase("Private")) {
                        asp.clicktypeofcmntsynPrvt();
                        GZLogger.ActivityLog("Clicked on option Private to Sync Salesforce comments");
                    } else {
                        GZLogger.ActivityLog("Test data is not found for the type of Salesforce comments to sync");
                    }
                } catch (Exception e) {
                    GZLogger.ErrorLog(e, "Not able to click on Select the type of comments to sync");
                }
            } else if (map.get("autoSyncSalesforceComnts").equalsIgnoreCase("No")) {
                asp.clickAutoSyncSfCmntNo();
                GZLogger.ActivityLog("Disabled Auto Sync Salesforce Comments toggle button");
            } else {
                GZLogger.ActivityLog("Test data is not found for Auto Sync Salesforce Comments");
            }
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Not able to click on toggle button for Auto Sync Salesforce Comments");
        }

        try {
            if (map.get("autoSyncSalesforceAttach").equalsIgnoreCase("Yes")) {
                asp.clickAutoSynSfAttahYes();
                GZLogger.ActivityLog("Enabled the Auto Sync Salesforce Attachments toggle button");
            } else if (map.get("enblestatusTrans_inSalesforce").equalsIgnoreCase("No")) {
                asp.clickAutoSynSfAttahNo();
                GZLogger.ActivityLog("Disabled the Auto Sync Salesforce Attachments toggle button");
            } else {
                GZLogger.ActivityLog("Test data is not found for Auto sync salesforce attachments option");
            }
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Not able to click on Auto sync salesforce attachments toggle");
        }

        try {
            if (map.get("syncJiraAttachSf").equalsIgnoreCase("Yes")) {
                asp.syncJiraAttToSfYes();
                GZLogger.ActivityLog("Enabled toggle for Sync Jira Attachments into Salesforce");
                try {
                    if (map.get("SyncAttachmentsToSfdcObject").equalsIgnoreCase("Yes")) {
                        asp.syncAttachmentToSfObjectYes();
                        GZLogger.ActivityLog("Enabled toggle for Sync Attachments SFDC object");
                    } else if (map.get("SyncAttachmentsToSfdcObject").equalsIgnoreCase("No")) {
                        asp.syncAttachmentToSfObjectNo();
                        GZLogger.ActivityLog("Disabled toggle for Sync Attachments SFDC object");
                    } else {
                        GZLogger.ActivityLog("Test data is not found for Sync Attachments SFDC object");
                    }
                } catch (Exception e) {
                    GZLogger.ErrorLog(e, "Not able to click on toggle for Sync Attachments SFDC object");
                }

                try {
                    if (map.get("syncJiracommntsinsf").equalsIgnoreCase("Yes")) {
                        asp.clickjirscmntinsfyes();
                        GZLogger.ActivityLog("Enabled Sync Jira Comments into Salesforce button");
                        try {
                            if (map.get("comntsToSaved").equalsIgnoreCase("Public")) {
                                asp.clickcmntssavedpublic();
                                GZLogger.ActivityLog("Clicked on Sync Jira Comments into SF as Public");
                            } else if (map.get("comntsToSaved").equalsIgnoreCase("Private")) {
                                asp.clickcmntssavedPrvt();
                                GZLogger.ActivityLog("Clicked on Sync Jira Comments into SF as Private");
                            } else {
                                GZLogger.ActivityLog("Test data is not found for Sync Jira Comments into SF to be saved as?");
                            }
                        } catch (Exception e) {
                            GZLogger.ErrorLog(e, "Not able to click on Sync Jira Comments into SF to be saved as?");
                        }
                    } else if (map.get("syncJiracommntsinsf").equalsIgnoreCase("No")) {
                        asp.clickjirscmntinsfNo();
                        GZLogger.ActivityLog("Disabled Sync Jira Comments into Salesforce button");
                    } else {
                        GZLogger.ActivityLog("Test data is not found for Sync Jira comments into salesforce");
                    }
                } catch (Exception e) {
                    GZLogger.ErrorLog(e, "Not able to click on Sync Jira comments into salesforce");
                }

                try{
                    if (map.get("syncAsCaseComments").equalsIgnoreCase("Yes")) {
                        asp.clicksyncascasecmntyes();
                        GZLogger.ActivityLog("Enabled Sync as Case Comments button");
                    } else if (map.get("syncAsCaseComments").equalsIgnoreCase("No")) {
                        asp.clicksyncascasecmntNo();
                        GZLogger.ActivityLog("Disabled Sync as Case Comments button");
                    } else {
                        GZLogger.ActivityLog("Test data is not found for Sync as Case Comments");
                    }
                } catch(Exception e){
                    GZLogger.ErrorLog(e, "Not able to click on Sync as Case Comments");
                }

                try {
                    if (map.get("AddAttachmentsQualifier").equalsIgnoreCase("No")) {
                        /*do nothing*/
                        GZLogger.ActivityLog("Nothing to enter in Add Attachments Qualifier input");
                    } else {
                        asp.enterAttachmentQualifier(map.get("AddAttachmentsQualifier"));
                        GZLogger.ActivityLog("Entered the test data in Add Attachments Qualifier input");
                    }
                } catch (Exception e) {
                    GZLogger.ErrorLog(e, "Not able to enter value in Add Attachments Qualifier input");
                }
            } else if (map.get("syncJiraAttachSf").equalsIgnoreCase("No")) {
                asp.syncJiraAttToSfNo();
                GZLogger.ActivityLog("Disabled toggle for Sync Jira Attachments into Salesforce");
            } else {
                GZLogger.ActivityLog("Test data is not found for Sync Jira Attachments into Salesforce");
            }
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Not able to click on Sync Jira Attachments into Salesforce");
        }


        try {
            if (map.get("AddCommentsQualifier").equalsIgnoreCase("No")) {
                /*do nothing*/
                GZLogger.ActivityLog("Nothing to enter in Add Comments Qualifier input");
            } else {
                asp.enterCommentsQualifier(map.get("AddCommentsQualifier"));
                GZLogger.ActivityLog("Entered the test data in Add Comments Qualifier input");
            }
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Not able to enter value in Add Comments Qualifier input");
        }

}
    public boolean refreshProjects(){
        boolean status;
        try{
            asp.clickRefreshProject();
            asp.selectProjectPerPageDropdown();
            asp.clickSearchProjectInput();
            asp.clickSaveBtn();
            status=asp.clickNxtBtn();
            GZLogger.ActivityLog("Refreshed the project successfully");
            return status;
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to refresh the Project");
            return false;
        }

    }
    public boolean saveProjects(){
        boolean status;
        ProjectsPage psp=new ProjectsPage();
        try{
//            asp.clickRefreshProject();
//            asp.selectProjectPerPageDropdown();
            asp.clickSearchProjectInput();
            asp.searchProject();
            psp.checkIfSyncCheckBoxTrue();
            psp.checkIfWriteCheckBoxTrue();

//            asp.clickSyncCheckbox();
//            asp.clickWriteCheckbox();
            asp.clickDefaultIssueTypeDropdown();
            asp.selectDefaultIssueType();
            asp.clickIssueAdderIcon();
            asp.clickAllIssueTypeToSync();
            //asp.clickAddShareSettingIcon();
            asp.clickDefaultProject();
            asp.selectDefaultProject();
            asp.clickSaveBtn();
            status=asp.clickNxtBtn();
            GZLogger.ActivityLog("Saved the project with Sync and Write permission");
            return status;
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to save the Project");
            return false;
        }

}
//    public void setProjectMapping(){
//        try{
//            asp.clickChooseSfdcObjectDropdown();
//            asp.selectCaseObject();
//            asp.clickChooseJiraProject();
//            asp.selectJiraProject();
//            asp.clickEditIcon();
//            asp.setLabel();
//            asp.saveButtonProjectMapping();
//            GZLogger.ActivityLog("Saved the Project mapping");
//        }catch (Exception e){
//            GZLogger.ErrorLog(e,"Not able to save Project mapping");
//        }
//
//    }
//    public void setDefaultValues(){
//        try{
//            asp.GZWait(0);
//            asp.clickDefaultValueMenu();
//            asp.clickDefaultValueEditIcons();
//            asp.setDefaultValues();
//            GZLogger.ActivityLog("Saved the Default value");
//        }catch (Exception e){
//            GZLogger.ErrorLog(e,"Not able to save Default values");
//        }
//    }
    //public void setAdminOptions(Map<String,String> map) {

//        asp.scrollBtm();
//        asp.scrollBtm();
//        try {
//            if(map.get("DefaultInstance").equalsIgnoreCase("Yes")){
//                asp.clickJiraCloud();
//                GZLogger.ActivityLog("Clicked on Default instance as Yes");
//            }
//            else if(map.get("DefaultInstance").equalsIgnoreCase("No")){
//                asp.clickJiraServer();
//                GZLogger.ActivityLog("Clicked on Default instance as No");
//            }
//            else{
//                GZLogger.ActivityLog("Test data is not found for Default instance");
//            }
//        }catch(Exception e){
//            GZLogger.ErrorLog(e,"Not able to click on Default instance option");
//
//        }

    public boolean isSideOptionsVisible(){
        asp = new AdminSettingPage();
        Boolean flag=false;
        try{
            flag=asp.instanceOptionVisible();
            if(flag) {
                flag = asp.configurationSettingsOptionVisible();
                flag = asp.projectsOptionVisible();
                flag = asp.fieldConfigurationOptionVisible();
                flag = asp.validationOptionVisible();
                flag = asp.rulesetOptionVisible();
                GZLogger.ActivityLog("Admin user see all options (Instances, Configuration settings, Projects, Field Configuration,Validation,Ruleset) in the side menu");
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to see options in the side menu");
        }
        return flag;
    }

    public boolean checkSinergifyLogo(){
        asp = new AdminSettingPage();
        Boolean flag=false;
        flag=asp.isLogoVisible();
        return flag;
    }

    public boolean checkInstanceChangeIcon(){
        asp=new AdminSettingPage();
        Boolean flag=false;
        flag=asp.isChangeInstanceIconVisible();
        return flag;
    }
    public boolean checkLanguageChangeIcon(){
        asp=new AdminSettingPage();
        Boolean flag=false;
        try {
            flag = asp.isLanguageIconVisible();
            GZLogger.ActivityLog("Admin user see the language change icon (a flag) on the top right corner of the webpage");
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Admin user does not see the language change icon (a flag) on the top right corner of the webpage");
        }
        return flag;
    }

    public boolean checkJiraTiles(){
        asp=new AdminSettingPage();
        Boolean flag=false;
        try {
            flag = asp.jiraTiles();
        }catch (Exception e){
            GZLogger.ErrorLog(e,e.getMessage());
        }
        return flag;
    }
    public boolean checkBasicAuthTabClickable(){
        asp=new AdminSettingPage();
        Boolean flag=false;
        try {
            flag = asp.isBasicAuthClickable();
        }catch (Exception e){
            GZLogger.ErrorLog(e,e.getMessage());
        }
        return flag;
    }
    public void clickAddInstanceOption(){
        asp=new AdminSettingPage();
        try {
            asp.clickAddNewJiraInstance();
        }catch (Exception e){
            GZLogger.ErrorLog(e,e.getMessage());
        }
    }
    public boolean isOauthTabVisible(){
        asp=new AdminSettingPage();
        Boolean status=false;
        try {
            status=asp.isCallBackUrlPresent();
        }catch (Exception e){
            GZLogger.ErrorLog(e,e.getMessage());
        }
        return status;
    }

    public boolean isWebhookHelpClickable(){
        Boolean status=false;
        try{
            status=asp.clickWebhookHelpLink();
            GZLogger.ActivityLog("Admin user is able to click on Webhook help? link");
        }catch (Exception e){
            GZLogger.ErrorLog(e,e.getMessage());
        }
        return status;
    }
    public boolean isWebhookHelpClosed(){
        Boolean status=false;
        try{
            status=asp.clickCloseBtnWebhookPopup();
            GZLogger.ActivityLog("Admin user is able to close Webhook help? window");
        }catch (Exception e){
            GZLogger.ErrorLog(e,e.getMessage());
        }
        return status;
    }

    public boolean isOauthTabOpen(){
        Boolean status=false;
        try{
            status=asp.clickOauthTab();
        }catch (Exception e){
            GZLogger.ErrorLog(e,e.getMessage());
        }
        return status;
    }
    public void clickSelectSite() {
        asp.clickSelectSiteDropdown();
        asp.checkWebhookUrlCopyLink();
    }

    public boolean checkOptionsClickableInSideMenu(){
        asp=new AdminSettingPage();
        boolean status=false;
        String passMessage="All options (Configuration settings, Projects, Field Configuration, Validation, Ruleset) in left side pane are clickable";
        String failMessage="All options (Configuration settings, Projects, Field Configuration, Validation, Ruleset) in left side pane are not clickable";
        status=asp.isSideOptionsClickable();
        if (status)
            GZLogger.ActivityLog(passMessage);
        else
            GZLogger.FailLog(failMessage);
        return status;
    }

    public boolean elementsOnConfigSettings(){
        asp=new AdminSettingPage();
        return asp.findElementsOfConfigSettings();
    }

    public boolean setAllToggleEnable() throws InterruptedException {
        boolean status=false;
        try {
            asp.clickFeedSyncYes();
            asp.clickautosynsfcmntsyes();
            asp.clicktypeofcmntsynPublic();
            asp.clickEnableEditJira();
            asp.clickEnableStatusTranSfYes();
            asp.clickAutoSynSfAttahYes();
            asp.clickjirscmntinsfyes();
            asp.clickcmntssavedpublic();
            asp.syncJiraAttToSfYes();
            asp.clicksyncascasecmntyes();
            asp.syncAttachmentToSfObjectYes();
            asp.clickSaveBtn();
            status = asp.clickNxtBtn();
            GZLogger.ActivityLog("Admin user enables all toggle buttons on Configuration settings screen and save it successfully");
        }catch (Exception e){
            GZLogger.ErrorLog(e,e.getMessage());
            GZLogger.FailLog("Admin user is not able to enable all toggle buttons on Configuration settings screen");
        }
        return status;
    }

    public boolean enterQualifiers(Map<String ,String > map){
        boolean status = false;
        asp=new AdminSettingPage();
        try {
            //asp.clickjirscmntinsfyes();
            asp.clickcmntssavedpublic();
            asp.syncJiraAttToSfYes();
            asp.clicksyncascasecmntyes();
            asp.syncAttachmentToSfObjectYes();
            asp.enterCommentsQualifier(map.get("AddCommentsQualifier"));
            asp.enterAttachmentQualifier(map.get("AddAttachmentsQualifier"));
            asp.clickSaveBtn();
            status = asp.clickNxtBtn();
            GZLogger.ActivityLog("Admin user enters Comment qualifier and Attachment qualifier on Configuration settings screen and save it successfully");
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Admin user is not able to enter and save Comment qualifier and Attachment qualifier on Configuration settings screen");
        }
        return status;
    }
//    public boolean openProjects(){
//        asp=new AdminSettingPage();
//        boolean status=asp.projectsOptionVisible();
//        asp.clickProjectTab();
//        if (status)
//            GZLogger.ActivityLog("Admin user clicks on Projects and see projects in the Grid");
//        return status;
//    }

    public boolean checkIfJiraInstanceAdded(){
        asp=new AdminSettingPage();
        boolean isAdded=asp.checkIfFirstInstanceAdded();
        if(isAdded)
            return true;
        else
            return false;
    }

    public boolean checkIfJiraInstanceActive(Map<String,String > map){
        asp=new AdminSettingPage();
        boolean isActive=asp.instanceIsActive();
        if(isActive){
            GZLogger.ActivityLog("Jira instance is already in Active status");
            return true;
        }else{
            asp.clickFirstInstance();
            String authName=asp.getActiveAuthName();
            if(authName.contains("Basic Authentication")){
                String jiraName=asp.getJiraName();
                System.out.println("Jira Name is"+":"+jiraName);
                asp.clickInactiveStatusToggle();
                asp.clickSaveBtn();
                return asp.clickNxtBtn();
            }else if(authName.contains("OAuth Authentication")){
                asp.clickInactiveStatusToggle();
                asp.clickSaveBtn();
                return asp.verifyOauthPermission(map);
            }else {
                GZLogger.FailLog("Not able to find if a Jira instance is Active");
                return false;
            }
        }
    }


}



