package GZProducts.Sinergify.SinergifyUtilities.POMUtilities;

import GZProducts.Sinergify.SinergifyUtilities.POM.PluginAuthenticationPage;

import java.util.Map;

public class PluginAuthenticationPageUtility {

    public boolean loginJira(String uname, String pwd){
        try{
            PluginAuthenticationPage pap = new PluginAuthenticationPage();
            if (pap.enterUsername(uname))
                if (pap.enterPassword(pwd))
                    if (pap.clickLoginBtn())
                        if (pap.isSettingIconVisible())
                            return true;
        }catch (Exception e){
            return false;
        }
        return false;
    }

    public boolean searchApp(String appName,String pwd){
        try{
            PluginAuthenticationPage pap = new PluginAuthenticationPage();
            if(pap.isSettingIconVisible())
                if (pap.clickSettingIcon())
                    if (pap.clickManageAppsOption())
                        if (pap.enterSudoPassword(pwd))
                            if (pap.clickConfirmBtn())
                                if (pap.isAdminSearchBoxVisible())
                                    if (pap.clickSearchJiraAdminBox())
                                        if (pap.searchPage(appName))
                                            if (pap.clickAuthenticationOption())
                                                return true;
        }catch (Exception e){
            return false;
        }
        return false;
    }

    public boolean authenticateSF(Map<String,String> map){
        try {
            PluginAuthenticationPage pap = new PluginAuthenticationPage();
            if (pap.enterSfInstanceUrl(map.get("instanceSF")))
                if (pap.enterClientKey(map.get("cKey")))
                    if (pap.enterClientSecret(map.get("cSecret")))
                        if (pap.clickLoginInSfBtn())
                            return true;
        }catch (Exception e){
            return false;
        }
        return false;
    }

    public boolean loginInSFForJiraPlugin(Map<String,String> map){
        try{
            PluginAuthenticationPage pap = new PluginAuthenticationPage();
            if(pap.enterUserNameSf(map.get("username")))
                if (pap.enterPasswordSf(map.get("password")))
                    if (pap.clickLoginButtonSF())
                        if (verifySFStatus())
                            return true;
        }catch (Exception e){
            return false;
        }
        return false;
    }

    public boolean verifySFStatus(){
        try {
            PluginAuthenticationPage pap = new PluginAuthenticationPage();
            if (pap.verifyConnectionStatus())
                return true;
        }catch (Exception e){
            return false;
        }
        return false;
    }


}
