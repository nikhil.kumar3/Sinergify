package GZProducts.Sinergify.SinergifyUtilities.POMUtilities;

import CommonUtilities.GZLogger.GZLogger;
import GZProducts.Sinergify.SinergifyUtilities.POM.AdminSettingPage;
import GZProducts.Sinergify.SinergifyUtilities.POM.ProjectsPage;

public class ProjectsPageUtility {
    ProjectsPage pjp= null;
    AdminSettingPage asp;

    /*Checking if there are some projects available on the Projects page*/
    public boolean checkIfProjectsPresent(){
        pjp= new ProjectsPage();
        boolean status=false;
        try{
           status=pjp.someProjectsAvail();
        }catch (Exception e){
            GZLogger.ErrorLog(e,"No project found");
        }
        return status;
    }

    /*Setting the sync & write permissions to projects on the Projects page*/
    public boolean setProjectPermission(){
        boolean flag=false;
        try {
            pjp.clickSyncBox();
            pjp.clickWriteBox();
            Thread.sleep(4000);
            pjp.clickDefaultIssuType();
            Thread.sleep(4000);
            pjp.selectDefaultIssue();
            Thread.sleep(4000);
            pjp.selectDefProject();
            Thread.sleep(4000);
            pjp.clickSave();
            Thread.sleep(7000);
            flag=pjp.clickNext();
            Thread.sleep(7000);
            GZLogger.ActivityLog("Project(s) are saved");
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to save projects setting");
        }
        return flag;
    }
    public boolean openProjects(){
        asp=new AdminSettingPage();
        boolean status=asp.projectsOptionVisible();
        if (status)
            asp.clickProjectTab();

            GZLogger.ActivityLog("Admin user clicks on Projects and see projects in the Grid");
        return status;
    }

    public boolean checkDefaultProjectDropdown(){
        pjp=new ProjectsPage();
        boolean status=false;
        status=pjp.isDefaultProjectDropdownVisible();
        return status;
    }

    public boolean checkSearchProject() throws InterruptedException {
        pjp=new ProjectsPage();
        boolean status = pjp.searchProject();
        return status;
    }

    public boolean refreshProjects() throws InterruptedException {
        pjp=new ProjectsPage();
        boolean status = pjp.clickRefreshProject();
        return status;
    }

    public boolean selectPageNumber() throws InterruptedException {
        pjp=new ProjectsPage();
        boolean status = pjp.clickPageNumber();
        return status;
    }

    public boolean setSharingSetting(){
        boolean status=false;
        try{
            pjp=new ProjectsPage();
            pjp.clickAddShareSettingIcon();
            pjp.clickAddButtonProjectSharingSetting();
            pjp.clickShareToDropdown();
            pjp.clickFirstRole();
            pjp.clickMoveElementToSelected();
            status=pjp.clickSaveButtonSharingSetting();
            Thread.sleep(5000);
            return status;
        }catch(Exception e){
            return status;
        }
    }

    public boolean setWriteSharingSetting(){
        boolean status=false;
        try{
            pjp=new ProjectsPage();
            pjp.clickAddShareSettingIcon();
            pjp.clickAddButtonProjectSharingSetting();
            pjp.clickShareToDropdown();
            pjp.clickFirstRole();
            pjp.clickMoveElementToSelected();
            status=pjp.clickAccessDropdown();
            status=pjp.clickSaveButtonSharingSetting();
            Thread.sleep(5000);
            return status;
        }catch(Exception e){
            return status;
        }
    }

    public boolean editSharingSetting(){
        boolean status=false;
        try{
            pjp=new ProjectsPage();
            pjp.clickAddShareSettingIcon();

            status=pjp.clickSaveButtonSharingSetting();
            Thread.sleep(5000);
            return status;
        }catch(Exception e){
            return status;
        }
    }

    public boolean findClickSyncCheckbox(){
        pjp=new ProjectsPage();
        return pjp.checkIfSyncCheckBoxTrue();
    }

    public boolean findClickWriteCheckbox(){
        pjp=new ProjectsPage();
        return pjp.checkIfWriteCheckBoxTrue();
    }

    public boolean findDefaultIssueType(){
        pjp=new ProjectsPage();
        return pjp.checkIfDefaultIssueTypeSelected();
    }

    public boolean findIssueTypeToSync(){
        pjp=new ProjectsPage();
        return pjp.checkIfIssueTypeToSyncSelected();
    }









}
