package GZProducts.Sinergify.SinergifyUtilities.POMUtilities;

import CommonUtilities.GZLogger.GZLogger;
import GZProducts.Sinergify.SinergifyUtilities.POM.SFDCLoginPage;

public class SFDCLoginPageUtility {
    SFDCLoginPage lp = null;
    public boolean LogIn(String username, String password){
        lp = new SFDCLoginPage();
        Boolean flag= false;
        try{
            lp.setUserName(username);
            lp.setpassword(password);
            flag=lp.clickSubmit();
            Thread.sleep(2000);
        }catch (Exception ex){
            GZLogger.ErrorLog(ex, ex.getMessage());
        }
        return flag;
    }

    public String InvalidLogIn(String username, String password){
        lp = new SFDCLoginPage();
        try{
            LogIn(username, password);
        }catch (Exception ex){
            GZLogger.ErrorLog(ex, ex.getMessage());
        }
        return lp.getLoginError();
    }
}
