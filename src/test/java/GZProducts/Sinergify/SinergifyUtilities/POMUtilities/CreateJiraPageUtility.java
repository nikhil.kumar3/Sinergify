package GZProducts.Sinergify.SinergifyUtilities.POMUtilities;
import CommonUtilities.GZCommonFuncs.GZCommonElementFuncs;
import CommonUtilities.GZLogger.GZLogger;
import GZProducts.Sinergify.SinergifyUtilities.POM.CreateJiraPage;
import GZProducts.Sinergify.SinergifyUtilities.POM.SearchJiraPage;
import GZProducts.Sinergify.UITest.TestScripts.CreateJiraPageTest;

import java.util.Map;
public class CreateJiraPageUtility extends GZCommonElementFuncs {

    public boolean openCaseNumber(Map<String,String> map){
        CreateJiraPage cjp=new CreateJiraPage();
        try{
            GZWait(0);
            if(cjp.clickOnCase(map.get("caseNumber"))){
                        return true;
            }
        }catch (Exception e){
            return false;
        }
        return false;
    }

    public boolean createJiraBtnVisible(){
        CreateJiraPage cjp=new CreateJiraPage();
        try{
            if(cjp.clickDropdownIcon())
                if(cjp.isCreateJiraBtnVisible())
                    return true;
        }catch (Exception e){
            return false;
        }
        return false;
    }



    public boolean checkUICreateJiraForm(){
        CreateJiraPage cjp=new CreateJiraPage();
        try {
            if(cjp.clickCreateJiraBtn())
                if(cjp.headersOnCreateJiraForm_Visible())
                    if (cjp.buttonsOnCreateJiraForm_Visible())
                        return true;
        }catch (Exception e){
            GZLogger.FailLog("Not able to find all UI elements on Create Jira form");
            return false;
        }
        return false;
    }

    public boolean checkUIAndCloseJiraForm(){
        CreateJiraPage cjp=new CreateJiraPage();
        try {
            if (cjp.buttonsOnCreateJiraForm_Visible())
                if(cjp.clickCloseJiraBtn())
                    if(!(cjp.cancelBtnVisible()))
                        return true;
        }catch (Exception e){
            GZLogger.FailLog("Not able to find all UI elements on Create Jira form");
            return false;
        }
        return false;
    }

    public boolean fillCreateJiraForm(Map<String,String> map){
        CreateJiraPage cjp=new CreateJiraPage();
        try{
            if (cjp.headersOnCreateJiraForm_Visible())
                if(cjp.enterSummary(map.get("Summary")))
                    if(cjp.enterReporter(map.get("Reporter")))
                        if(cjp.enterDescription(map.get("Description")))
                            return true;
        }catch (Exception e){
            return false;
        }
        return false;
    }

    public boolean openJiraTask(Map<String,String> map){
        CreateJiraPage cjp=new CreateJiraPage();
        try{
            if (cjp.enterInGlobalSearch(map.get("JiraTaskKey")))
                if(cjp.clickJiraTask())
                    if (cjp.clickSubtaskButton())
                        return true;
        }catch (Exception e){
            GZLogger.Log("Unable to open the Jira task successfully");
            return false;
        }
        return false;
    }

    public boolean openJiraIssue(Map<String,String> map){
        CreateJiraPage cjp=new CreateJiraPage();
        try{
            if (cjp.enterInGlobalSearch(map.get("JiraTaskKey")))
                if(cjp.clickJiraTask())
                        return true;
        }catch (Exception e){
            GZLogger.Log("Unable to open the Jira task successfully");
            return false;
        }
        return false;
    }

    public boolean fillSubtaskForm(Map<String,String> map){
        CreateJiraPage cjp=new CreateJiraPage();
        try {
            if (cjp.checkHeaderOfSubtaskForm())
                if(cjp.enterSummary(map.get("Summary")))
                    if(cjp.enterReporter(map.get("Reporter")))
                        if(cjp.enterDescription(map.get("Description")))
                            return true;
        }catch (Exception e){
            GZLogger.Log("Not able to fill Create Subtask Form");
            return false;
        }
        return false;
    }

    public boolean enterSaveBtn(){
        CreateJiraPage cjp=new CreateJiraPage();
        try{
            if(cjp.clickSaveBtn())
                return true;
        }catch (Exception e){
            return false;
        }
        return false;
    }

    public boolean editJira(Map<String,String> map){
        CreateJiraPage cjp=new CreateJiraPage();
        try{
            if(cjp.clickEditJiraBtn())
                if (cjp.enterSummary(map.get("Summary")))
                    if (cjp.enterDescription(map.get("Description"))){
                        GZLogger.PassLog("Edited the Jira issue successfully");
                        return true;
                    }
        }catch (Exception e){
            GZLogger.FailLog("Not able to edit the Jira issue");
            return false;
        }
        return false;
    }

    public boolean saveCommentAtCreateJira(Map<String,String> map){
        CreateJiraPage cjp=new CreateJiraPage();
        try{
            if(cjp.clickSendCommentAttchBtn())
                if (cjp.clickAddCommentBtn())
                    if (cjp.enterComment(map.get("CaseComment")))
                        if (cjp.clickSaveBtnComment())
                            if (cjp.getText(map))
                                if (cjp.clickSendToJiraBtn()){
                                    GZLogger.PassLog("Able to create a JIRA issue with Comment");
                                    return true;
                                }
        }catch (Exception e){
            GZLogger.FailLog("Not able to create a JIRA issue with Comment");
            return false;
        }
        return false;
    }

    public boolean verifyUIOnAddCommentModal(){
        CreateJiraPage cjp=new CreateJiraPage();
        try{
            if(cjp.clickSendCommentAttchBtn())
                if (cjp.isAddCommentFileHeaderVisible())
                    if (cjp.isAddCommentBtnVisible())
                        if(cjp.isSyncCaseComntCheckboxVisible())
                            if(cjp.isBackBtnVisible())
                                if (cjp.isSendToJIRABtnVisible())
                                    if (cjp.isSyncAttchSObjCheckboxVisible()){
                                        GZLogger.PassLog("All Elements are visible on the modal");
                                        return true;
                                    }
        }catch (Exception e){
            GZLogger.FailLog("Not able to view all elements on the Modal");
            return false;
        }
        return false;
    }

    public boolean saveFileAtCreateJira(Map<String,String> map){
        CreateJiraPage cjp=new CreateJiraPage();
        try{
            if(cjp.clickSendCommentAttchBtn())
                if(cjp.clickUploadFileBtn())
                    if (cjp.clickSendToJiraBtn()){
                        GZLogger.PassLog("Able to create a JIRA issue with Comment");
                        return true;
                    }
        }catch (Exception e){
            GZLogger.FailLog("Not able to create a JIRA issue with Comment");
            return false;
        }
        return false;
    }

    public boolean potentialResults(Map<String,String> map){
        CreateJiraPage cjp=new CreateJiraPage();
        try{
            if (cjp.enterSummary(map.get("Summary")))
                if (cjp.potentialResultsCount()){
                    GZLogger.PassLog("Able to find the Potential Results");
                    return true;
                }
        }catch (Exception e){
            GZLogger.FailLog("Not able to find the Potential Results");
            return false;
        }
        return false;
    }

    public String  captureJiraNumberFromNavBar(){
        CreateJiraPage cjp=new CreateJiraPage();
            return cjp.getJiraNumberFromNavBar();
            /*fetching and returning the Jira Number*/
    }

    public boolean verifyCapturedJiraKeyInRelatedList() {
        CreateJiraPage cjp = new CreateJiraPage();
        try {
            if (cjp.iterateListOfJiraNumber())
                /*fetching and comparing the Jira Number*/
                return true;
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    public String  findAndLinkIssue(Map<String,String> map){
        CreateJiraPage cjp=new CreateJiraPage();
        SearchJiraPage sjp=new SearchJiraPage();
        try {
            if(cjp.enterSummary(map.get("Summary")))
                if (cjp.getLinkedJiraNumber()!=null){
                    String jNum=cjp.getLinkedJiraNumber();
                    if(cjp.clickLinkBtn())
                        if (cjp.verifySuccessMsg()){
                            GZLogger.ActivityLog("The Jira is linked successfully");
                            if (sjp.clickJiraIssueTab())
                                if(verifyLinkedJiraKeyInRelatedList(jNum))
                            return jNum;
                        }
                }
        }catch (Exception e){
            GZLogger.FailLog("The Jira is not Linked successfully");
            return null;
        }
        return null;
    }

    public boolean verifyLinkedJiraKeyInRelatedList(String jiraNum1){
        CreateJiraPage cjp = new CreateJiraPage();
        try {
            if (cjp.iterateListOfLinkedJiraNumber(jiraNum1))
                /*fetching and comparing the Jira Number*/
                return true;
            return false;
        } catch (Exception e) {
            return false;
        }

    }

    public boolean unlinkJiraFromRelatedList(){
        CreateJiraPage cjp = new CreateJiraPage();
        SearchJiraPage sjp = new SearchJiraPage();
        try{
            if(sjp.clickJiraIssueTab())
                if (cjp.clickRefreshBtn())
                    if (cjp.clickShowActionBtn())
                        if (cjp.clickUnlinkBtn()){
                            GZLogger.ActivityLog("Unlinked the Jira successfully");
                            return true;
                        }
        }catch (Exception e){
            GZLogger.ActivityLog("Not able to Unlink the Jira successfully");
            return false;
        }
        return false;
    }

    public boolean verifyUnlinkedJiraFromRelatedList(String jiraNum1){
        CreateJiraPage cjp = new CreateJiraPage();
        try{
            if(cjp.clickRefreshBtn())
                if (cjp.verifyUnlinkedJiraInRelatedList(jiraNum1)){
                    GZLogger.ActivityLog("Jira is unlinked from the Related list successfully");
                    return true;
                }
        }catch (Exception e){
            GZLogger.ActivityLog("Jira was not unlinked from the Related list successfully");
            return false;
        }
        return false;
    }

    public boolean changeJiraStatusTransition(){
        CreateJiraPage cjp = new CreateJiraPage();
        try{
            if (cjp.isEditIconVisibleOnJiraStatus())
                if(cjp.clickEditToChangeStatus())
                    if (cjp.clickJiraStatusDropdwn())
                        if (cjp.clickInProgressJiraStatus()){
                            GZLogger.ActivityLog("Able to change the status to In-progress");
                            return true;
                        }
        }catch (Exception e){
            GZLogger.FailLog("Not able to change the status to In-progress");
            return false;
        }
        return false;
    }

    public boolean openJiraIssueFromRelatedList(){
        try{
            CreateJiraPage cjp = new CreateJiraPage();
            SearchJiraPage sjp = new SearchJiraPage();
            if (sjp.clickJiraIssueTab())
                if (cjp.clickShowActionBtn())
                    if (cjp.clickShowDetailsOption()){
                        GZLogger.ActivityLog("Show Details option is clicked");
                        return true;
                    }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Show Details option is not clicked");
            return false;
        }
        return false;
    }

public boolean openCreateJiraFormAndVerifyErrMsg(String err){
    try{
        CreateJiraPage cjp=new CreateJiraPage();
        if(cjp.clickCreateJiraBtn())
            if(cjp.verifyOneOneErrMsg(err))
                return true;
    }catch (Exception e){
        GZLogger.ErrorLog(e,"Not able to open create Jira form");
        return false;
    }
    return false;
}

public boolean openRelatedListAndVerifyJiraKey(String key){
        try{
            SearchJiraPage sjp = new SearchJiraPage();
            if (sjp.clickJiraIssueTab())
                if (verifyLinkedJiraKeyInRelatedList(key))
                    return true;
        }catch (Exception e){
            return false;
        }
        return false;
}

public boolean openViewInJira(){
        try {
            CreateJiraPage cjp=new CreateJiraPage();
            if (cjp.clickJiraTab())
                if (cjp.clickOnViewInJiraLink())
                    return true;
        }catch (Exception e){
            return false;
        }
        return false;
}

public boolean checkJiraKey(String key){
        try{
            CreateJiraPage cjp=new CreateJiraPage();
            if(cjp.isJiraKeyDisplayed(key))
                return true;
        }catch (Exception e){
            return false;
        }
        return false;
}

public boolean saveCaseCommentInSalesforce(String caseComment){
        try {
            CreateJiraPage cjp=new CreateJiraPage();
            if (cjp.openCaseCommentBox())
                if (cjp.enterCaseComment(caseComment))
                    return true;
        }catch (Exception e){
            return false;
        }
        return false;
}

public boolean findCaseCommentOnJiraSide(String expectedComment){
        try{
            CreateJiraPage cjp=new CreateJiraPage();
            if (cjp.isCommentVisibleOnJira(expectedComment))
                return true;
        }catch (Exception e){
            return false;
        }
        return false;
}

public boolean saveJiraIssueCommentAtSFSide(String comment){
        try {
            CreateJiraPage cjp=new CreateJiraPage();
            if (cjp.clickNewCommentBtn())
                if (cjp.enterNewJiraCommentSF(comment))
                    return true;
        }catch (Exception e){
            return false;
        }
        return false;
}

public boolean commentOnJiraIssueAtJiraPage(String comment, String jiraKey){
        try{
            CreateJiraPage cjp=new CreateJiraPage();
            if (cjp.searchJiraIssueAtJiraPage(jiraKey))
                if (cjp.writeJiraCommentOnJiraIssue(comment))
                    return true;
        }catch (Exception e){
            return false;
        }
        return false;
}


















}
