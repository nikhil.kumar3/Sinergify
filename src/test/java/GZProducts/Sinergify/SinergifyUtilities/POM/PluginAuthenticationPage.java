package GZProducts.Sinergify.SinergifyUtilities.POM;

import CommonUtilities.GZBrowser.GZBrowser;
import CommonUtilities.GZCommonFuncs.GZCommonElementFuncs;
import CommonUtilities.GZLogger.GZLogger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PluginAuthenticationPage extends GZCommonElementFuncs {

    public PluginAuthenticationPage(){
        PageFactory.initElements(GZBrowser.getDriver(), this);
    }

    @FindBy(xpath = "//input[@id='login-form-username']")
    WebElement usernameInput;
    public boolean enterUsername(String uname){
        try{
            GZWait(3,usernameInput,10);
            if (usernameInput.isDisplayed()){
                usernameInput.sendKeys(uname);
                GZLogger.ActivityLog("Entered the username");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to enter the username");
            return false;
        }
        return false;
    }
    @FindBy(xpath = "//input[@id='login-form-password']")
    WebElement pwdInput;
    public boolean enterPassword(String pwd){
        try{
            GZWait(3,pwdInput,10);
            if (pwdInput.isDisplayed()){
                pwdInput.sendKeys(pwd);
                GZLogger.ActivityLog("Entered the login password");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to enter the password");
            return false;
        }
        return false;
    }

    @FindBy(xpath = "//input[@id='login']")
    WebElement loginBtn;
    public boolean clickLoginBtn(){
        try{
            GZWait(3,loginBtn,10);
            if (loginBtn.isDisplayed()){
                loginBtn.click();
                GZLogger.ActivityLog("Clicked on the Login button");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Login button");
            return false;
        }
        return false;
    }
    @FindBy(xpath = "//div[@class='aui-header-secondary']/ul/li[@id='system-admin-menu']/a[@title='Administration']")
    WebElement settingIcon;
    public boolean isSettingIconVisible(){
        try{
            GZWait(3,settingIcon,15);
            if (settingIcon.isDisplayed() && settingIcon.isEnabled()){
                GZLogger.ActivityLog("Setting icon is visible");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Setting icon is not visible");
            return false;
        }
        return false;
    }
    public boolean  clickSettingIcon(){
        try {
            if (isSettingIconVisible()){
                settingIcon.click();
                GZLogger.ActivityLog("Clicked on the Setting icon");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Setting icon");
            return false;
        }
        return false;
    }

    @FindBy(xpath = "//div[@class='aui-header-secondary']/ul/li[@id='system-admin-menu']/a[@title='Administration']/following-sibling::div//li/a[contains(text(),'Manage apps')]")
    WebElement manageAppsOption;
    public boolean clickManageAppsOption(){
        try{
            GZWait(3,manageAppsOption,10);
            if (manageAppsOption.isDisplayed() && manageAppsOption.isEnabled()){
                manageAppsOption.click();
                GZLogger.ActivityLog("Clicked on Manage Apps option");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Manage App option");
            return false;
        }
        return false;
    }

    @FindBy(xpath = "//input[@name='webSudoPassword']")
    WebElement sudoPwd;
    public boolean enterSudoPassword(String pwd){
        try{
            GZWait(3,sudoPwd,10);
            if (sudoPwd.isDisplayed() && sudoPwd.isEnabled()){
                sudoPwd.sendKeys(pwd);
                GZLogger.ActivityLog("Entered the password again");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to enter the Sudo password");
            return false;
        }
        return false;
    }
    @FindBy(xpath = "//input[@id='login-form-submit']")
    WebElement confirmBtn;
    public boolean clickConfirmBtn(){
        try{
            GZWait(3,confirmBtn,10);
            if (confirmBtn.isEnabled()){
                confirmBtn.click();
                GZLogger.ActivityLog("Clicked on Confirm button");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on Confirm button");
            return false;
        }
        return false;
    }
    @FindBy(xpath = "//a[@id='admin-search-link']")
    WebElement adminSearchInput;
    public boolean isAdminSearchBoxVisible(){
        try{
            GZWait(3,adminSearchInput,15);
            if (adminSearchInput.isDisplayed() && adminSearchInput.isEnabled()){
                GZLogger.ActivityLog("Admin Search box is visible on the page");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Admin Search box is not visible on the page");
            return false;
        }
        return false;
    }
    public boolean clickSearchJiraAdminBox(){
        try{
            if (isAdminSearchBoxVisible()){
                adminSearchInput.click();
                GZLogger.ActivityLog("Clicked on the Search Jira Admin box");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Search Jira Admin box");
            return false;
        }
        return false;
    }
    @FindBy(xpath = "//input[@id='shifter-dialog-field']")
    WebElement shifterDialogInputBox;

    public boolean searchPage(String pageName){
        try{
            GZWait(3,shifterDialogInputBox,10);
            if (shifterDialogInputBox.isEnabled()){
                shifterDialogInputBox.sendKeys(pageName);
                GZLogger.ActivityLog("Entered the"+" "+pageName+" "+"in the search box");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to enter the"+" "+pageName+" "+"in the search box");
            return false;
        }
        return false;
    }

    @FindBy(xpath = "//div[@id='shifter-dialog']/form//div[last()]/ul/li[1]")
    WebElement authenticationOption;
    public boolean clickAuthenticationOption(){
        try{
            GZWait(3,authenticationOption,10);
            if (authenticationOption.isDisplayed()){
                authenticationOption.click();
                GZLogger.ActivityLog("Clicked on the Sinergify Authentication option");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Sinergify Authentication option");
            return false;
        }
        return false;
    }
    @FindBy(xpath = "//input[@id='inst']")
    WebElement sfInstanceInput;
    public boolean enterSfInstanceUrl(String url){
        try{
            GZWait(3,sfInstanceInput,10);
            if (sfInstanceInput.isDisplayed()){
                sfInstanceInput.clear();
                sfInstanceInput.sendKeys(url);
                GZLogger.ActivityLog("Entered the"+" "+url);
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to enter the Url");
            return false;
        }
        return false;
    }
    @FindBy(xpath = "//input[@id='cid']")
    WebElement clientIdInput;
    public boolean enterClientKey(String cKey){
        try{
            GZWait(3,clientIdInput,10);
            if (clientIdInput.isDisplayed()){
                clientIdInput.clear();
                clientIdInput.sendKeys(cKey);
                GZLogger.ActivityLog("Entered the"+" "+cKey);
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to enter the Client Key");
            return false;
        }
        return false;
    }
    @FindBy(xpath = "//input[@id='ckey']")
    WebElement clientSecretInput;
    public boolean enterClientSecret(String cSecret){
        try{
            GZWait(3,clientSecretInput,10);
            if (clientSecretInput.isDisplayed()){
                clientSecretInput.clear();
                clientSecretInput.sendKeys(cSecret);
                GZLogger.ActivityLog("Entered the"+" "+cSecret);
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to enter the Client Secret");
            return false;
        }
        return false;
    }
    @FindBy(xpath = "//button[contains(@class,'Asavebtn') and contains(text(),'Login to Salesforce')]")
    WebElement loginInSfBtn;
    public boolean clickLoginInSfBtn(){
        try{
            GZWait(3,loginInSfBtn,10);
            if (loginInSfBtn.isEnabled()){
                loginInSfBtn.click();
                GZWait(0);
                GZLogger.ActivityLog("Clicked on the Login in Salesforce button");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on Login in Salesforce Button");
            return false;
        }
        return false;
    }
    @FindBy(xpath = "//input[@id='username']")
    WebElement unameSf;
    public boolean enterUserNameSf(String uname){
        try{
            GZWait(3,unameSf,15);
            if (unameSf.isDisplayed()){
                unameSf.clear();
                unameSf.sendKeys(uname);
                GZLogger.ActivityLog("Entered the username in SF login page");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to enter the username in SF login page");
            return false;
        }
        return false;
    }

    @FindBy(xpath = "//input[@id='password']")
    WebElement pwdSf;
    public boolean enterPasswordSf(String pwd){
        try{
            GZWait(3,pwdSf,15);
            if (pwdSf.isDisplayed()){
                pwdSf.clear();
                pwdSf.sendKeys(pwd);
                GZLogger.ActivityLog("Entered the password in SF login page");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to enter the password in SF login page");
            return false;
        }
        return false;
    }
    @FindBy(xpath = "//input[@id='Login']")
    WebElement loginBtnSf;

    public boolean clickLoginButtonSF(){
        try {
            GZWait(3,loginBtnSf,10);
            if (loginBtnSf.isEnabled()){
                loginBtnSf.click();
                GZLogger.ActivityLog("Clicked on the the Login button");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on Login button");
            return false;
        }
        return false;
    }
    @FindBy(xpath = "//input[@id='oaapprove']")
    WebElement allowBtn;
    @FindBy(xpath = "//span[@id='status']")
    WebElement sfStatus;
    public boolean verifyConnectionStatus(){
        try {
            GZWait(3,sfStatus,15);
            if (sfStatus.isDisplayed()){
                String status=sfStatus.getText();
                if (status.equalsIgnoreCase("Connected")){
                    GZLogger.ActivityLog("Authentication is success");
                    return true;
                }
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to get the Status");
            return false;
        }
        return false;
    }




}
