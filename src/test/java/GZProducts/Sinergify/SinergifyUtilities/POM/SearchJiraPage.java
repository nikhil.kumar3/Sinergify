package GZProducts.Sinergify.SinergifyUtilities.POM;

import CommonUtilities.GZBrowser.GZBrowser;
import CommonUtilities.GZCommonFuncs.GZCommonElementFuncs;
import CommonUtilities.GZLogger.GZLogger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SearchJiraPage extends GZCommonElementFuncs {
    public SearchJiraPage(){
        PageFactory.initElements(GZBrowser.getDriver(), this);
    }

    @FindBy(xpath = "//a[normalize-space()='Jira Issue']")
    WebElement jiraIssueTab;
    @FindBy(xpath = "//button[@title='Search Jira']")
    WebElement searchJiraBtn;
    @FindBy(xpath = "//button[@title='Search']")
    WebElement searchBtn;
    @FindBy(xpath = "//span[normalize-space()='View Detail']")
    WebElement viewJiraDetailTitle;
    @FindBy(xpath = "//button[normalize-space()='Cancel']")
    WebElement cancelBtn;
    @FindBy(xpath = "//span[normalize-space()='SearchJira']")
    WebElement searchJiraTabTitle;
    @FindBy(xpath = "//input[@placeholder='Start typing to get a list of possible matches.' and @name='txt']")
    WebElement searchInputBox;
    @FindBy(xpath = "//div[contains(@class,'SearchJiraIssue')]/div[last()]/div[last()]")
    WebElement searchGrid;
    @FindBy(xpath = "//div[contains(@class,'SearchJiraIssue')]/div[last()-1]/div[1]")
    WebElement jiraInstance;
    @FindBy(xpath = "//div[contains(@class,'SearchJiraIssue')]/div[last()]/div[1]/div[last()]//section[1]")
    WebElement firstProjectName;

    public boolean isJiraIssueTabVisible(){
        try{
            GZWait(3,jiraIssueTab,20);
            if (jiraIssueTab.isDisplayed()){
                GZLogger.ActivityLog("Jira Issue tab is visible");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Jira Issue tab is not visible");
            return false;
        }
        return false;
    }

    public boolean isSearchJiraBtnVisible(){
        try{
            GZWait(3,searchJiraBtn,20);
            if (searchJiraBtn.isDisplayed()){
                GZLogger.ActivityLog("Search Jira button is visible");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Search Jira button is not visible");
            return false;
        }
        return false;
    }

    public boolean isSearchBtnVisible(){
        try{
            GZWait(3,searchBtn,10);
            if (searchBtn.isDisplayed()){
                GZJSEleClick(searchBtn);
                GZLogger.ActivityLog("Search button is visible on the page");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Search button is not visible");
            return false;
        }
        return false;
    }


    public boolean clickSearchBtn(){
        try{
            GZWait(3,searchBtn,10);
            if (searchBtn.isDisplayed()){
                searchBtn.click();

                GZLogger.ActivityLog("Clicked on the Search button on the page");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Search button");
            return false;
        }
        return false;
    }

    public boolean isSearchJiraTitleVisible(){
        try{
            GZWait(3,searchJiraTabTitle,20);
            if (searchJiraTabTitle.isDisplayed()){
                GZLogger.ActivityLog("SearchJira tab is visible");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to navigate to the SearchJira tab");
            return false;
        }
        return  false;
    }

    public boolean clickSearchJiraBtn(){
        try{
            GZWait(0);
            if (searchJiraBtn.isDisplayed()){
                GZJSEleClick(searchJiraBtn);
//              searchJiraBtn.click(); /*this click did not work so that we have used JS click method*/
                GZLogger.ActivityLog("Clicked on the Search Jira button");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Search Jira button");
            return false;
        }
        return false;
    }

    public boolean clickJiraIssueTab(){
        try{
            GZWait(0);
            GZWait(3,jiraIssueTab,20);
            if (jiraIssueTab.isDisplayed()){
                GZJSEleClick(jiraIssueTab);
                GZLogger.ActivityLog("Clicked on the Jira Issue tab");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Jira Issue tab");
            return false;
        }
        return false;
    }

    public boolean searchJiraKey(String jiraKey){
        try{
            GZWait(0);
            GZWait(3,searchInputBox,12);
            if (searchInputBox.isDisplayed()){
                searchInputBox.sendKeys(jiraKey);
                GZLogger.ActivityLog("Entered the Jira issue key");
                if(clickSearchBtn()){
                    GZLogger.ActivityLog("Clicked on the Search button");
                    GZWait(3,searchGrid,10);
                    GZWait(0);
                    String text=searchGrid.getText();
                    if (text.contains(jiraKey)){
                        GZLogger.ActivityLog("Found the entered"+" "+jiraKey+" "+"in the search grid");
                        return true;
                    }
                }
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to enter the Jira issue in Search Box");
            return false;
        }
        return false;
    }

    public boolean getJiraInstanceText(String instance){
        try{
            GZWait(0);
            if (jiraInstance.isDisplayed()){
                String instanceName=jiraInstance.getText();
                if(instanceName.contains(instance)){
                    GZLogger.ActivityLog("Found the same Jira Instance as"+" "+instance+" "+"on Search Jira page");
                    return true;
                }
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to see the Jira instance");
            return false;
        }
        return false;
    }

    public boolean getProjectName(String projectName){
        try{
            GZWait(0);
            if (firstProjectName.isDisplayed()){
                String pName=firstProjectName.getText();
                if(pName.contains(projectName)){
                    GZLogger.ActivityLog("Found the same Project Name as"+" "+projectName+" "+"on Search Jira page");
                    return true;
                }
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to see the Project name");
            return false;
        }
        return false;
    }

    @FindBy(xpath = "//div[contains(@class,'SearchJiraIssue')]/div[last()]/div[last()]//table/tbody/tr[1]/th//button")
    WebElement showMoreActionButtonAtSearchPage;

    @FindBy(xpath = "//div[contains(@class,'SearchJiraIssue')]/div[last()]/div[last()]//table/tbody/tr[1]/th//button/following-sibling::div//lightning-menu-item[last()]/a")
    WebElement linkButtonOption;

    public boolean clickShowMoreActionButton(){
        try{
            GZWait(3,showMoreActionButtonAtSearchPage,10);
            if (showMoreActionButtonAtSearchPage.isEnabled()){
                showMoreActionButtonAtSearchPage.click();
                GZLogger.ActivityLog("Clicked on the Show more action button");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Show more action button");
            return false;
        }
        return false;
    }

    public boolean linkJira(){
        try {
            GZWait(0);
            clickSecondOptionInList(showMoreActionButtonAtSearchPage,linkButtonOption);
            GZLogger.ActivityLog("Clicked on the Link button");
            return true;
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Link button");
            return false;
        }
    }

    public boolean checkIfLinkButtonOptionIsDisabled(){
        try {
            GZWait(3,linkButtonOption,10);
            if (linkButtonOption.isDisplayed()){
                GZLogger.ActivityLog("Able to view the Link option");
                String ariaValue=linkButtonOption.getAttribute("aria-disabled");
                if (ariaValue.equalsIgnoreCase("True")){
                    GZLogger.ActivityLog("Found the Link option as Disabled");
                    return true;
                }
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to find the Link option");
            return false;
        }
        return false;
    }

    //@FindBy(xpath = "//button[normalize-space()='Clone']")
    @FindBy(xpath = "//*[@class='slds-button-group']/slot//button[contains(text(),'Clone')]")
    WebElement cloneBtn;

    public boolean clickCloneBtn(){
        try{
            GZWait(3,cloneBtn,10);
            if (cloneBtn.isDisplayed() && cloneBtn.isEnabled()){
                GZWait(0);
                GZWait(0);
                cloneBtn.click();
                GZLogger.ActivityLog("Clicked on the Clone button on the page");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Clone button");
            return false;
        }
        return false;
    }

    public boolean clickViewDetailOption(){
        try{
            GZWait(3,viewJiraDetailTitle,10);
            if (viewJiraDetailTitle.isDisplayed()){
                viewJiraDetailTitle.click();
                GZLogger.ActivityLog("Clicked on the View Detail option on the search page");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the  View Detail option on the search page");
            return false;
        }
        return false;
    }
    @FindBy(xpath = "//footer/div[contains(text(),'Save')]")
    WebElement saveBtn;
    @FindBy(xpath = "//div[@data-key='success']")
    WebElement instanceSuccessMessage;

    public boolean clickSaveBtn(){
        try{
            GZWait(3,saveBtn,10);
            if (saveBtn.isDisplayed()){
                saveBtn.click();
                GZLogger.ActivityLog("Clicked on the Save button");
                GZWait(3,instanceSuccessMessage,20);
                if (instanceSuccessMessage.isDisplayed()){
                    GZLogger.ActivityLog("New Jira Issue is created");
                    return true;
                }
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Save button - Create a New Jira Issue");
            return false;
        }
        return false;
    }






}
