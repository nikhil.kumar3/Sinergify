package GZProducts.Sinergify.SinergifyUtilities.POM;

import CommonUtilities.GZBrowser.GZBrowser;
import CommonUtilities.GZCommonFuncs.GZCommonElementFuncs;
import CommonUtilities.GZLogger.GZLogger;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.*;
import java.util.NoSuchElementException;

public class AdminSettingPage extends GZCommonElementFuncs{
    public AdminSettingPage(){
        PageFactory.initElements(GZBrowser.getDriver(), this);
    }


    @FindBy(xpath = "//div[contains(@class,'appLauncher') and @aria-label='App']//button")
    //@FindBy(xpath = "//*[contains(@class,'salesforceIdentityAppLauncherHeader')]")
    WebElement appLauncherIcon;
    //slds-icon-waffle
    @FindBy(xpath = "//div[@class='multicontainer']/div[5]")
    WebElement addNewInstanceLink;
    //div[@class='EvenClass light-card-custom currentInstance']/table/tbody/tr/td[2]
    @FindBy(xpath = "//input[@placeholder='Search apps and items...']")
    //@FindBy(xpath = "//input[@class='slds-input']")
    WebElement searchAppBox;
    @FindBy(xpath = "//div[@class='al-menu-dropdown-list']/descendant::img")
    WebElement sinergifySearchResult;
    @FindBy(xpath = "//span[contains(text(), 'Admin Setting')]/parent::a[@title='Admin Setting']")
    WebElement adminSettingsTab;
    @FindBy(xpath = "//lightning-tab[@aria-labelledby='Basic__item']/slot/div/lightning-input[@data-id='jiraname']/div/input[@placeholder='JIRA Instance Name']")
    WebElement jiraNameInBasicAuth;
    @FindBy(xpath = "//lightning-tab[@aria-labelledby='Auth__item']/slot/div/lightning-input[@data-id='jiraname']/div/input[@placeholder='JIRA Instance Name']")
    WebElement jiraNameInOauth;
    @FindBy(xpath="//div[contains(text(),'Default Instance')]/following::span[1]")
    WebElement defaultInstance;
    @FindBy(xpath="//div[@class='slds-grid slds-gutters'][2]/div/div/lightning-helptext/following::span[1]/span[2]")
    WebElement status;
    @FindBy(xpath = "//div[@class='slds-grid slds-gutters'][2]/div/div/lightning-helptext/following::span[1]/span[3]")
    WebElement statusInactive;
    @FindBy(xpath = "//lightning-tab[@aria-labelledby='Basic__item']/slot/div//input[@name='endpoint']")
    WebElement jiraUrl;
    @FindBy(xpath = "//lightning-tab[@aria-labelledby='Auth__item']/slot/div//input[@name='endpoint']")
    WebElement jiraUrlOauth;
    @FindBy(xpath = "//lightning-tab[@aria-labelledby='Basic__item']/slot/div[2]/div/div/lightning-input/div/input[@type='text']")
    WebElement jiraUsername;
    @FindBy(xpath = "//lightning-tab[@aria-labelledby='Basic__item']/slot/div[2]/div/div/lightning-input/div/input[@type='password']")
    WebElement jiraPassword;
    @FindBy(xpath ="//div[@class='instancechange_inner']")
    WebElement instanceChangeIcon;
    @FindBy(id = "YesbasicOauth")
    WebElement rdbBasicAuthYes;
    @FindBy(xpath = "//li[@data-label='OAuth Authentication']")
    WebElement rdbOAuthYes;
    @FindBy(xpath = "//div[@class='oneAlohaPage']/descendant::iframe")
    WebElement frmAdminSettings;
    @FindBy(xpath = "//lightning-combobox[@class='cursorclick slds-form-element']/div/lightning-base-combobox[@class='slds-combobox_container']")
    WebElement piklistSfjira;
    @FindBy(xpath="//lightning-combobox[@class='cursorclick slds-form-element']/div/lightning-base-combobox[@class='slds-combobox_container']")
    WebElement sfJiraPicklist;
    @FindBy(xpath = "//*[@class='slds-p-around_small cofigcontainer']/div[1]/div[1]/div/label/span/span[contains(text(),'Disabled')]")
    WebElement enableFeedSyncYes;
    @FindBy(xpath = "//*[@class='slds-p-around_small cofigcontainer']/div[1]/div[1]/div/label/span/span[2]")
    WebElement enableFeedSyncNo;
    @FindBy(xpath = "//*[@class='slds-p-around_small cofigcontainer']/div[2]/div[1]/div/label/span/span[2]")
    WebElement rdbEnblEdtYes;
    @FindBy(xpath="//*[@class='slds-p-around_small cofigcontainer']/div[2]/div[2]/div/div/label/span/span[2]")
    WebElement rdbEnblStatusTransSfYes;
    @FindBy(xpath="//*[@class='slds-p-around_small cofigcontainer']/div[2]/div[2]/div/div/label/span/span[3]")
    WebElement rdbEnblStatusTransSfNo;
    @FindBy(xpath = "//*[@class='slds-p-around_small cofigcontainer']/div[2]/div[1]/div/label/span/span[3]")
    WebElement rdbEnblEdtNo;
    @FindBy(xpath = "//div[@class='slds-grid slds-gutters']/div[2]/div/label[@class='slds-checkbox_toggle slds-grid']/span/span[contains(text(),'Cloud')]")
    WebElement rdbJiraswClud;
    @FindBy(xpath = "//div[@class='slds-grid slds-gutters']/div[2]/div/label[@class='slds-checkbox_toggle slds-grid']/span/span[contains(text(),'Server')]")
    WebElement rdbJiraswSrvr;
    @FindBy(xpath = "//*[@class='slds-p-around_small cofigcontainer']/div[1]/div[2]/div[1]/label/span/span[contains(text(),'Disabled')]")
    WebElement rdbAutosyncComntYes;
    @FindBy(xpath = "//*[@class='slds-p-around_small cofigcontainer']/div[1]/div[2]/div[1]/label/span/span[2]")
    WebElement rdbAutosyncComntNo;
    @FindBy(xpath = "//*[@class='slds-p-around_small cofigcontainer']/div[2]/div[3]/div/label/span/span[3]")
    WebElement rdbAutosyncAttchYes;
    @FindBy(xpath = "//*[@class='slds-p-around_small cofigcontainer']/div[2]/div[3]/div/label/span/span[2]")
    WebElement rdbAutosyncAttchNo;
    @FindBy(xpath ="//*[@class='slds-p-around_small cofigcontainer']/div[1]/div[2]/div[2]/fieldset[3]")
    WebElement rdbTypcomntSyncBoth;
    @FindBy(xpath = "//*[@class='slds-p-around_small cofigcontainer']/div[1]/div[2]/div[2]/fieldset[1]")
    WebElement rdbTypcomntSyncPblc;
    @FindBy(xpath = "//*[@class='slds-p-around_small cofigcontainer']/div[1]/div[2]/div[2]/fieldset[2]")
    WebElement rdbTypcomntSyncPvt;
    @FindBy(xpath = "//*[@class='slds-p-around_small cofigcontainer']/div[1]/div[3]/div[1]/div/label/span/span[3]")
    WebElement rdbSyncJrcmtSfYes;
    @FindBy(xpath = "//*[@class='slds-p-around_small cofigcontainer']/div[1]/div[3]/div[1]/div/label/span/span[2]")
    WebElement rdbSyncJrcmtSfNo;
    @FindBy(xpath = "//*[@class='slds-p-around_small cofigcontainer']/div[1]/div[3]/div[2]/div[2]/div/label/span/span[3]")
    WebElement rdbSyncCasecmntYes;
    @FindBy(xpath = "//*[@class='slds-p-around_small cofigcontainer']/div[1]/div[3]/div[2]/div[2]/div/label/span/span[2]")
    WebElement rdbSyncCasecmntNo;
    @FindBy(xpath = "//*[@class='slds-p-around_small cofigcontainer']/div[1]/div[3]/div[2]/div[1]/fieldset[1]")
    WebElement rdbCmntSavPblc;
    @FindBy(css = "//*[@class='slds-p-around_small cofigcontainer']/div[1]/div[3]/div[2]/div[1]/fieldset[2]")
    WebElement rdbCmntSavPvt;
    @FindBy(xpath = "//*[@class='slds-p-around_small cofigcontainer']/div[2]/div[4]/div/div/label/span/span[3]")
    WebElement rdbSynJirAttchSfYes;
    @FindBy(xpath = "//*[@class='slds-p-around_small cofigcontainer']/div[2]/div[4]/div/div/label/span/span[2]")
    WebElement rdbSynJirAttchSfNo;
    @FindBy(xpath = "//*[@class='slds-p-around_small cofigcontainer']/div[1]/div[3]/div[2]/div[3]/lightning-input/div/input")
    WebElement commentsQualifierInput;
    @FindBy(xpath = "//*[@class='slds-p-around_small cofigcontainer']/div[2]/div[5]/div[2]/lightning-input/div/input")
    WebElement attachmentQualifierInput;
    @FindBy(xpath = "//*[@class='slds-p-around_small cofigcontainer']/div[2]/div[5]/div/div/label/span/span[3]")
    WebElement syncAttachmentToSfObjectYes;
    @FindBy(xpath = "//*[@class='slds-p-around_small cofigcontainer']/div[2]/div[5]/div/div/label/span/span[2]")
    WebElement syncAttachmentToSfObjectNo;
    @FindBy(xpath = "//button[@name='Save']")
    WebElement saveBtn; // Save button on Add instance screen
    @FindBy(xpath = "//button[@name='Next']")
    WebElement nxtBtn; // Next button on Add instance screen
//    @FindBy(xpath = "//a[contains(text(),'Next')]")
//    WebElement nxtBtn1;
    @FindBy(xpath = "//span[normalize-space()='Projects']")
    WebElement projectTab;
    @FindBy(xpath = "//span[@title='Sinergify']")
    WebElement sfTitle;
    @FindBy(xpath = "//label[contains(text(),'Consumer Key')]/following::textarea[1]")
    WebElement consumrKey;
    @FindBy(xpath = "//label[contains(text(),'Private Key')]/following::textarea[1]")
    WebElement privteKey;
    @FindBy(css = "div.modal-content .btn:nth-of-type(1)")
    WebElement alertPop;
    @FindBy(xpath = "//table[@class='messageTable']//div[contains(text(),'Attempt fail to login in jira. Error:: Status:Unauthorized')]")
    WebElement errorClass;
    @FindBy(xpath = "//div[@data-key='success']")
    WebElement instanceSuccessMessage;
    @FindBy(xpath = "//div[@class='forceVisualMessageQueue']/div/div[@role='alert']")
    WebElement instanceSuccessMessageOauth;



    /*Elements & Methods of Project page*/




    @FindBy(xpath = "//div[@class='ProjectContainer slds-col']/div[6]/div[1]/div/table/tbody/tr[1]/td[1]")
    WebElement projectFirstName;

    @FindBy(xpath = "//div[@class='ProjectContainer slds-col']/div[6]/div[1]/div/table/tbody/tr[1]/td[2]")
    WebElement syncProjectCheckbox;
    @FindBy(xpath = "//div[@class='ProjectContainer slds-col']/div[6]/div[1]/div/table/tbody/tr[1]/td[3]")
    WebElement writeProjectCheckbox;
    @FindBy(xpath = "//div[@class='ProjectContainer slds-col']/div[6]/div[1]/div/table/tbody/tr[1]/td[4]")
    WebElement defaultIssueTypeDropdown;
    @FindBy(xpath = "//lightning-base-combobox-item/span[2]/span[@title='Task']")
    WebElement taskIssueType;
    @FindBy(xpath = "//div[@class='ProjectContainer slds-col']/div[6]/div[1]/div/table/tbody/tr[1]/td[5]/div/span[2]")
    WebElement issueAdderIcon;
    @FindBy(xpath = "//div[@class='CheckBoxGroups']/div/lightning-input")
    WebElement selectAllIssueTypesCheckbox;
    @FindBy(xpath = "//div[@class='ProjectContainer slds-col']/div[6]/div[1]/div/table/tbody/tr[1]/td[6]/div/lightning-icon[@title='Add Sharing Settings']")
    WebElement addShareSettingIcon;
    @FindBy(xpath = "//input[@name='Default Project']")
    WebElement defaultProjectDropdown;
    public String getProjectName(){
            GZWait(3,projectFirstName,20);
            return projectFirstName.getText();
    }
    public void clickDefaultProject(){
            try{
                GZWait(2,defaultProjectDropdown,15);
                defaultProjectDropdown.click();
                GZLogger.ActivityLog("clicked on Default project dropdown");
            }catch (Exception e){
                    GZLogger.ErrorLog(e, "Not able to click on Default project dropdown");
                }
    }

    public void selectDefaultProject(){
        GZWait(3,defaultProjectDropdown,3);
        defaultProjectDropdown.sendKeys(Keys.ARROW_DOWN);
        defaultProjectDropdown.sendKeys(Keys.RETURN);

    }
    @FindBy(xpath = "//input[@name='enter-search']")
    WebElement searchProjectInputBox;
    public void clickSearchProjectInput(){
        GZWait(2,searchProjectInputBox,15);
        searchProjectInputBox.click();
    }
    public void searchProject() throws InterruptedException {
        String projectName=getProjectName();
        GZWait(3,projectFirstName,20);
        searchProjectInputBox.sendKeys(projectName);
        GZWait(0);
        GZWait(0);
    }
    @FindBy(xpath = "//input[@name='Records per page']")
    WebElement projectPerPageDropdown;
    @FindBy(xpath = "//*[@data-target-id='PageNumber_Selector']/ul/div[@data-value='20']")
    WebElement projectPerPageDropdownValue;
    public void selectProjectPerPageDropdown(){
        GZWait(2,projectPerPageDropdown,10);
        projectPerPageDropdown.click();
        projectPerPageDropdownValue.click();
    }
    @FindBy(xpath = "//img[contains(@src,'refresh.png')]")
    WebElement refreshIconOnProjectPage;
    public void clickRefreshProject(){
        GZWait(2,refreshIconOnProjectPage,20);
        refreshIconOnProjectPage.click();

    }
    public void clickSyncCheckbox(){
        GZWait(2,syncProjectCheckbox,10);
        GZJSEleClick(syncProjectCheckbox);
        syncProjectCheckbox.click();
    }
    public void clickWriteCheckbox(){
        GZWait(2,writeProjectCheckbox,10);
        writeProjectCheckbox.click();
    }
    public void clickDefaultIssueTypeDropdown(){
        GZWait(2,defaultIssueTypeDropdown,10);
        defaultIssueTypeDropdown.click();
    }
    public void selectDefaultIssueType(){
//        piklistSfjira.sendKeys(Keys.ARROW_DOWN);
//        taskIssueType.sendKeys(Keys.RETURN);
        WebDriver driver=GZBrowser.getDriver();
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", taskIssueType);

    }
    public void clickIssueAdderIcon(){issueAdderIcon.click();}
    public void clickAllIssueTypeToSync(){selectAllIssueTypesCheckbox.click();}

    /*----------------------------End-of-section------------------------------------------*/




    /*Elements & Methods of Project Mapping page*/




    /*----------------------------End-of-section------------------------------------------*/

    public boolean clickAddNewInstance(){
        try {
            GZWait(2,addNewInstanceLink,20);
            if (addNewInstanceLink.isDisplayed()){
                addNewInstanceLink.click();
                GZLogger.ActivityLog("Clicked the Add new instance link");
                return true;
            }
            return false;
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Add new instance link");
            return false;
        }
    }
    public void clickDropdown(){
        //GZJSEleClick(sfJiraPicklist);
        sfJiraPicklist.click();
    }
    public String verifyToggleStatus(){
        String toggleStatus=rdbEnblEdtYes.getText();
        System.out.println(toggleStatus);
        return toggleStatus;
    }
    public boolean clickAlertOk(){
        boolean status=false;
        try{
            GZWait(3, alertPop,30);
            GZJSEleClick(alertPop);
            status=true;
            GZLogger.ActivityLog("Clicked OK button on the pop up");
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click OK button on the pop up");
        }
        return status;
    }
    public void clickStatusToggle() {
        try {
            GZWait(2, status, 30);
            status.click();
            GZLogger.ActivityLog("Clicked the Status toggle button on Admin setting screen");
        }catch (Exception e) {
            GZLogger.ErrorLog(e,"Not able to click on the Status toggle button on Admin setting");

        }
    }
    public void clickInactiveStatusToggle() {
        try {
            GZWait(0);
            GZWait(2, statusInactive, 30);
            statusInactive.click();
            GZLogger.ActivityLog("Clicked the Inactive Status toggle button on Admin setting screen");
        }catch (Exception e) {
            GZLogger.ErrorLog(e,"Not able to click on the Status toggle button on Admin setting");

        }
    }
    public boolean clickAppLauncher(){
        try {
            GZWait(0);
            GZWait(3, appLauncherIcon, 40);
            if(appLauncherIcon.isDisplayed()){
                appLauncherIcon.click();
                GZLogger.ActivityLog("Clicked on the App launcher icon in lightning");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Unable to click on the App launcher icon in lightning");
            return false;
        }
        return false;
    }


    public boolean setAppName(String appName){
        try {
            GZWait(0);
            GZWait(2, searchAppBox, 60);
            if (searchAppBox.isDisplayed()){
                searchAppBox.sendKeys(appName);
                GZLogger.ActivityLog("Entered " +appName+ " in the search box");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Unable to enter the " +appName+ " in the search box" );
            return false;
        }
        return false;
    }


    public boolean clickSinergifyApp(){
        GZWait(2, sinergifySearchResult,20);
        sinergifySearchResult.click();
        return sinergifySearchResult.isDisplayed();
    }
    public void clickAdminSettings() throws InterruptedException {
        GZWait(2, adminSettingsTab,60);
        GZJSEleClick(adminSettingsTab);
        GZWait(0);
    }
    public void clickBasicAuth() {
        GZWait(3, rdbBasicAuthYes,60);
        rdbBasicAuthYes.click();
    }
    public void clickOpenAuth() {
        GZWait(2, rdbOAuthYes,30);
        rdbOAuthYes.click();
    }
    public void clearJiraName(){
        GZWait(2,jiraNameInBasicAuth,10);
        jiraNameInBasicAuth.clear();
    }
    public void setJiraName(String jiraName){
        GZWait(2,jiraNameInBasicAuth,10);
        Random obj=new Random();
        int randNum=obj.nextInt(100);
        jiraNameInBasicAuth.sendKeys(jiraName+randNum);
    }
    public String  getJiraName(){
        GZWait(2,jiraNameInBasicAuth,10);
        JavascriptExecutor js=(JavascriptExecutor) GZBrowser.getDriver();
        String text=js.executeScript("return arguments[0].value",jiraNameInBasicAuth).toString();
//      return jiraNameInBasicAuth.getText(); This getText() does not support for the hidden element
        return text;
    }
    public void clearJiraUrl(){
        GZWait(2,jiraUrl,10);
        jiraUrl.clear();
    }
    public void setFrmSettings(){GZWait(2, frmAdminSettings,60);}
    public void setUrl(String jiraurl){
        GZWait(2,jiraUrl,10);
        jiraUrl.sendKeys(jiraurl);
    }
    public void clearJiraNameOauth() throws InterruptedException {
        GZWait(0);
        GZWait(2,jiraNameInOauth,10);
        jiraNameInOauth.clear();
    }
    public void setJiraNameOauth(String jiraName){
        GZWait(2,jiraNameInOauth,10);
        jiraNameInOauth.sendKeys(jiraName);
    }
    public void clearJiraUrlOauth(){
        GZWait(2,jiraUrlOauth,10);
        jiraUrlOauth.clear();
    }
    public void setUrlOauth(String jiraurl){
        GZWait(2,jiraUrlOauth,10);
        jiraUrlOauth.sendKeys(jiraurl);
    }

    @FindBy(xpath="//input[@id='username']")
    WebElement usernameBoxJiraPage;
    public void enterUsernameJira(String uname){
        try{
            GZWait(3,usernameBoxJiraPage,10);
            usernameBoxJiraPage.clear();
            usernameBoxJiraPage.sendKeys(uname);
            GZLogger.ActivityLog("Entered the username on Jira page");
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to enter the username in Jira page");
        }
    }
    @FindBy(xpath="//input[@id='password']")
    WebElement passwordBoxJiraPage;
    public void enterPasswordJira(String paswd){
        try{
            GZWait(3,passwordBoxJiraPage,10);
            passwordBoxJiraPage.clear();
            passwordBoxJiraPage.sendKeys(paswd);
            GZLogger.ActivityLog("Entered the password on Jira page");
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to enter the password in Jira page");
        }
    }
    @FindBy(xpath="//button[@id='login-submit']")
    WebElement loginBtnJiraPage;

    public void clickLoginBtnJira(){
        try{
            GZWait(3,loginBtnJiraPage,10);
            loginBtnJiraPage.click();
            GZLogger.ActivityLog("Clicked on Continue button on Jira page");
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Continue button on Jira page");
        }
    }

    @FindBy(xpath = "//input[@value='Allow']")
    WebElement allowButton_2;
    public boolean clickAllowBtn_2(){
        Boolean status=false;
        try{
            GZWait(3,allowButton_2,10);
            status=allowButton_2.isDisplayed();
            allowButton_2.click();
            GZLogger.ActivityLog("Clicked on Allow_2 button");
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on Allow_2 button");
        }
        return status;
    }
    @FindBy(xpath = "//input[@id='approve']")
    WebElement allowButton_1;
    public void clickAllowBtn_1(){
        try{
            GZWait(3,allowButton_1,10);
            allowButton_1.click();
            GZLogger.ActivityLog("Clicked on Allow_1 button");
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on Allow_1 button");
        }
    }


    public void clearUsername(){jiraUsername.clear();}
    public void setUname(String juname){jiraUsername.sendKeys(juname);}
    public void clearPassword(){jiraPassword.clear();}
    public void setPassword(String jpswd){jiraPassword.sendKeys(jpswd);}
    public void clickDefaultInstance(){
        GZWait(2,defaultInstance,20);
        //System.out.println(defaultInstance.isEnabled());
        //String text=defaultInstance.getText();
      //  System.out.println(text);
        defaultInstance.click();
       // Boolean flag1= defaultInstance.isEnabled();
       // System.out.println(flag1);
    }
    //public void selectsfjira(){piklistSfjira.click();}
    public void clickEnableEditJira() { rdbEnblEdtNo.click(); }
    public void clickEnableStatusTranSfYes(){ rdbEnblStatusTransSfNo.click(); }
    public void clickEnableStatusTra_inSalesforceNo(){rdbEnblStatusTransSfYes.click();}
    public void clickEnableEditJiraNo() { rdbEnblEdtYes.click(); }
    public void scrollBtm(WebElement ele){GZScrollBtm(ele);}
    public void scrollTop(){GZScrollTop();}
    public void scrollTop(WebElement element){GZScrollTop(element);}
    public void clickJiraCloud() {rdbJiraswClud.click();}
    public void clickJiraServer() { rdbJiraswSrvr.click(); }
    public void clickFeedSyncYes(){
        GZWait(2,enableFeedSyncYes,10);
        enableFeedSyncYes.click();
    }
    public void clickFeedSyncNo(){
        GZWait(2,enableFeedSyncNo,10);
        enableFeedSyncNo.click();
    }
    public void clickautosynsfcmntsyes() {
        GZWait(2,rdbAutosyncComntYes,10);
        rdbAutosyncComntYes.click(); }
    public void clicktypeofcmntsynboth() {
        GZWait(2,rdbTypcomntSyncBoth,10);
        rdbTypcomntSyncBoth.click();
    }
    public void clicktypeofcmntsynPublic() {
        GZWait(2,rdbTypcomntSyncPblc,10);
        rdbTypcomntSyncPblc.click();
    }
    public void clicktypeofcmntsynPrvt() { rdbTypcomntSyncPvt.click(); }
    public void clickAutoSyncSfCmntNo(){rdbAutosyncComntNo.click();}
    public void clickAutoSynSfAttahYes() { rdbAutosyncAttchYes.click(); }
    public void clickAutoSynSfAttahNo(){rdbAutosyncAttchNo.click();}
    public void clickjirscmntinsfyes() { rdbSyncJrcmtSfYes.click(); }
    public void clicksyncascasecmntyes() { rdbSyncCasecmntYes.click(); }
    public void clicksyncascasecmntNo() { rdbSyncCasecmntNo.click(); }
    public void clickcmntssavedpublic() { rdbCmntSavPblc.click(); }
    public void clickcmntssavedPrvt() { rdbCmntSavPvt.click(); }
    public void clickjirscmntinsfNo() { rdbSyncJrcmtSfNo.click(); }
    public void syncJiraAttToSfYes(){rdbSynJirAttchSfYes.click();}
    public void syncJiraAttToSfNo(){rdbSynJirAttchSfNo.click();}
    public void syncAttachmentToSfObjectYes() throws InterruptedException {GZWait(0);syncAttachmentToSfObjectYes.click();}
    public void syncAttachmentToSfObjectNo(){syncAttachmentToSfObjectNo.click();}
    public void enterAttachmentQualifier(String attachmentQualifier){

        GZWait(2,attachmentQualifierInput,10);
        attachmentQualifierInput.sendKeys(attachmentQualifier);
    }
    public void enterCommentsQualifier(String commentsQualifier){
        GZWait(2,commentsQualifierInput,10);
        commentsQualifierInput.sendKeys(commentsQualifier);
    }
    public void checkSuccessMessage(WebElement ele){
        System.out.println(ele.getText());
    }
    public boolean clickSaveBtn() {
        boolean flag=false;
        try {
            GZScrollBtm(saveBtn);
            GZWait(3,saveBtn,20);
            saveBtn.click();
            flag=true;
        } catch (Exception e) {
            GZLogger.ErrorLog(e,e.getMessage());
        }
        return flag;
    }

    public boolean verifySaveSuccessMessageOauth(Map<String,String> map){
        boolean status=false;
        try {
            GZWait(0);
            status=verifyOauthPermission(map);

//            if (instanceSuccessMessageOauth.isDisplayed()) {
//                String message = instanceSuccessMessageOauth.getText();
//                status = message.contains("warning");
//                if (status) {
//                    GZLogger.ActivityLog("Admin user is not able to add a Jira cloud with OAuth successfully");
//                } else {
//                    GZLogger.ActivityLog("Admin user is able to add a Jira cloud with OAuth successfully");
//                }
//            } else {
//                status=verifyOauthPermission(map);
//                GZLogger.ActivityLog("Admin user is able to add a Jira cloud with OAuth successfully");
//            }
        }
        catch (Exception e){
            GZLogger.ErrorLog(e,"Admin user is not able to add a Jira cloud with OAuth successfully");
        }
        return status;
    }

    public boolean verifyOauthPermission(Map<String,String > map){
        boolean status=false;
        WebDriver driver=GZBrowser.getDriver();
        try{
            String parentWindow = driver.getWindowHandle();
            //System.out.println("Parent window handle: "+" "+parentWindow);
            Set<String> handles =  driver.getWindowHandles();
            for(String windowHandle  : handles)
            {
                if(!windowHandle.equals(parentWindow))
                {
                    //System.out.println("Child window handle: "+" "+windowHandle);
                    WebDriver test= driver.switchTo().window(windowHandle);
                    //System.out.println(test);
                    enterUsernameJira(map.get("username_jira"));
                    clickLoginBtnJira();
                    enterPasswordJira(map.get("password_jira"));
                    clickLoginBtnJira();
                    clickAllowBtn_1();
                    status=clickAllowBtn_2();
                    GZWait(0);
                    //driver.close();
                    driver.switchTo().window(parentWindow);
                    GZLogger.ActivityLog("Admin user is able to add a Jira cloud with OAuth successfully");
                }
            }
        }catch (Exception e){

        }
        return status;
    }

    public boolean clickNxtBtn(){
        boolean status=false;
        try{
            GZWait(3,instanceSuccessMessage,30);
            status=instanceSuccessMessage.isDisplayed();
            GZWait(2,nxtBtn,3);
            nxtBtn.click();
            GZScrollTop();
            GZWait(0);
            }catch (Exception e){
            GZLogger.ErrorLog(e,"No able to find the next button");
        }
        return status;
    }
    public void clickProjectTab() {
        GZWait(3,projectTab,50);
        projectTab.click();
        System.out.println("Clicked on the Project Tab");
    }
    public void setConsumerKey(String consumerkey) {consumrKey.sendKeys(consumerkey);}
    public void setPrivateKey(String privtkey){privteKey.sendKeys(privtkey);}
    public void clearConsumerKey(){consumrKey.clear();}
    public void clearPrivateKey(){privteKey.clear();}
    public void selectDvalOne(){piklistSfjira.sendKeys(Keys.RETURN);}
    public void selectDvalMany(){
        piklistSfjira.sendKeys(Keys.ARROW_DOWN);
        piklistSfjira.sendKeys(Keys.RETURN);
    }
    public void selectDvalMany(WebElement ele){
        ele.sendKeys(Keys.ARROW_DOWN);
        ele.sendKeys(Keys.RETURN);
    }

    @FindBy(xpath = "//span[normalize-space()='Instances']")
    WebElement instancesOption;

    public boolean instanceOptionVisible(){
        boolean flag=false;
        try{
            GZWait(3,instancesOption,10);
            flag=instancesOption.isDisplayed();
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to see Instances option in side menu");
        }
        return flag;
    }

    @FindBy(xpath = "//span[normalize-space()='Configuration Settings']")
    WebElement configurationSettingsOption;

    public boolean configurationSettingsOptionVisible(){
        boolean flag=false;
        try{
            GZWait(3,configurationSettingsOption,1);
            flag=configurationSettingsOption.isDisplayed();
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to see Configuration Settings option in side menu");
        }
        return flag;
    }

    @FindBy(xpath = "//span[normalize-space()='Projects']")
    WebElement projectsOption;

    public boolean projectsOptionVisible(){
        boolean flag=false;
        try{
            GZWait(3,projectsOption,20);
            flag=projectsOption.isDisplayed();
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to see Projects option in side menu");
        }
        return flag;
    }

    @FindBy(xpath = "//span[@data-target-id='fieldExpand']")
    WebElement fieldConfigurationOption;

    public boolean fieldConfigurationOptionVisible(){
        boolean flag=false;
        try{
            GZWait(3,fieldConfigurationOption,1);
            flag=fieldConfigurationOption.isDisplayed();
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to see Field Configuration option in side menu");
        }
        return flag;
    }

    @FindBy(xpath = "//span[normalize-space()='Validation']")
    WebElement validationOption;

    public boolean validationOptionVisible(){
        boolean flag=false;
        try{
            GZWait(3,validationOption,1);
            flag=validationOption.isDisplayed();
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to see Validation option in side menu");
        }
        return flag;
    }
    @FindBy(xpath = "//span[normalize-space()='Rule Set']")
    WebElement rulesetOption;

    public boolean rulesetOptionVisible(){
        try{
            GZWait(3,rulesetOption,1);
            if(rulesetOption.isDisplayed()){
                GZLogger.ActivityLog("Rule Set option is visible in side menu");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to see Rule Set option in side menu");
        }
        return false;
    }

    @FindBy(xpath = "//div[@class='slds-global-header__item']//div[@class='slds-global-header__logo']")
    WebElement sinergifyHeaderLogo;

    public boolean isLogoVisible(){
        boolean flag=false;
        try{
            flag=sinergifyHeaderLogo.isDisplayed();
            GZLogger.ActivityLog("Admin user see the Logo of Sinergify on the top header of the page");
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Admin user does not see the Logo of Sinergify on the top header of the page");
        }
        return flag;
    }

    @FindBy(xpath = "//div[@class='dropbtn']//img[@class='slds-icon flag']")
    WebElement languageChangeFlag;

    public boolean isLanguageIconVisible(){
        Boolean status=false;
        try{
            status=languageChangeFlag.isDisplayed();
            GZLogger.ActivityLog("Change language icon is visible");
        }catch (Exception e) {
            GZLogger.ErrorLog(e, "Change language icon is not visible");
        }
        return status;
    }

    public boolean isChangeInstanceIconVisible(){
        Boolean status=false;
        try{
            GZWait(3,instanceChangeIcon,10);
            status=instanceChangeIcon.isDisplayed();
            GZLogger.ActivityLog("Change Jira instance icon is visible");
        }catch (Exception e) {
            GZLogger.ErrorLog(e, "Change Jira instance icon is not visible");
        }
        return status;
    }

    @FindAll({
            @FindBy(xpath = "//div[@class='multicontainer']/div")
    })
    public List<WebElement> jiraTiles;

    public boolean jiraTiles(){
        Boolean flag=false;
        try{
            int size=jiraTiles.size();
            if(size==7)
                GZLogger.ActivityLog("Admin user see the 5 tiles to add new Jira instance");
            flag=true;
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Admin user does not see the 5 tiles to add new Jira instance");
        }
        return flag;
    }

    @FindBy(xpath = "//li[@data-label='Basic Authentication']")
    WebElement basicAuthTab;

    public boolean isBasicAuthClickable(){
        Boolean status=false;
        try{
            GZWait(2,basicAuthTab,20);
            status=basicAuthTab.isEnabled();
            GZLogger.ActivityLog("Admin user navigates successfully to Basic authentication tab");
        }catch (Exception e) {
            GZLogger.ErrorLog(e, "Admin user is not able to navigate to Basic authentication tab");
        }
        return status;
    }

    @FindBy(xpath = "//div[@class='multicontainer']/div[4]")
    WebElement addNewJiraInstanceTile;
    public void clickAddNewJiraInstance(){
        try{
            GZWait(2,addNewJiraInstanceTile,40);
            addNewJiraInstanceTile.click();
            GZLogger.ActivityLog("Clicks on the Add New Instance link successfully");
        }catch (Exception e) {
            GZLogger.ErrorLog(e, "Not able to click on the Add New Instance link");
        }
    }
    @FindBy(xpath = "//label[contains(text(),\"Callback URL\")]")
    WebElement callBackUrlLabel;

    public boolean isCallBackUrlPresent(){
        Boolean status=false;
        try{
            clickOauthTab();
            GZWait(3,callBackUrlLabel,10);
            status=callBackUrlLabel.isDisplayed();
            GZLogger.ActivityLog("Admin user navigates successfully to OAuth authentication tab");
        }catch (Exception e) {
            GZLogger.ErrorLog(e, "Admin user does not navigate to OAuth authentication tab");
        }
        return status;
    }
    @FindBy(xpath = "//li[contains(@title,'OAuth Authentication')]")
    WebElement oAuthTab;
    public boolean clickOauthTab(){
        WebDriver driver=GZBrowser.getDriver();
        Boolean status=false;
        try{
            GZWait(0);
            GZWait(0);
            GZWait(2,oAuthTab,10);
            JavascriptExecutor jse = (JavascriptExecutor)driver;
            jse.executeScript("arguments[0].click()", oAuthTab);
            status=true;
            GZLogger.ActivityLog("Admin user clicks on the Oauth tab");
        }catch (Exception e) {
            GZLogger.ErrorLog(e, "Admin user is not able to click on the Oauth tab");
        }
        return status;
    }

    @FindBy(xpath = "//a[normalize-space()='WebHook Help?']")
    WebElement webhookLink;
    @FindBy(xpath = "//h2[normalize-space()='WebHook Help']")
    WebElement webhookWindowPopup;
    public boolean clickWebhookHelpLink(){
        Boolean status=false;
        try{
            GZWait(2,webhookLink,10);
            status=webhookLink.isDisplayed();
            GZWait(0);
            webhookLink.click();
            GZWait(3,webhookWindowPopup,10);
            status=webhookWindowPopup.isDisplayed();
            GZLogger.ActivityLog("Clicked on the Webhook link");
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Webhook link");
        }
        return status;
    }
    @FindBy(xpath = "//button[normalize-space()='Close']")
    WebElement closeBtnWebhook;
    public boolean clickCloseBtnWebhookPopup(){
        boolean status=false;
        try{
            GZWait(3,closeBtnWebhook,3);
            status=closeBtnWebhook.isDisplayed();
            closeBtnWebhook.click();
            GZWait(3,webhookLink,3);
            status=webhookLink.isDisplayed();
            GZLogger.ActivityLog("Closed the webhook help window successfully");
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to Close the webhook help window successfully");
        }
        return status;
    }

    @FindBy(xpath = "//div/input[contains(@placeholder,'Select an Option')]")
    WebElement selectSiteWebhookDropdown;
    public boolean clickSelectSiteDropdown(){
        boolean status=false;
        try{
            GZWait(2,selectSiteWebhookDropdown,5);
            status=selectSiteWebhookDropdown.isEnabled();
            selectSiteWebhookDropdown.click();
            selectDvalMany(selectSiteWebhookDropdown);

            GZLogger.ActivityLog("Please select a site- dropdown is enabled");
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Please select a site- dropdown is not enabled");
        }
        return status;
    }
    @FindBy(xpath = "//div/a[contains(text(),'Click here to copy webhook Url')]")
    WebElement webhookUrlCopyLink;
    public boolean checkWebhookUrlCopyLink(){
        boolean status=false;
        try{
            GZWait(2,webhookUrlCopyLink,5);
            status=webhookUrlCopyLink.isEnabled();
            if (status)
               webhookUrlCopyLink.click();
            GZWait(3,instanceSuccessMessage,5);
            status=instanceSuccessMessage.isDisplayed();
            GZLogger.ActivityLog("Copied the webhook url successfully");
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to copy the webhook url");
        }
        return status;
    }

    public boolean isSideOptionsClickable() {
        boolean status = false;
        try {
            GZWait(2,instancesOption,10);
            configurationSettingsOption.click();
            status = configurationSettingsOption.isSelected();
            if (status)
                GZLogger.ActivityLog("Configuration Settings option is clickable");
            status = projectsOption.isSelected();
            if (status)
                GZLogger.ActivityLog("Projects option is clickable");
            status = fieldConfigurationOption.isSelected();
            fieldConfigurationOption.click();
            GZWait(0);
            scrollBtm(rulesetOption);
            GZWait(0);
            if (status)
                GZLogger.ActivityLog("Field Configuration option is clickable");
            status = validationOption.isSelected();
            if (status)
                GZLogger.ActivityLog("Validation option is clickable");
            status = rulesetOption.isSelected();
            scrollTop();
            if (status)
                GZLogger.ActivityLog("Rule Set option is clickable");

        } catch (Exception e) {
            GZLogger.ErrorLog(e, "The options in the side menu are not clickable");
        }
        return status;
    }
    @FindBy(xpath = "//div[@class='diffcolor']")
    List<WebElement> listOfTogglesConfigElements;
    @FindBy(xpath = "//label[contains(text(),'Salesforce-Jira Relation')]")
    List<WebElement> listOfDropdownConfigElements;
    @FindBy(xpath = "//lightning-input[@class='slds-form-element']")
    List<WebElement> listOfInputBoxConfigElements;

    public boolean findElementsOfConfigSettings(){
        boolean status=false;
        try{
            GZWait(0);
            configurationSettingsOption.click();
            GZWait(0);
            int s1=listOfTogglesConfigElements.size();
            int s2=listOfDropdownConfigElements.size();
            int s3=listOfInputBoxConfigElements.size();
            int s4=s1+s2+s3;
            if (s4==12) {
                status = true;
                GZLogger.ActivityLog("Admin user clicks on Configuration settings and see all 12 options on the screen after admin user successfully adds a Jira instance");
            }else
                GZLogger.FailLog("Admin user clicks on Configuration settings and does not see all 12 options on the screen after admin user successfully adds a Jira instance");
        }catch (Exception e){
            GZLogger.ErrorLog(e,e.getMessage());
            GZLogger.FailLog("Admin user clicks on Configuration settings and does not see all 12 options on the screen after admin user successfully adds a Jira instance");
        }
        return status;
    }

    @FindBy(xpath = "//div[@class='multicontainer']/div[3]//tbody/tr[3]")
    WebElement instance1Status;

    public boolean checkIfInstanceActive(){
        String text=null;
        try{
            instance1Status.isDisplayed();
            text=instance1Status.getText();
            System.out.println(text);
            if(text.contains("Active"))
                return true;
            return false;
        }catch (Exception e){
            return false;
        }
    }

    @FindBy(xpath = "//div[@class='multicontainer']/div[3]//div//tr[3]")
    WebElement isActiveInstance;
    public boolean instanceIsActive(){
        try{
            GZWait(3,isActiveInstance,10);
            if(isActiveInstance.getText().contains("Active")){
                return true;
            }
            else if(isActiveInstance.getText().contains("Inactive"))
                return false;
            return false;
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Either the Jira instance is not added or Inactive");
            return false;
        }
    }

    @FindBy(xpath = "//div[@class='slds-tabs_default']//ul/li/a[@aria-selected='true']")
    WebElement activeAuthTab;
    public String getActiveAuthName(){
        String authName = null;
        try{
            GZWait(0);
            GZWait(2,activeAuthTab,6);
            authName= activeAuthTab.getText();
            GZLogger.ActivityLog("Active authentication name is:"+" "+authName);
            return authName;
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to find the name of the Active auth type");
            return authName;
        }
    }

    @FindBy(xpath = "//div[@class='multicontainer']/div[3]//div")
    WebElement firstInstance;
    public boolean checkIfFirstInstanceAdded(){
        try{
            GZWait(3,firstInstance,20);
            return firstInstance.isDisplayed();
        }catch (Exception e){
            GZLogger.ErrorLog(e,"No Jira instance is added yet");
            return false;
        }
    }
    public boolean clickFirstInstance(){
        try {
            GZWait(2,firstInstance,5);
            firstInstance.click();
            GZLogger.ActivityLog("Clicked on the first added Jira instance");
            return true;
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the first added Jira instance");
            return false;
        }
    }


}