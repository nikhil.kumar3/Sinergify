package GZProducts.Sinergify.SinergifyUtilities.POM;

import CommonUtilities.GZBrowser.GZBrowser;
import CommonUtilities.GZCommonFuncs.GZCommonElementFuncs;
import CommonUtilities.GZLogger.GZLogger;
import org.apache.logging.log4j.core.appender.rolling.action.IfAll;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.NoSuchElementException;


public class ProjectsPage extends GZCommonElementFuncs {
    public ProjectsPage() {
        PageFactory.initElements(GZBrowser.getDriver(), this);
    }

    @FindBy(xpath = "//div[contains(text(),'Default Project')]/following::select[1]")
    WebElement defaultProject;

    @FindBy(xpath = "//label[contains(text(),'No project available')]")
    WebElement noProject;

    @FindBy(xpath = "//div[@class='ProjectContainer slds-col']/div[6]/div[1]/div/table/tbody/tr[1]/td[1]")
    WebElement selectProject;

    @FindBy(xpath = "//div[@id='Project']//input[@class='btn btn']")
    WebElement saveButton;

    @FindBy(xpath = "//div[@id='Project']//a[@class='btn']")
    WebElement nxtBtn;

    @FindBy(xpath = "//div[@id='sync_C2']//input[1]")
    WebElement synCheckbox;

    @FindBy(xpath = "//div[@id='write_C2']//input[1]")
    WebElement writeCheckbox;

    @FindBy(xpath = "//div[@id='write_C2']/following::select[1]")
    WebElement defaultIssueType;

    @FindBy(xpath = "//div[@id='write_C2']//input[1]")
    WebElement issueTypesToSync;

    @FindBy(xpath = "//div[@id='write_C2']//input[1]")
    WebElement plusIssue;

    @FindBy(xpath = "//div[contains(text(),'Default Project')]/following::select[1]")
    WebElement defaultProjDropdown;

    @FindBy(xpath = "//div[contains(text(),'Default Project')]/following::select[1]/option[2]")
    WebElement selectDefProjct;
    @FindBy(xpath = "//span[normalize-space()='Projects']")
    WebElement projectsOption;

    public void clickSave() {
        saveButton.click();
    }
    @FindBy(xpath = "//button[@name='Save']")
    WebElement saveBtn;

    public boolean clickSaveBtn() {
        boolean flag=false;
        try {
            GZScrollBtm(saveBtn);
            GZWait(3,saveBtn,20);
            saveBtn.click();
           GZWait(3,instanceSuccessMessage,20);
            flag=true;
        } catch (Exception e) {
            GZLogger.ErrorLog(e,e.getMessage());
        }
        return flag;
    }

    public void selectDefaultIssue() {
        selectDdVal(defaultIssueType, "Task");
    }

    public boolean clickNext() {
        boolean flag = false;
        try {
            nxtBtn.click();
            flag = true;
            GZLogger.ActivityLog("Clicked on the Next button");
        } catch (Exception e) {
            GZLogger.ErrorLog(e, e.getMessage());
        }
        return flag;
    }

    public void clickSyncBox() {
        synCheckbox.click();
    }

    public void clickWriteBox() {
        writeCheckbox.click();
    }

    public void clickDefaultIssuType() {
        defaultIssueType.click();
    }

    public void clickIssuTypeToSync() {
        issueTypesToSync.click();
    }

    public boolean someProjectsAvail() {
        boolean status = false;
        try {
            GZWait(3, selectProject, 10);
            status = selectProject.isDisplayed();
            GZLogger.ActivityLog("Able to see at least one project in the grid");
        } catch (Exception e) {
//            GZLogger.ErrorLog(e,"Not able to see at least one project in the grid");
            GZLogger.FailLog("Not able to see at least one project in the grid");
        }
        return status;
    }

    public void selectDefProject() {
        selectDdValByIndex(defaultProjDropdown, 1);
    }

    @FindBy(xpath = "//table/tbody/tr")
    WebElement projectList;

    public boolean isProjectsDisplayed() {
        boolean status = false;
        try {
            GZWait(2, projectsOption, 10);
            projectsOption.click();
            GZWait(3, projectList, 10);
            status = projectList.isDisplayed();
            GZLogger.ActivityLog("Projects are displayed in the grid");
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Projects are not displayed in the grid");
        }
        return status;
    }

    public boolean projectsOptionVisible() {
        boolean flag = false;
        try {
            GZWait(3, projectsOption, 1);
            flag = projectsOption.isDisplayed();
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Not able to see Projects option in side menu");
        }
        return flag;
    }

    @FindBy(xpath = "//input[@name='Default Project']")
    WebElement defaultProjectDropdown;

    public boolean isDefaultProjectDropdownVisible() {
        boolean status = false;
        try {
            GZWait(3, defaultProjectDropdown, 10);
            status = defaultProjectDropdown.isDisplayed();
            GZLogger.ActivityLog("Default Project dropdown is visible on the project page");
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Unable to locate Default Project dropdown");
        }
        return status;
    }

    @FindBy(xpath = "//input[@name='enter-search']")
    WebElement searchProjectInputBox;

    public boolean clickSearchProjectInput() {
        boolean status = false;
        try {
            GZWait(3, searchProjectInputBox, 20);
            searchProjectInputBox.click();
            status = true;
            GZLogger.ActivityLog("Able to click inside the search box on Projects page");
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Not able to click inside the search box on Projects page");
        }
        return status;
    }

    @FindBy(xpath = "//div[@class='ProjectContainer slds-col']/div[6]/div[1]/div/table/tbody/tr[1]/td[1]")
    WebElement projectFirstName;

    public String getProjectName() {
        try {
            GZWait(3, projectFirstName, 10);
            GZLogger.ActivityLog("Able to copy the Project name from the grid");
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Not able to copy the Project name from the grid");
        }
        return projectFirstName.getText();
    }

    public boolean searchProject() throws InterruptedException {
        boolean status = false;
        try {
            String projectName = getProjectName();
            if (projectName != null)
                searchProjectInputBox.sendKeys(projectName);
            GZWait(0);
            status = true;
            GZLogger.ActivityLog("Able to search the project on Project page");
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Not able to search the project on Project page");
        }
        return status;
    }

    @FindBy(xpath = "//img[contains(@src,'refresh.png')]")
    WebElement refreshIconOnProjectPage;

    public boolean clickRefreshProject() {
        boolean status = false;
        try {
            GZWait(2, refreshIconOnProjectPage, 10);
            refreshIconOnProjectPage.click();
            GZWait(3, refreshIconOnProjectPage, 20);
            status = true;
            GZLogger.ActivityLog("Clicked on the refresh icon on Project page");
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Not able to click on the Refresh icon on Project page");
        }
        return status;
    }

    @FindBy(xpath = "//div[@class='PageNumberSelector']//input")
    WebElement pageNumberInput;

    @FindBy(xpath = "//div[@class='PageNumberSelector']//ul/div[2]")
    WebElement pageNumberValue;

    public boolean clickPageNumber() {
        try {
            GZWait(3, pageNumberInput, 10);
            pageNumberInput.click();
            GZWait(3, pageNumberValue, 10);
            pageNumberValue.click();
            GZLogger.ActivityLog("Selected the page number on Project page");
            return true;
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Not able to select page number on Project page");
            return false;
        }
    }

    @FindBy(xpath = "//span[normalize-space()='Projects']")
    WebElement projectTab;

    public void clickProjectTab() {
        GZWait(3, projectTab, 50);
        projectTab.click();
        System.out.println("Clicked on the Project Tab");
    }

    @FindBy(xpath = "//div[@class='ProjectContainer slds-col']/div[6]/div[1]/div/table/tbody/tr[1]/td[6]/div/lightning-icon[@title='Add Sharing Settings']")
    WebElement addShareSettingIcon;

    public boolean clickAddShareSettingIcon() {
        try {
            clickProjectTab();
            GZWait(3, addShareSettingIcon, 15);
            addShareSettingIcon.click();
            GZLogger.ActivityLog("Clicked on the Sharing setting icon");
            return true;
        } catch (Exception e) {
            GZLogger.ErrorLog(new NoSuchElementException(), "Share project icon is not visible");
            return false;
        }
    }

    @FindBy(xpath = "//lightning-layout-item/slot/div/button[@name='Add']")
    WebElement addButtonOnProjectSharingSetting;
    @FindBy(xpath = "//lightning-layout-item/slot/div/button[@name='Cancel']")
    WebElement cancelButtonOnProjectSharingSetting;

    public boolean clickAddButtonProjectSharingSetting() {
        try {
            GZWait(3, addButtonOnProjectSharingSetting, 10);
            addButtonOnProjectSharingSetting.click();
            GZLogger.ActivityLog("Clicked on the Add button on Project Sharing pop up");
            return true;
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Not able to click on the Add button on Project Sharing Setting pop up");
            return false;
        }
    }

    public boolean clickCancelButtonProjectSharingSetting() {
        try {
            GZWait(3, addButtonOnProjectSharingSetting, 10);
            addButtonOnProjectSharingSetting.click();
            GZLogger.ActivityLog("Clicked on the Add button on Project Sharing pop up");
            return true;
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Not able to click on the Add button on Project Sharing Setting pop up");
            return false;
        }
    }

    @FindBy(xpath = "//div[@class='selectshare']//input[@placeholder='Select an Option']")
    WebElement shareToDropdown;
    @FindBy(xpath = "//div[@class='selectshare']//input[@placeholder='Search']")
    WebElement shareToSearchBox;
    @FindBy(xpath = "//div[@class='selectshare']//lightning-base-combobox-item[@data-value='Role']")
    WebElement shareToRoleOption;

    public boolean clickShareToDropdown() {
        try {
            GZWait(3, shareToDropdown, 10);
            shareToDropdown.click();
            GZWait(3, shareToRoleOption, 3);
            shareToRoleOption.click();
            GZLogger.ActivityLog("Clicked on ShareTo dropdown and Selected Role from the list options");
            return true;
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Not able to click on ShareTo dropdown");
            return false;
        }
    }

    public boolean clickShareToSearch() {
        try {
            GZWait(3, shareToSearchBox, 10);
            shareToSearchBox.sendKeys("Test");
            GZLogger.ActivityLog("Entered data in ShareTo search box");
            return true;
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Not able to find ShareTo search box");
            return false;
        }
    }

    @FindBy(xpath = "//section[@role='dialog']//li[1]//div[1]")
    WebElement roleAtFirst;

    public boolean clickFirstRole() {
        try {
            GZWait(0);
            roleAtFirst.click();
            GZLogger.ActivityLog("Able to select a role from the list of roles");
            return true;
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Not able to select a role to add in Sharing setting");
            return false;
        }
    }

    @FindBy(xpath = "//div[@class='slds-dueling-list__column']//lightning-button-icon[1]//button[@title=\"Move selection to undefined\"]")
    WebElement moveElementToSelected;

    @FindBy(xpath = "//div[@class='slds-dueling-list__column']//lightning-button-icon[2]//button[@title=\"Move selection to undefined\"]")
    WebElement moveElementToAvailable;


    public boolean clickMoveElementToSelected() {
        try {
            GZWait(3, moveElementToSelected, 3);
            moveElementToSelected.click();
            GZLogger.ActivityLog("Added role from available to selected");
            return true;
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Not able to add role from available to selected");
            return false;
        }
    }

    @FindBy(xpath = "//div[@class='aligncenter']//input[@role='combobox']")
    WebElement accessDropdown;
    public boolean clickAccessDropdown() {
        try {
            GZScrollBtm(accessDropdown);
            GZWait(3, accessDropdown, 3);
            GZWait(0);
            accessDropdown.click();
            GZWait(0);
            accessDropdown.sendKeys(Keys.ARROW_DOWN);
            accessDropdown.sendKeys(Keys.RETURN);
            GZLogger.ActivityLog("Added Read/Write access ");
            return true;
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Not able to add  Read/Write access");
            return false;
        }
    }


    @FindBy(xpath = "//div[@class='saveContainer']//button[@name='Save'][normalize-space()='Save']")
    WebElement saveButtonAtSharing;

    public boolean clickSaveButtonSharingSetting() {
        boolean status = false;
        try {
            GZScrollBtm(saveButtonAtSharing);
            GZWait(3, saveButtonAtSharing, 3);
            saveButtonAtSharing.click();
            status = checkSuccessMessage();
            GZLogger.ActivityLog("Saved the sharing setting successfully");
            return status;
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Not able to save the Sharing setting");
            return status;
        }

    }

    @FindBy(xpath = "//span[contains(text(),'Success')]")
    WebElement instanceSuccessMessage;

    public boolean checkSuccessMessage() {
        boolean status = false;
        try {
            GZWait(3, instanceSuccessMessage, 20);
            instanceSuccessMessage.isDisplayed();
            String msg = instanceSuccessMessage.getText();
            if (msg.contains("Success")) {
                GZLogger.ActivityLog("Operation is success");
                status = true;
            }
            return status;
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Not able to save the action");
            return status;
        }

    }

//    @FindBy(xpath = "//lightning-layout-item/slot/div[2]//div[@class='Searchcontainer']/lightning-input/div/input")
//    WebElement searchRole;
//    public boolean searchRole(){
//        boolean status=false;
//        //will have to add search
//    }

    @FindBy(xpath = "//div[@class='ProjectTableContainer slds-scrollable--y']//table/tbody/tr")
    List<WebElement> projectTableAllRows;

    public int getRowsInProjectTable() {
        int rows = 0;
        try {
            GZWait(3, addShareSettingIcon, 10);
            rows = projectTableAllRows.size();
            GZLogger.ActivityLog("Total projects in the table are" + ":" + rows);
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Unable to find the projects in the table");

        }
        return rows;
    }

    @FindBy(xpath = "//table/thead/tr/th")
    List<WebElement> projectTableAllColumns;

    public int getColumnsInProjectTable() {
        int columns = 0;
        try {
            GZWait(3, addShareSettingIcon, 10);
            columns = projectTableAllColumns.size();
            GZLogger.ActivityLog("Total columns in the table are" + ":" + columns);
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Unable to find the columns in the project table");
        }
        return columns;
    }

    /*Handling the project table dynamically | Defining all xpath dynamically*/

      @FindBy(xpath = "//table/tbody/tr/td[2]//input")
      List<WebElement> syncBox;
      @FindBy(xpath = "//table/tbody/tr/td[2]//input")
      WebElement syncCheckBox;

    public boolean checkIfSyncCheckBoxTrue() {
        boolean flag=false;
        try {
            GZWait(3, refreshIconOnProjectPage, 7);
            GZWait(0);
            for(WebElement e : syncBox){
                boolean isTrue = e.isSelected();
                System.out.println("The value of isTrue at Sync Checkbox is" + ":" + isTrue);
                if (!isTrue) {
                    GZWait(2,syncCheckBox,3);
                    GZJSEleClick(e);
                    flag=true;
                    break;
                }
            }
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Not able to find any sync checkbox on Project table");
            return false;
        }
        return flag;
    }

    @FindBy(xpath ="//table/tbody/tr/td[3]//input")
    List<WebElement> writeCheckBox;
    public boolean checkIfWriteCheckBoxTrue() {
        boolean flag=false;
        try {
            GZWait(3, refreshIconOnProjectPage, 7);
            for(WebElement e : writeCheckBox){
                boolean isTrue = e.isSelected();
                System.out.println("The value of isTrue Write checkbox is" + ":" + isTrue);
                if (!isTrue) {
                    GZJSEleClick(e);
                    flag=true;
                    break;
                }
            }
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Not able to find any write checkbox on Project table");
            return false;
        }
        return flag;
    }


    @FindBy(xpath ="//table/tbody/tr/td[4]//input")
    List<WebElement> defaultIssueTypeList;
    public boolean checkIfDefaultIssueTypeSelected() {
        boolean flag=false;
        try {
            GZWait(3, refreshIconOnProjectPage, 10);
            for(WebElement e : defaultIssueTypeList){
                String issueType=getHiddenElementValue(e);
                System.out.println("The value of issue type is" + ":" + issueType);
                if (!issueType.contains("Task")) {
                    selectValueInPicklist(e);
                    flag=true;
                    //clickSaveBtn();
                    break;
                }
            }
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Not able to find any issue type in Default issue type on Project table");
            return false;
        }
        return flag;
    }


    @FindBy(xpath ="//table/tbody/tr/td[5]//input")
    List<WebElement> issueTypeToSyncList;
    public boolean checkIfIssueTypeToSyncSelected() {
        boolean flag=false;
        try {
            GZWait(3, refreshIconOnProjectPage, 10);
            for(WebElement e : issueTypeToSyncList){
                String issueTypeToSyncList=getHiddenElementValue(e);
                System.out.println("The value of issue type is" + ":" + issueTypeToSyncList);
                if (!issueTypeToSyncList.contains("Task")) {
                    selectValueInPicklist(e);
                    flag=true;
                    clickSaveBtn();
                    break;
                }
            }
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Not able to find any issue type in Default issue type on Project table");
            return false;
        }
        return flag;
    }








}
