package GZProducts.Sinergify.SinergifyUtilities.POM;

import CommonUtilities.GZBrowser.GZBrowser;
import CommonUtilities.GZCommonFuncs.GZCommonElementFuncs;
import CommonUtilities.GZLogger.GZLogger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class RuleSetPage extends GZCommonElementFuncs {
    public RuleSetPage() {
        PageFactory.initElements(GZBrowser.getDriver(), this);
    }

    GZCommonElementFuncs eleFunc = null;
    public String descText="This is a test description";


    @FindBy(xpath = "//div[@id='Addneawruleset']//div[@class='addruleimg']")
    WebElement addNewRuleBtn;
    @FindBy(xpath = "//div[@id='newruleset']//input[@class='form-control']")
    WebElement sn;
    @FindBy(xpath = "//label[contains(text(),'Active')]//following::input[@type='checkbox']")
    WebElement activeChkBx;
    @FindBy(xpath = "//label[contains(text(),'Description:')]//following::textarea")
    WebElement des;
    @FindBy(className = "projectselect")
    WebElement selProject;
    @FindBy(xpath = "//label[contains(text(),'Select Object')]//following::select[@onselect='allField']")
    WebElement selObj;
    @FindBy(className = "fieldName")
    WebElement selField;
    @FindBy(className = "fieldFilter")
    WebElement selOperator;
    @FindBy(xpath = "//span[contains(@id,'table')]/input[@type='text']")
    WebElement valueBox;
    @FindBy(xpath = "//a[@class=' adddiv']")
    WebElement addRuleIcon;
    @FindBy(xpath = "//div[@class='filtertoolpoint']/following::input[@value='Save']")
    WebElement saveBtn;

    public void clickAddNewRule() {
        addNewRuleBtn.click();
    }

    public void setSn() {
        sn.sendKeys("1");
    }

    public void clickActiveBx() {
        activeChkBx.click();
    }

    public void setDes() {
        des.sendKeys("Case closed should be True");
    }

    public void selectProject() {
        eleFunc = new GZCommonElementFuncs();
        selProject.click();
        eleFunc.selectDdValByIndex(selProject, 1);
    }

    public void selectObj() {
        eleFunc = new GZCommonElementFuncs();
        selObj.click();
        eleFunc.selectDdValByIndex(selObj, 1);
    }

    public void selRuleField() {
        selField.click();
        eleFunc.selectDdVal(selField, "Closed");
    }

    public void selRuleOperator() {
        selOperator.click();
        eleFunc.selectDdVal(selOperator, "equals");
    }

    public void setValue() {
        valueBox.sendKeys("True");
    }

    public boolean clickSaveBtn() {
        boolean status = false;
        try {
            saveBtn.click();
            status = true;
        } catch (Exception e) {
            GZLogger.ErrorLog(e, e.getMessage());
        }
        return status;
    }

    /*ALL RULESET PAGE ELEMENTS AND METHODS OCT-2021*/


    @FindBy(xpath = "//span[normalize-space()='Rule Set']")
    WebElement rulesetOption;

    public boolean rulesetOptionIsVisible() {
        try {
            GZWait(3, rulesetOption, 1);
            if (rulesetOption.isDisplayed()) {
                GZLogger.ActivityLog("Rule Set option is visible in side menu");
                return true;
            }
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Not able to see Rule Set option in side menu");
        }
        return false;
    }

    public boolean clickRulesetOption() {
        try {
            GZWait(3, rulesetOption, 5);
            if (rulesetOption.isDisplayed()) {
                rulesetOption.click();
                GZLogger.ActivityLog("Clicked on the Ruleset option in side menu");
                return true;
            }
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Not able to click on the Ruleset option in Side menu");
            return false;
        }
        return false;
    }

    @FindBy(xpath = "//div[text()='Rule Set']")
    WebElement rulesetHeader;

    @FindBy(xpath = "//div[contains(@class,'flagposition')]")
    WebElement flagDiv;

    @FindBy(xpath = "//button[contains(text(),'Add New Rule')]")
    WebElement addNewRuleButton;

    public boolean ifAllElementsVisibleOnRulesetPage() {
        try {
            GZWait(3, flagDiv, 10);
            GZWait(3,addNewRuleButton,5);
            GZWait(3,rulesetHeader,5);
            if (flagDiv.isDisplayed() & addNewRuleButton.isDisplayed() & rulesetHeader.isDisplayed()) {
                GZLogger.ActivityLog("All elements are visible on the Ruleset page");
                return true;
            }
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Not able to see all the elements on the Ruleset page");
            return false;
        }
        return false;
    }

    public boolean isAddNewRuleButtonVisible() {
        try {
            GZWait(3, addNewRuleButton, 5);
            if (addNewRuleButton.isDisplayed()) {
                GZLogger.ActivityLog("Add New Rule button is displayed");
                return true;
            }
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Not able to see the Add New Rule button");
            return false;
        }
        return false;
    }

    @FindBy(xpath = "//div[contains(text(),'Add New Rule')]")
    WebElement addNewRuleHeader;

    public boolean isAddNewRuleFormVisible(){
        try{
            GZWait(3,addNewRuleHeader,7);
            if(addNewRuleHeader.isDisplayed())
                return true;
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to view the Add New Rule form");
            return false;
        }
        return false;
    }

    public boolean clickAddNewRuleBtn() {
        try {
            GZWait(3, addNewRuleButton,3);
            if(isAddNewRuleButtonVisible()){
                GZJSEleClick(addNewRuleButton);
                GZLogger.ActivityLog("Clicked on the Add New Rule button");
                return true;
            }
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Not able to click in Add New Rule button");
            return false;
        }
        return false;
    }

    /****************************************************************/
        /*ALL PAGE ELEMENTS AND METHODS OF VALIDATION PAGE*/
    /****************************************************************/




    @FindBy(xpath = "//span[contains(@class,'slds-checkbox_faux')]/span[contains(text(),'Disabled')]")
    WebElement activeToggleDisabled;

    @FindBy(xpath = "//span[contains(@class,'slds-checkbox_faux')]/span[contains(text(),'Enabled')]")
    WebElement activeToggleEnabled;

    public boolean clickActiveToggleDisable(){
        try{
            GZWait(2,activeToggleDisabled,5);
            if(activeToggleDisabled.isDisplayed()){
                GZJSEleClick(activeToggleDisabled);
                GZLogger.ActivityLog("Clicked on the Active toggle-disabled");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to Click on the Active toggle-disabled\"");
            return false;
        }
        return false;
    }

    public boolean clickActiveToggleEnabled(){
        try{
            GZWait(3,activeToggleEnabled,5);
            if(activeToggleEnabled.isDisplayed()){
                GZJSEleClick(activeToggleEnabled);
                GZLogger.ActivityLog("Clicked on the Active toggle-Enabled");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click Active toggle-Enabled");
            return false;
        }
        return false;
    }

    @FindBy(xpath = "//div[contains(@id,'modal-content-id')]//textarea[@name='input2']")
    WebElement descriptionBox;

    @FindBy(xpath = "//div[@class='addscrol']//input[@placeholder='Select an Option']")
    WebElement projectDropdown;

    @FindBy(xpath = "//c-addrule//lightning-base-combobox//input[@role='combobox'][@placeholder='Select an Option']")
    WebElement objectDropdown;

    @FindBy(xpath = "//label[contains(text(),'Filter Logic')]/following::input[1]")
    WebElement filterLoginInput;

    @FindBy(xpath = "//label[contains(text(),'Filter Logic')]/following::input[2]")
    WebElement errMsgInput;

    @FindBy(xpath = "//button[contains(@class,'savebutton')]")
    WebElement saveBtnRuleset;

    @FindBy(xpath = "//button[@name='Close']")
    WebElement closeBtnRuleset;


    @FindBy(xpath = "//label[contains(text(),'Project')]/following::li//span")
    WebElement project;

    @FindBy(xpath = "//div[@data-key='success']")
    WebElement instanceSuccessMessage;


    public boolean clickSaveBtnRuleset(){
        try{
            GZWait(3,saveBtnRuleset,5);
            if(saveBtnRuleset.isDisplayed() || saveBtnRuleset.isEnabled()){
                GZJSEleClick(saveBtnRuleset);
                GZLogger.ActivityLog("Clicked on the Save button - Ruleset Page");
                GZWait(3,instanceSuccessMessage,9);
                if(instanceSuccessMessage.isDisplayed())
                    if(instanceSuccessMessage.getText().contains("Success"))
                        GZLogger.ActivityLog("Saved the new Ruleset rule");
                if(findSnInRulesetTable())
                    return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to Save Ruleset Page");
            return false;
        }
        return false;
    }

    public boolean enterFilterLogic(){
        try{
            GZWait(3,filterLoginInput,5);
            if(filterLoginInput.isDisplayed()){
                filterLoginInput.sendKeys("1");
                GZLogger.ActivityLog("Entered value in the Filter logic input");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e," Not able to add value in Filter login input box");
            return false;
        }
        return false;
    }

//    public boolean enterErrMsg(){
//        try{
//            GZWait(3,errMsgInput,5);
//            if(errMsgInput.isDisplayed()){
//                GZJSEleClick(errMsgInput);
//                errMsgInput.sendKeys("Please fix the errors!");
//                GZLogger.ActivityLog("Entered value in the Error Message input");
//                return true;
//            }
//        }catch (Exception e){
//            GZLogger.ErrorLog(e," Not able to add value in Error Message input box");
//            return false;
//        }
//        return false;
//    }


    public boolean clickSelectProject(){
        try{
            GZWait(2,projectDropdown,5);
            if(projectDropdown.isDisplayed()){
                GZJSEleClick(projectDropdown);
                GZLogger.ActivityLog("Clicked on the Project dropdown - Ruleset Page");
                GZWait(3,project,3);
                project.click();
                GZLogger.ActivityLog("Selected the value  from the Project dropdown - Ruleset Page");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click & Select the Project- Ruleset Page");
            return false;
        }
        return false;
    }

    public boolean clickSelectObject(){
        try{
            GZWait(3,objectDropdown,5);
            if(objectDropdown.isDisplayed()){
                GZJSEleClick(objectDropdown);
                GZWait(3,objectDropdown,5);
                GZLogger.ActivityLog("Clicked on the Project dropdown - Ruleset Page");
                enterKeyPress(objectDropdown);
                GZLogger.ActivityLog("Selected the value from the Project dropdown - Ruleset Page");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click & Select the Project- Ruleset Page");
            return false;
        }
        return false;
    }

    @FindBy(xpath = "//table/tbody/tr[1]/td[2]//input[@name='field']")
    WebElement fieldNameDrpDwn;

    @FindBy(xpath = "//table/tbody/tr[1]/td[3]//input[@name='1']")
    WebElement operatorDrpDwn;

    @FindBy(xpath = "//table/tbody/tr[1]/td[2]//lightning-base-combobox-item//span[@title='IsClosed']")
    WebElement fieldNameVal;

    @FindBy(xpath = "//table/tbody/tr[1]/td[3]//lightning-base-combobox-item//span[contains(text(),'equals')]")
    WebElement operatorVal;

    @FindBy(xpath = "//table/tbody/tr[1]/td[4]//input[@value='checkbox']")
    WebElement valueCheckbox;

    public boolean clickSelectFieldName(){
        try{
            GZWait(3,fieldNameDrpDwn,3);
            if(fieldNameDrpDwn.isDisplayed()){
                GZJSEleClick(fieldNameDrpDwn);
                GZWait(3,fieldNameVal,5);
                GZScrollBtm(fieldNameVal);
                GZJSEleClick(fieldNameVal);
                GZLogger.ActivityLog("Clicked and Selected the Field name value");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click & select a Field name value");
            return false;
        }
        return false;
    }

    public boolean clickSelectOperator(){
        try{
            GZWait(3,operatorDrpDwn,3);
            if(operatorDrpDwn.isDisplayed()){
                GZWait(0);
                GZJSEleClick(operatorDrpDwn);
                GZWait(3,operatorVal,5);
                GZScrollBtm(operatorVal);
                GZJSEleClick(operatorVal);
                GZLogger.ActivityLog("Clicked and Selected the Operator value");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click & select a Operator value");
            return false;
        }
        return false;
    }
    public boolean clickValueCheckBox(){
        try{
            GZWait(3,valueCheckbox,3);
            if(valueCheckbox.isDisplayed()){
                GZJSEleClick(valueCheckbox);
                GZLogger.ActivityLog("Clicked on the Value checkbox - Ruleset page");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the value check box - Ruleset page");
            return false;
        }
        return false;
    }

    public boolean enterDescription(){
        try{
            GZWait(3,descriptionBox,3);
            if(descriptionBox.isDisplayed()){
                descriptionBox.sendKeys(descText);
                GZLogger.ActivityLog("Entered the description - Ruleset page");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to the description - Ruleset page");
            return false;
        }
        return false;
    }

    @FindBy(xpath = "//div[@class='mainLabel']//table")
    WebElement valTable;

    @FindBy(xpath = "//div[@class='mainLabel']//table/tbody/tr/td[5]/div")
    List<WebElement> descList;

    public boolean findSnInRulesetTable(){
        try{
            GZWait(3,valTable,5);
            if(valTable.isDisplayed()){
                GZLogger.ActivityLog("Able to see the table of recently added Ruleset");
                for(int i=0;i<descList.size();i++){
                    WebElement desc=descList.get(i);
                    if(desc.getText().contains(descText)){
                        GZLogger.ActivityLog("Able to see the recently added Ruleset");
                        return true;
                    }
                }
            }
            GZLogger.FailLog("Not able to find the recently added Ruleset");
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to see the table of added Ruleset");
            return false;
        }
        return false;
    }


    @FindBy(xpath = "//div[@class='mainLabel']//table/tbody/tr[1]/td[6]//span[contains(@title,'Edit rule')]/img")
    WebElement editRuleIcon;

    public boolean clickEditRuleBtn(){
        try{
            GZWait(3,editRuleIcon,5);
            if(editRuleIcon.isDisplayed()){
                GZJSEleClick(editRuleIcon);
                GZLogger.ActivityLog("Clicked on the Edit Rule icon");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Edit Rule button");
            return false;
        }
        return false;
    }

    public boolean editDescriptionInRuleset() {
        try {
            GZWait(3, descriptionBox, 5);
            if (descriptionBox.isDisplayed()) {
                GZWait(2000);
                descriptionBox.clear();
                GZWait(0);
                descriptionBox.sendKeys(descText + " " + "edited");
                if(saveBtnRuleset.isDisplayed()) {
                    GZScrollBtm(saveBtnRuleset);
                    GZJSEleClick(saveBtnRuleset);
                    GZWait(3, instanceSuccessMessage, 8);
                    if (instanceSuccessMessage.isDisplayed()) {
                        GZLogger.ActivityLog("Edited the description of the Ruleset page");
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Not able to update the Ruleset rule");
            return false;
        }
        return false;
    }


    @FindBy(xpath = "//div[@class='mainLabel']//table/tbody/tr[1]/td[6]//span[contains(@title,'Delete')]/img")
    WebElement deleteRuleIcon;

    @FindBy(xpath = "//div[contains(@id,'modal-content-id')]/p[contains(text(),'Do you want to delete')]")
    WebElement delValRuleModal;

    @FindBy(xpath = "//div[contains(@id,'modal-content-id')]//button[contains(text(),'OK')]")
    WebElement okBtnDelValModal;

    public boolean clickDeleteRuleBtn(){
        try{
            GZWait(3,deleteRuleIcon,5);
            if(deleteRuleIcon.isDisplayed()){
                GZJSEleClick(deleteRuleIcon);
                GZLogger.ActivityLog("Clicked on the Delete Rule icon");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Delete Rule button");
            return false;
        }
        return false;
    }

    public boolean clickOkBtnRuleset() {
        try {
            GZWait(3, delValRuleModal, 5);
            if (delValRuleModal.isDisplayed() & delValRuleModal.getText().contains("delete")) {
                GZJSEleClick(okBtnDelValModal);
                GZWait(3,addNewRuleButton,5);
                if(addNewRuleButton.isDisplayed()) {
                    GZLogger.ActivityLog("Deleted the Ruleset rule");
                    return true;
                }

            }
        }catch (Exception e) {
            GZLogger.ErrorLog(e, "Not able to delete the Ruleset rule");
            return false;
        }
        return false;
    }


}