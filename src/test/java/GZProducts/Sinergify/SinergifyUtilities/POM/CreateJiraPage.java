package GZProducts.Sinergify.SinergifyUtilities.POM;

import CommonUtilities.GZBrowser.GZBrowser;
import CommonUtilities.GZCommonFuncs.GZCommonElementFuncs;
import CommonUtilities.GZLogger.GZLogger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.Map;

public class CreateJiraPage extends GZCommonElementFuncs {

    public CreateJiraPage(){
        PageFactory.initElements(GZBrowser.getDriver(), this);
    }

    @FindBy(xpath = "//div[contains(@class,'appLauncherMenu')]//one-app-launcher-menu-item")
    WebElement appLauncherMenuItem;

    @FindBy(xpath = "//input[@name='Case-search-input' and @placeholder='Search this list...']")
    WebElement caseSearchBox;

    @FindBy(xpath = "//img[@title='Cases']")
    WebElement caseImgIcon;

    @FindBy(xpath = "//table[@data-aura-class='uiVirtualDataTable']//tbody/tr/th//a")
    WebElement caseRecordTitle;

    @FindBy(xpath = "//div[@role='menu']//a[@name='Create_Jira']/span")
    WebElement createJiraBtn;

    @FindBy(xpath = "//div[contains(@class,'searchCss1')]//header")
    WebElement createJiraFormHeading;

    @FindBy(xpath = "//div[contains(@class,'searchCss1')]//header//b[contains(text(),'Potential Results')]")
    WebElement potentialResultHeading;

    @FindBy(xpath = "//footer/div[contains(text(),'Cancel')]")
    WebElement cancelBtn;

    @FindBy(xpath = "//footer/div[contains(text(),'Save')]")
    WebElement saveBtn;

    @FindBy(xpath = "//footer/div[contains(text(),'Add Comments and Files')]")
    WebElement addCommentFileBtn;

    @FindBy(xpath = "//label[contains(text(),'Summary')]/following::input[1]")
    //@FindBy(xpath = "//div[@class='slds-accordion__content']//div[@class='columnCss2'][1]//input")
    WebElement summaryInputBox;

    @FindBy(xpath = "//label[contains(text(),'Reporter')]/following::input[1]")
    //@FindBy(xpath = "//div[@class='slds-accordion__content']//div[@class='columnCss2'][2]//input")
    WebElement reporterInputBox;


    @FindBy(xpath = "//label[contains(text(),'Reporter')]/following::div/ul/li//span[2]")
    //@FindBy(xpath = "//div[@class='slds-accordion__content']//div[@class='columnCss2'][2]//div[@role='listbox']/ul/li//span[2]")
    WebElement reporterValueList;

    ////div[@role='listbox']/ul/li[contains(@class,'useritemlist')]

    @FindBy(xpath = "//lightning-textarea/label[contains(text(),'Description')]/following::div[1]/textarea")
    //@FindBy(xpath = "//div[@class='slds-accordion__content']//div[@class='columnCss2'][4]//textarea")
    WebElement descInputBox;

    @FindBy(xpath = "//flexipage-component2[@data-component-id='viewJira']//button[contains(text(),'Edit')]")
    WebElement editJiraBtn;

    @FindBy(xpath = "//flexipage-component2[@data-component-id='viewJira']//button[contains(text(),'Send Comments/Attachments To Jira')]")
    WebElement sendCommentBtn;

    @FindBy(xpath = "//flexipage-component2[@data-component-id='viewJira']//button[contains(text(),'Subtask')]")
    WebElement subtaskBtn;

    @FindBy(xpath = "//flexipage-component2[@data-component-id='viewJira']//lightning-button-menu/button")
    WebElement showMenuIconViewJiraPage;

    @FindBy(xpath = "//button[@title='Primary action' and contains(text(),'Add Comment')]")
    WebElement addCommentBtn;

    @FindBy(xpath = "//div/header/h2[contains(text(),'New Comment')]")
    WebElement newCommentHeader;

    @FindBy(xpath = "//h3/b[contains(text(),'Information')]")
    WebElement informationLabel;

    @FindBy(xpath = "//textarea[@placeholder='type here...']")
    WebElement commentBodyInputBox;

    @FindBy(xpath = "//button[contains(text(),'Save')]")
    WebElement saveBtnCommentModal;

    @FindBy(xpath = "//lightning-accordion[@class='accordion slds-accordion']/slot/div[1]//tbody/tr[1]/th//lightning-base-formatted-text")
    WebElement commentBodyTextInTable;

    @FindBy(xpath = "//button[contains(text(),'Send to JIRA')]")
    WebElement sendToJIRABtn;

    @FindBy(xpath = "//button[contains(text(),'Back')]")
    WebElement backBtn;

    @FindBy(xpath = "//div[@class='slds-file-selector slds-file-selector_files']//*[local-name()='svg']")
    WebElement uploadFileBtn;

    @FindBy(xpath = "//div[@class='modal-footer slds-modal__footer']//button")
    WebElement doneBtn;

    @FindBy(xpath = "//lightning-icon//*[local-name()='svg' and @data-key='success']")
    WebElement successIcon;

    @FindBy(xpath = "//div[@data-id='slds-modal__container1234']//div[@class='slds-border_left']//div")
    List<WebElement> potentialList;

    @FindBy(xpath = "//label[contains(text(),'Issue Type')]/following-sibling::div//input")
    WebElement issueTypeDropdown;

    @FindBy(xpath = "//span[contains(text(),'Refresh')]/following::button[@title='Refresh']")
    WebElement refreshBtn;

    @FindBy(xpath = "//a[contains(text(),'Link')]")
    WebElement linkBtn;

    @FindBy(xpath = "//flexipage-component2[contains(@data-component-id,'JiraRelatedListLightning')]//tbody/tr[1]/th")
    WebElement jiraNameKey;


    @FindBy(xpath = "//flexipage-component2[contains(@data-component-id,'JiraRelatedListLightning')]//table")
    WebElement relatedListTable;

    @FindBy(xpath = "//div[contains(text(),'Status')]/following::div[1]//lightning-primitive-icon/*[name()='svg' and @data-key='edit']")
    WebElement editIconViewjira;

    @FindBy(xpath = "//div[contains(text(),'Status')]/following::div[@data-id='_select']//input")
    WebElement jiraStatusDropdownIcon;


    public boolean clickMenuItem(){
        try{
            GZWait(3,appLauncherMenuItem,10);
            if(appLauncherMenuItem.isDisplayed()){
                appLauncherMenuItem.click();
                GZLogger.ActivityLog("Clicked on the menu item in App launcher");
                return true;
            }
            else
                GZLogger.ActivityLog("Not able to find the item in App menu");
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on menu item in App launcher");
        }
        return false;
    }

    public boolean enterAndSearchCase(String caseNumber){
        try{
            GZWait(3,caseSearchBox,10);
            if(caseSearchBox.isDisplayed()){
                caseSearchBox.clear();
                caseSearchBox.sendKeys(String.valueOf(caseNumber));
                caseSearchBox.sendKeys(Keys.RETURN);
                GZLogger.ActivityLog("Entered the"+" "+caseNumber+" "+ "in the search box");
//                GZWait(3,caseRecordTitle,20);
                GZWait(0);
                if(caseRecordTitle.isDisplayed()){
                    String actualCaseNumber= caseRecordTitle.getAttribute("title");
                    GZWait(0);
                    if(actualCaseNumber.equalsIgnoreCase(String.valueOf(caseNumber))){
                        GZLogger.ActivityLog("The entered case number"+" "+caseNumber+" "+"is found");
                        return true;
                    }
                }
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to find the Case search box");
            return false;
        }
        return false;
    }

    public boolean clickOnCase(String caseNumber){
        try{
            GZWait(3,caseRecordTitle,20);
            if(caseRecordTitle.isDisplayed()){
               // caseRecordTitle.click();
                GZJSEleClick(caseRecordTitle);
                GZLogger.ActivityLog("Clicked on the"+" "+caseNumber);
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Case:"+" "+caseNumber);
            return false;
        }
        return false;
    }

    @FindBy(xpath = "//ul[@class='slds-button-group-list']/li[4]/lightning-button-menu/button")
    WebElement dropdownIcon;


    public boolean clickDropdownIcon() {
        try {
            GZWait(3, dropdownIcon, 20);
            if (dropdownIcon.isDisplayed()) {
                dropdownIcon.click();
                GZLogger.ActivityLog("Clicked on the Show more actions dropdown icon");
                return true;
            }
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Not able to click on the Dropdown icon");
            return false;
        }
        return false;
    }

    public boolean clickCreateJiraBtn(){
        try{
            GZWait(3,dropdownIcon,20);
            if(dropdownIcon.isDisplayed()){
                dropdownIcon.click();
                GZLogger.ActivityLog("Clicked on the Show more actions dropdown icon");
                GZWait(3,createJiraBtn,6);
                if(createJiraBtn.isDisplayed()){
                    createJiraBtn.click();
                    GZLogger.ActivityLog("Clicked on the Create Jira button");
                    return true;
                }else
                    GZLogger.ActivityLog("Not able to click on Create Jira button");
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on Show more actions dropdown icon");
            return false;
        }
        return false;
    }

    public boolean isCreateJiraBtnVisible(){
        try{
            GZWait(3,createJiraBtn,10);
            if (createJiraBtn.isDisplayed()){
                GZLogger.ActivityLog("Create JIRA button is visible");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Create JIRA button is not visible");
            return false;
        }
        return false;
    }

    public boolean buttonsOnCreateJiraForm_Visible(){
        try {
            GZWait(3,saveBtn,10);
            if(cancelBtn.isDisplayed())
                if (saveBtn.isDisplayed())
                    if(addCommentFileBtn.isDisplayed()){
                        GZLogger.ActivityLog("All buttons (Cancel, Save, Add Comments & File) visible");
                        return true;
                    }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to see buttons (Cancel, Save, Add Comments & File) ");
            return false;
        }
        return false;
    }

    public boolean clickCloseJiraBtn(){
        try{
            GZWait(3,cancelBtn,10);
            if(cancelBtn.isDisplayed()){
                cancelBtn.click();
                GZLogger.ActivityLog("Clicked on Cancel button");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on Cancel button");
            return false;
        }
        return false;
    }

    public boolean cancelBtnVisible(){
        try{
            GZWait(4,cancelBtn,3);
            if(cancelBtn.isDisplayed()){
                return true;
            }
        }catch (Exception e){
            return false;
        }
        return false;
    }



    public boolean headersOnCreateJiraForm_Visible(){
        try {
            GZWait(3,createJiraFormHeading,15);
            if (createJiraFormHeading.isDisplayed())
                if(potentialResultHeading.isDisplayed()){
                        GZLogger.ActivityLog("Create New Jira Issue form is visible");
                        return true;
                }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to see the 'Create New Jira Issue' form");
            return false;
        }
        return false;
    }

    @FindBy(xpath = "//h2[contains(text(),'Add Comments and Files')]")
    WebElement addCommentFilesHeader;

    @FindBy(xpath = "//lightning-input[@data-id='checkbox-1']")
    WebElement syncCaseCmntCheckbox;

    @FindBy(xpath = "//lightning-input[@data-id='checkbox-2']")
    WebElement syncAttachSObj;

    public boolean isAddCommentBtnVisible(){
        try{
            GZWait(3,addCommentBtn,20);
            if(addCommentBtn.isDisplayed()){
                GZLogger.ActivityLog("Add Comment button is visible");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Add Comment button is not visible");
            return false;
        }
        return false;
    }

    public boolean isAddCommentFileHeaderVisible(){
        try{
            GZWait(3,addCommentFilesHeader,20);
            GZWait(0);
            if(addCommentFilesHeader.isDisplayed()){
                GZLogger.ActivityLog("Add Comment and File haeder is visible");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Add Comment and File haeder is not visible");
            return false;
        }
        return false;
    }
    public boolean isSendToJIRABtnVisible(){
        try{
            GZWait(3,sendToJIRABtn,10);
            if(sendToJIRABtn.isDisplayed()){
                GZLogger.ActivityLog("'Send to JIRA' button is visible");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"'Send to JIRA' button is not visible");
            return false;
        }
        return false;
    }
    public boolean isBackBtnVisible(){
        try{
            GZWait(3,backBtn,10);
            if(backBtn.isDisplayed()){
                GZLogger.ActivityLog("'Back' button is visible");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"'Back' button is not visible");
            return false;
        }
        return false;
    }

    public boolean isSyncCaseComntCheckboxVisible(){
        try{
            GZWait(3,syncCaseCmntCheckbox,10);
            if(syncCaseCmntCheckbox.isDisplayed()){
                GZLogger.ActivityLog("'Sync as Case Comment' checkbox is visible");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"'Sync as Case Comment' checkbox is not visible");
            return false;
        }
        return false;
    }

    public boolean isSyncAttchSObjCheckboxVisible(){
        try{
            GZWait(3,syncAttachSObj,10);
            if(syncAttachSObj.isDisplayed()){
                GZLogger.ActivityLog("'Sync attachment to SFDC Object' checkbox is visible");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"'Sync attachment to SFDC Object' checkbox is not visible");
            return false;
        }
        return false;
    }

    public boolean enterSummary(String summary){
        try{
            GZWait(2,summaryInputBox,12);
            if(summaryInputBox.isEnabled()){
                summaryInputBox.clear();
                summaryInputBox.sendKeys(summary);
//                clickIssueTypeDropdown(); checking here the step to click on Issue type dropdown
                GZLogger.ActivityLog("Entered the text:"+" "+summary);
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to enter the summary");
            return false;
        }
        return false;
    }


    public boolean enterReporter(String reporter){
        try{
            GZWait(3,reporterInputBox,5);
            if(reporterInputBox.isDisplayed()){
                reporterInputBox.sendKeys(reporter);
                GZLogger.ActivityLog("Entered the reporter name:"+" "+reporter);
                GZWait(3,reporterValueList,10);
                if(reporterValueList.isDisplayed()) {
                    GZJSEleClick(reporterValueList);
                    GZLogger.ActivityLog("Selected the reporter as:"+" "+reporter);
                    return true;
                }
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to enter/select the reporter as:"+" "+reporter);
            return false;
        }
        return false;
    }

    public boolean enterDescription(String description){
        try{
            GZWait(3,descInputBox,8);
            if(descInputBox.isDisplayed()){
                descInputBox.clear();
                descInputBox.sendKeys(description);
                GZLogger.ActivityLog("Entered the description as:"+" "+description);
                    return true;
                }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to enter the description");
            return false;
        }
        return false;
    }
    @FindBy(xpath = "//div[@data-key='success']")
    WebElement instanceSuccessMessage;

    public boolean clickSaveBtn(){
        try{
            GZWait(3,saveBtn,10);
            if (saveBtn.isDisplayed()){
                saveBtn.click();
                GZLogger.ActivityLog("Clicked on the Save button - Create a New Jira Issue");
                GZWait(3,instanceSuccessMessage,20);
                if (instanceSuccessMessage.isDisplayed()){
                    GZLogger.ActivityLog("New Jira Issue is created");
                    return true;
                }
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Save button - Create a New Jira Issue");
            return false;
        }
        return false;
    }

    public boolean clickEditJiraBtn(){
        try{
            GZWait(0);
            GZWait(3,editJiraBtn,20);
            if(editJiraBtn.isDisplayed()){
                GZJSEleClick(editJiraBtn);
                GZLogger.ActivityLog("Clicked on the Edit button- View Jira Page");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Edit Button - View Jira Page");
            return false;
        }
        return false;
    }



    public boolean clickSendCommentAttchBtn(){
        try{
            GZWait(3,addCommentFileBtn,20);
            if(addCommentFileBtn.isDisplayed()){
                addCommentFileBtn.click();
                GZLogger.ActivityLog("Clicked on the Send Comments/Attachment button- View Jira Page");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the  Send Comments/Attachment button- View Jira Page");
            return false;
        }
        return false;
    }

    public boolean clickAddCommentBtn(){
        try{
            GZWait(3,addCommentBtn,15);
            if (addCommentBtn.isDisplayed()){
                GZWait(0);
                addCommentBtn.click();
                GZLogger.ActivityLog("Clicked on Add Comment button");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on Add Comment button");
            return false;
        }
        return false;
    }

    public boolean enterComment(String comment){
        try{
            GZWait(3,commentBodyInputBox,10);
            if(commentBodyInputBox.isDisplayed()){
                commentBodyInputBox.sendKeys(comment);
                GZLogger.ActivityLog("Entered the comment:"+" "+comment);
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to enter the comment");
            return false;
        }
        return false;
    }
    public boolean clickSaveBtnComment(){
        try{
            GZWait(3,saveBtnCommentModal,5);
            if (saveBtnCommentModal.isDisplayed()){
                saveBtnCommentModal.click();
                GZLogger.ActivityLog("Clicked on the Save button");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Save button");
            return false;
        }
        return false;
    }

    public boolean clickSubtaskBtn(){
        try{
            GZWait(3,subtaskBtn,20);
            if(subtaskBtn.isDisplayed()){
                subtaskBtn.click();
                GZLogger.ActivityLog("Clicked on the Subtask button- View Jira Page");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Subtask button- View Jira Page");
            return false;
        }
        return false;
    }

    public boolean getText(Map<String,String> map){
        try{
            GZScrollBtm(commentBodyTextInTable);
            GZWait(3,commentBodyTextInTable,5);
            if (commentBodyTextInTable.isDisplayed()){
                String text=commentBodyTextInTable.getText();
                if (text.contains(map.get("CaseComment"))){
//                    System.out.println(commentBodyTextInTable.getText());
                    GZLogger.ActivityLog("Get Text from the table");
                    return true;
                }
            }
        }catch (Exception e){
            return false;
        }
        return false;
    }


    public boolean clickSendToJiraBtn(){
        try{
            GZWait(2,sendToJIRABtn,20);
            if (sendToJIRABtn.isDisplayed()){
                sendToJIRABtn.click();
                GZLogger.ActivityLog("Clicked on the SendToJIRA button");
                GZWait(3,instanceSuccessMessage,25);
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the SendToJIRA button");
            return false;
        }
        return false;
    }

    public boolean clickUploadFileBtn(){
        try{
            GZWait(3,uploadFileBtn,10);
            if (uploadFileBtn.isEnabled()){
                GZWait(0);
                uploadFileBtn.click();
                GZWait(0);
                GZLogger.ActivityLog("Clicked on the Upload Files button");
                Runtime.getRuntime().exec("C:\\Users\\nikhil.kumar\\Desktop\\Sinergify\\AutoIT\\fileupload.exe");
                GZLogger.ActivityLog("Uploaded the file succesfully");
                if (clickDoneBtn())
                    return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Upload File button");
            return false;
        }
        return false;
    }

    public boolean clickDoneBtn(){
        try{
            GZWait(2,doneBtn,20);
            if (doneBtn.isEnabled()){
               doneBtn.click();
                GZLogger.ActivityLog("Clicked on the Done button");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on Done button");
            return false;
        }
        return false;
    }

    public boolean potentialResultsCount(){
        try{
            GZWait(3,potentialResultHeading,10);
            GZWait(0);
            if (potentialList.size()>1){
                GZLogger.ActivityLog("See the matching potential results");
                return true;
            }else {
                GZLogger.FailLog("Can't see the matching potential results");
                return false;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to find the potential results");
            return false;
        }
    }

    public boolean clickIssueTypeDropdown(){
        try{
            GZWait(3,issueTypeDropdown,10);
            if (issueTypeDropdown.isDisplayed()){
                issueTypeDropdown.click();
                GZLogger.ActivityLog("Clicked on the Issue Type dropdown");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Issue Type dropdown");
            return false;
        }
        return false;
    }

    @FindBy(xpath = "//nav//one-app-nav-bar-item-root[last()]/a")
    WebElement jiraIssueNumber;

    public String getJiraNumberFromNavBar(){
        String jiraNumber=null;
        try{
            GZWait(3,jiraIssueNumber,20);
            if (jiraIssueNumber.isDisplayed()){
                GZWait(0);
                jiraNumber=jiraIssueNumber.getAttribute("title");
                //System.out.println(jiraNumber);
                if(jiraNumber!=null){
                    GZLogger.ActivityLog("Able to find the Jira Number"+" "+jiraNumber+"");
                    return jiraNumber;
                }else
                    GZLogger.ActivityLog("Not able to the Jira Number");
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to get the Jira number from the Nav bar");
            return null;
        }
        return jiraNumber;
    }

    @FindBy(xpath = "//flexipage-component2[contains(@data-component-id,'JiraRelatedListLightning')]//tbody/tr/th[1]")
    List<WebElement> jiraIssueList;

    public boolean iterateListOfJiraNumber() throws InterruptedException {
        try{
            GZWait(0);
            GZWait(3,refreshBtn,10);
            GZJSEleClick(refreshBtn);
            int sizeList=jiraIssueList.size();
            String jiraNum1=getJiraNumberFromNavBar();
            for(WebElement ele:jiraIssueList){
                String jiraNum=ele.getText();
                if (jiraNum.equalsIgnoreCase(jiraNum1)){
                    GZLogger.ActivityLog("The Jira issue is found in the Related list");
                    return true;
                }
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to find the Jira in the list");
            return false;
        }
        return false;
    }

    public boolean clickLinkBtn(){
        try{
            GZWait(3,linkBtn,10);
            if (linkBtn.isEnabled()){
                linkBtn.click();
                GZLogger.ActivityLog("Clicked on the Link button");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Link button");
            return false;
        }
        return false;
    }

    public String getLinkedJiraNumber(){
        String jiraNum=null;
        try{
            GZWait(0);
            jiraNum=linkBtn.getAttribute("name");
            if (jiraNum!=null){
                GZLogger.ActivityLog("Found the Jira number:"+jiraNum+"");
                return jiraNum;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to find the Jira number of the Linked Jira");
            return null;
        }
        return null;
    }

    public boolean verifySuccessMsg(){
        try {
            GZWait(3,instanceSuccessMessage,11);
            if (instanceSuccessMessage.isDisplayed()){
                GZLogger.ActivityLog("Able to see the Success message");
                return true;
            }
        }catch (Exception e){
            GZLogger.ActivityLog("Not able to see the Success message");
            return false;
        }
        return false;
    }

    public boolean iterateListOfLinkedJiraNumber(String jiraNum1) throws InterruptedException {
        try{
            GZWait(0);
            GZWait(3,refreshBtn,10);
            GZJSEleClick(refreshBtn);
            int sizeList=jiraIssueList.size();
            //String jiraNum1=getLinkedJiraNumber();
            for(WebElement ele:jiraIssueList){
                String jiraNum=ele.getText();
                if (jiraNum.equalsIgnoreCase(jiraNum1)){
                    GZLogger.ActivityLog("The Jira issue:"+" "+jiraNum1+" "+"is found in the Related list");
                    return true;
                }
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to find the Jira in the list");
            return false;
        }
        return false;
    }

    @FindBy(xpath = "//flexipage-component2[contains(@data-component-id,'JiraRelatedListLightning')]//tbody/tr[1]/td[last()]//button")
    WebElement showActionDownBtn;

    @FindBy(xpath = "//flexipage-component2[contains(@data-component-id,'JiraRelatedListLightning')]//a//span[contains(text(),'Show details')]")
    //@FindBy(xpath = "//flexipage-component2[contains(@data-component-id,'JiraRelatedListLightning')]//tbody/tr[1]/td[last()]//button//lightning-menu-item")
    WebElement showDetailsOption;

    public boolean clickShowActionBtn(){
        try{
            GZWait(3,showActionDownBtn,10);
            if (showActionDownBtn.isEnabled()){
                showActionDownBtn.click();
                GZLogger.ActivityLog("Clicked on the Show Action down button");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Show Action down button");
            return false;
        }
        return false;
    }
    @FindBy(xpath = "//flexipage-component2[contains(@data-component-id,'JiraRelatedListLightning')]//tbody/tr[1]/td[last()]//a[last()]/span[contains(text(),'Unlink')]")
    WebElement unlinkBtnOption;

    public boolean clickUnlinkBtn(){
        try{
            GZWait(3,unlinkBtnOption,10);
            if (unlinkBtnOption.isDisplayed()){
                unlinkBtnOption.click();
                GZLogger.ActivityLog("Clicked on the Unlink button");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on Unlink button");
            return false;
        }
        return false;
    }

    public boolean clickRefreshBtn(){
        try{
            GZWait(3,refreshBtn,10);
            if (refreshBtn.isDisplayed()){
                GZJSEleClick(refreshBtn);
                GZLogger.ActivityLog("Clicked on the Refresh button");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Refresh button");
            return false;
        }
        return false;
    }

    public boolean verifyUnlinkedJiraInRelatedList(String jiraNum1){
        try{
            GZWait(0);
            int sizeList=jiraIssueList.size();
        //String jiraNum1=getLinkedJiraNumber();
            for(WebElement ele:jiraIssueList){
                String jiraNum=ele.getText();
                if (jiraNum.equalsIgnoreCase(jiraNum1) || isRelatedListInvisible()){
                    GZLogger.ActivityLog("The Jira issue is unlinked from the Related list");
                    return true;
                }
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"The Jira issue was not unlinked from the Related list");
            return false;
        }
        return false;
    }

    public String getUnlinkedJiraNumber(){
        String jiraNum=null;
        try{
            GZWait(0);
            if (isRelatedListInvisible()==false){
                jiraNum=jiraNameKey.getText();
                if (jiraNum!=null){
                    GZLogger.ActivityLog("Found the Jira number:"+jiraNum+"");
                    return jiraNum;
                }
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to find the Jira number of the Linked Jira");
            return null;
        }
        return null;
    }


    public boolean isRelatedListInvisible(){
        try{
            GZWait(0);
            clickRefreshBtn();
            if (relatedListTable.isDisplayed()==false){
                GZLogger.ActivityLog("The related list is invisible now");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Still the related list is visible");
            return false;
        }
        return false;
    }

    public boolean isEditIconVisibleOnJiraStatus(){
        try{
            GZWait(3,editIconViewjira,10);
            if (editIconViewjira.isDisplayed()){
                GZLogger.ActivityLog("Able to see the Edit icon on Status Label");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to see the Edit icon on Status Label");
            return false;
        }
        return false;
    }

    public boolean clickEditToChangeStatus(){
        try{
            GZWait(3,editIconViewjira,10);
            if (editIconViewjira.isEnabled()){
                editIconViewjira.click();
                GZLogger.ActivityLog("Clicked on the Edit icon on Status");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Edit to change the Status");
            return false;
        }
        return false;
    }

    public boolean clickJiraStatusDropdwn(){
        try{
            GZWait(3,jiraStatusDropdownIcon,10);
            if (jiraStatusDropdownIcon.isDisplayed()){
                jiraStatusDropdownIcon.click();
                GZLogger.ActivityLog("Clicked on the Jira status dropdown");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Status Dropdown");
            return false;
        }
        return false;
    }

    @FindBy(xpath = "//div[contains(text(),'Status')]/following::div[@data-id='_select']//lightning-base-combobox-item[2]")
    WebElement inProgressJiraStatus;

    public boolean clickInProgressJiraStatus(){
        try{
            GZWait(3,inProgressJiraStatus,10);
            if (inProgressJiraStatus.isDisplayed()){
                inProgressJiraStatus.click();
                GZWait(3,editIconViewjira,15);
                /*Waiting till value gets saved*/
                GZLogger.ActivityLog("Clicked on the In-Progress status");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the In-progress status");
            return false;
        }
        return false;
    }

    public boolean clickShowDetailsOption(){
        try{
            GZWait(0);
            GZWait(2,showDetailsOption,14);
            if (showDetailsOption.isDisplayed()){
                //showDetailsOption.click();
                GZJSEleClick(showDetailsOption);
                //enterKeyPress(showDetailsOption);
                GZLogger.ActivityLog("Clicked on the Show Details option");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Show Details option");
            return false;
        }
        return false;
    }

    @FindBy(xpath = "//div[contains(text(),'Warning')]/following-sibling::span")
    WebElement warningMsgOneOne;

    public boolean verifyOneOneErrMsg(String err){
        try{
            GZWait(3,warningMsgOneOne,10);
            if (warningMsgOneOne.isDisplayed())
                if (err.equalsIgnoreCase(warningMsgOneOne.getText())){
                    String actualErr=warningMsgOneOne.getText();
                    GZLogger.ActivityLog("Found the Warning message:"+" "+actualErr);
                    return true;
                }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to see the Error Message Toast");
            return false;
        }
        return false;
    }

    @FindBy(xpath = "//div[contains(@class,'slds-global-header__item--search')]//input[starts-with(@placeholder,'Search')]")
    WebElement globalSearchBox;

    public boolean enterInGlobalSearch(String serachItem){
        try {
            GZWait(0);
            GZWait(0);
            GZWait(3,globalSearchBox,15);
            if (globalSearchBox.isEnabled() && globalSearchBox.isDisplayed()){
                globalSearchBox.sendKeys(serachItem);
                GZLogger.ActivityLog("Entered the:"+" "+serachItem+" "+"in the Global search");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to enter in Global search");
            return false;
        }
        return false;
    }

    //@FindBy(xpath ="//div[@class=slds-truncate]")
    @FindBy(xpath = "(//li[@role='presentation'])[2]")
    WebElement jiraIssueSearch;

    public boolean clickJiraTask(){
        try{
            GZWait(0);
            /*lines of code that will perform the action (click,sendKey etc)*/
            GZWait(3,jiraIssueSearch,15);
            if(jiraIssueSearch.isDisplayed()){
                jiraIssueSearch.click();
                GZLogger.ActivityLog("Clicked on the Jira Issue Search Result in Global Search List");
                return true;
            }
        }catch (Exception e){
            /*Adding logs for debungging purpose*/
            GZLogger.ErrorLog(e,"Not able to click on the Jira Issue Search Result");
            return false;
        }
        return false;
    }

    @FindBy(xpath = "(//button[@class='slds-button slds-button_neutral slds-button_middle'])[2]")
    WebElement subTaskBtn;

    public boolean clickSubtaskButton(){
        try{
            /*lines of code that will perform the action (click,sendKey etc)*/
            GZWait(3,subTaskBtn,15);
            if(subTaskBtn.isDisplayed()){
                subTaskBtn.click();
                GZWait(0);
                GZWait(0);
                GZLogger.ActivityLog("Clicked on the SubTask Button");
                return true;
            }
        }catch (Exception e){
            /*Adding logs for debungging purpose*/
            GZLogger.ErrorLog(e,"Not able to click on the SubTask Button");
            return false;
        }
        return false;
    }

    @FindBy(xpath = "//h2/b")
    WebElement subtaskFormHeader;

    public boolean checkHeaderOfSubtaskForm(){
        try{
            GZWait(3,subtaskFormHeader,10);
            if (subtaskFormHeader.isDisplayed()){
                String header=subtaskFormHeader.getText();
                if (header.contains("Create Subtask")){
                    GZLogger.ActivityLog("Create Subtask Form is opened");
                    return true;
                }
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to find the Create Subtask form");
            return false;
        }
        return false;
    }

    @FindBy(xpath = "//a[normalize-space()='Jira Issue']")
    WebElement jiraIssueTab;

    public boolean clickJiraTab(){
        try {
            GZWait(3,jiraIssueTab,10);
            if (jiraIssueTab.isDisplayed()){
                jiraIssueTab.click();
                GZLogger.ActivityLog("Clicked on the JIRA issue tab");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the JIRA tab");
            return false;
        }
        return false;
    }

    @FindBy(xpath = "//table//tbody/tr[@data-row-key-value='row-0']/td[@data-label='View In Jira']//a")
    WebElement viewInJiraLink;

    @FindBy(xpath = "//table//tbody/tr[@data-row-key-value='row-0']/th//div/lightning-base-formatted-text")
    WebElement jiraIssueName;

    public boolean clickOnViewInJiraLink(){
        try{
            GZWait(3,viewInJiraLink,10);
            GZWait(0);
            if (viewInJiraLink.isDisplayed()){
                //viewInJiraLink.click();
                GZJSEleClick(viewInJiraLink);
                GZWait(0);
                GZLogger.ActivityLog("Clicked on the View link in Relatedt list");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on View In Jira link");
            return false;
        }
        return false;
    }

    @FindBy(xpath = "//input[@id='username']")
    WebElement jiraUnameBox;

    @FindBy(xpath = "//input[@id='password']")
    WebElement jiraPasswd;

    @FindBy(xpath = "//button[@id='login-submit']")
    WebElement loginSubmitBtn;

    public boolean enterJiraUname(String uname){
        try{
            GZWait(3,jiraUnameBox,10);
            if (jiraUnameBox.isDisplayed()){
                jiraUnameBox.sendKeys(uname);
                GZLogger.ActivityLog("Entered the username in Jira username");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to enter the Jira username");
            return false;
        }
        return false;
    }
    public boolean enterJiraPasswd(String passwd){
        try{
            GZWait(3,jiraPasswd,10);
            if (jiraPasswd.isDisplayed()){
                jiraPasswd.sendKeys(passwd);
                GZLogger.ActivityLog("Entered the password in Jira username");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to enter the Jira password");
            return false;
        }
        return false;
    }

    @FindBy(xpath = "//div[@role='presentation']/button//span[@data-test-id='ak-spotlight-target-profile-spotlight']")
    WebElement profileIconJiraPage;

    public boolean isJiraProfileIconVisible(){
        try {
            GZWait(3,profileIconJiraPage,15);
            if (profileIconJiraPage.isDisplayed())
                return true;
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to see the profile icon on JIRA");
            return false;
        }
        return false;
    }
    public boolean clickLoginSubmitBtnAtJiraPage(){
        try{
            GZWait(3,loginSubmitBtn,10);
            if (loginSubmitBtn.isDisplayed() && loginSubmitBtn.isEnabled()){
                loginSubmitBtn.click();
                GZLogger.ActivityLog("CLicked on the Login Submit button on Jira page");
                return true;
            }
        }catch(Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Login button on Jira Page");
            return false;
        }
        return false;
    }

    @FindBy(xpath = "//nav[@aria-label='Breadcrumbs']/ol/div[2]/li//span")
    WebElement issueKeyLabelOnJira;

    public boolean isJiraKeyDisplayed(String expectedJiraKey){
        try{
            GZWait(3,issueKeyLabelOnJira,12);
            if (issueKeyLabelOnJira.isDisplayed()){
                String actualJiraKey=issueKeyLabelOnJira.getText();
                if (actualJiraKey.contains(expectedJiraKey)){
                    GZLogger.ActivityLog("Found the Jira Key");
                    return true;
                }
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to find the Jira Key");
            return false;
        }
        return false;
    }
    @FindBy(xpath = "//ul[@role='tablist']/li[@title='Related']/a")
    WebElement caseRelatedTab;
    @FindBy(xpath = "//div[@class='actionsContainer']/ul/li//div[@title='New']")
    WebElement newBtnOnCaseComment;
    @FindBy(xpath = "//article/h2")
    WebElement newCaseCommentBox;

    public boolean openCaseCommentBox(){
        try {
            GZWait(3,caseRelatedTab,15);
            if (caseRelatedTab.isDisplayed()){
                caseRelatedTab.click();
                GZLogger.ActivityLog("Clicked on the Related tab");
                GZWait(0);
                if (newBtnOnCaseComment.isDisplayed()){
                    newBtnOnCaseComment.click();
                    GZLogger.ActivityLog("Clicked on the New button");
                    GZWait(3,newCaseCommentBox,15);
                    if (newCaseCommentBox.isDisplayed()){
                        GZLogger.ActivityLog("New Case Comment box appears");
                        return true;
                    }
                }
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to add case comment");
            return false;
        }
        return false;
    }

    @FindBy(xpath = "//div[@class='actionBody']//textarea")
    WebElement bodyTextArea;
    @FindBy(xpath = "//button[@title='Save']")
    WebElement saveCommentBtn;

    public boolean enterCaseComment(String caseComment){
        try {
            GZWait(3,bodyTextArea,10);
            if (bodyTextArea.isDisplayed()){
                bodyTextArea.sendKeys(caseComment);
                GZLogger.ActivityLog("Entered the Case comment:"+" "+caseComment);
                saveCommentBtn.click();
                GZLogger.ActivityLog("Clicked on the Save button");
                GZWait(3,instanceSuccessMessage,15);
                if (instanceSuccessMessage.getText().contains("Case Comment")){
                    GZLogger.ActivityLog("Case comment is saved successfully");
                    return true;
                }
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to save the Case comment");
            return false;
        }
        return false;
    }

    @FindBy(xpath = "//div[@data-test-id='issue.activity.comments-list']/div[1]//p")
    WebElement commentOnJira;

    public boolean isCommentVisibleOnJira(String expectedComment){
        try {
            GZScrollBtm(commentOnJira);
            GZWait(3,commentOnJira,15);
            if (commentOnJira.isDisplayed()){
                String actualComment=commentOnJira.getText();
                if (actualComment.contains(expectedComment)){
                    GZLogger.ActivityLog("Salesforce comment is visible on JIRA side");
                    return true;
                }
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to find the comment on JIRA page");
            return false;
        }
        return false;
    }
    @FindBy(xpath = "//a/span[contains(text(),'Comments')]/following::button[1]")
    WebElement newCmtActionBtn;
    @FindBy(xpath = "//a[@name='New']")
    WebElement newCmntBtn;
    @FindBy(xpath = "//div[@class='slds-modal__header']/h2")
    WebElement newCommentModalHeader;

    public boolean clickNewCommentBtn(){
        try{
            GZWait(3,newCmtActionBtn,20);
            if (newCmtActionBtn.isDisplayed()){
                newCmtActionBtn.click();
                GZWait(0);
                newCmntBtn.click();
                GZLogger.ActivityLog("Clicked on the New Button to add comment");
                GZWait(3,newCommentModalHeader,10);
                if (newCommentModalHeader.isDisplayed()) {
                    GZLogger.ActivityLog("New Comment Modal appears");
                    return true;
                }
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the New button");
            return false;
        }
        return false;
    }

    @FindBy(xpath = "//label[contains(text(),'Comment Body')]/following::textarea")
    WebElement commentTextArea;

    @FindBy(xpath = "//button[@name='SaveEdit']")
    WebElement saveNewCommentBtn;

    public boolean enterNewJiraCommentSF(String newComment){
        try {
            GZWait(3,commentTextArea,10);
            if (commentTextArea.isDisplayed()){
                commentTextArea.sendKeys(newComment);
                GZLogger.ActivityLog("Entered the Comment in the input box");
                GZWait(0);
                saveNewCommentBtn.click();
                GZLogger.ActivityLog("Clicked on the Save button");
                GZWait(3,instanceSuccessMessage,10);
                if (instanceSuccessMessage.isDisplayed())
                    return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to save the new Comment");
            return false;
        }
        return false;
    }

    @FindBy(xpath = "//input[@data-test-id='search-dialog-input'][@placeholder='Search']")
    WebElement searchBoxJiraPage;
    @FindBy(linkText = "BM-127")
    //@FindBy(xpath = "//div[@data-test-id='search-dialog-dialog-wrapper']")
    WebElement searchJiraList;
    public boolean searchJiraIssueAtJiraPage(String jiraKey){
        try{
            GZWait(3,searchBoxJiraPage,10);
            if (searchBoxJiraPage.isDisplayed() && searchBoxJiraPage.isEnabled()){
               // searchBoxJiraPage.click();
                GZLogger.ActivityLog("Cliked in the Search JIRA box");
                GZWait(0);
                searchBoxJiraPage.sendKeys(jiraKey);
                GZLogger.ActivityLog("Enetered the JIRA key in the search box:"+" "+jiraKey);
                GZWait(0);
                searchJiraList.click();
                //enterKeyPress(searchJiraList);
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to search the JIRA key");
            return false;
        }
        return false;
    }
    @FindBy(xpath = "//*[@placeholder='Add a comment…']")
    WebElement addACommentBox;

    @FindBy(xpath = "//div[contains(@class,'akEditor')]/div[2]//div[@role='textbox']")
    WebElement jiraCommentBox;

    @FindBy(xpath = "//button[@data-testid='comment-save-button']")
    WebElement commentSaveBtn;

    @FindBy(xpath = "//*[contains(text(),'This comment is from Jira to SF case,This is an automation comment')]")
    WebElement jiraCommentBody;

    public boolean writeJiraCommentOnJiraIssue(String comment){
        try {
            GZScrollBtm(addACommentBox);
            GZWait(3,addACommentBox,15);
            if (addACommentBox.isDisplayed()){
                addACommentBox.click();
                jiraCommentBox.sendKeys(comment);
                GZLogger.ActivityLog("Entered the JIRA comment:");
                if (commentSaveBtn.isDisplayed()){
                    commentSaveBtn.click();
                    GZLogger.ActivityLog("Clicked on the comment Save button");
                    if (jiraCommentBody.isDisplayed())
                        return true;
                }
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to write JIRA comment");
            return false;
        }
        return false;
    }
    

}
