package GZProducts.Sinergify.SinergifyUtilities.POM;

import CommonUtilities.GZBrowser.GZBrowser;
import CommonUtilities.GZCommonFuncs.GZCommonElementFuncs;
import CommonUtilities.GZLogger.GZLogger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SFDCLoginPage extends GZCommonElementFuncs{

    public SFDCLoginPage(){
        PageFactory.initElements(GZBrowser.getDriver(), this);
    }
   // GZCommonElementFuncs commonElementFuncs=new GZCommonElementFuncs();


    @FindBy(id = "username")
    @CacheLookup
    WebElement txtUserName;

    @FindBy(id = "password")
    @CacheLookup
    WebElement txtpassword;

    @FindBy(id = "Login")
    @CacheLookup
    WebElement btnlogin;

    @FindBy(id = "error")
    @CacheLookup
    WebElement loginerror;

    @FindBy(xpath = "//*[text()=\"Verify Your Identity\"]")
    @CacheLookup
    WebElement verifyIdentityLabel;

    public void verifyIdentity(){
        try {
            Boolean flag = verifyIdentityLabel.isDisplayed();
            if(flag==true) {
                GZLogger.ActivityLog("Need to verify your identity : Add the IP address in Network Access of the org");
            }
        }catch (Exception e){
            GZLogger.ActivityLog("Verify identity is passed");
        }
    }

    public void setUserName(String uname) { txtUserName.sendKeys(uname); }

    public void setpassword(String pwd){txtpassword.sendKeys(pwd);}

    @FindBy(xpath = "//*[@class='slds-global-header__item']/ul/li[9]/span/button")
    WebElement profileIcon;
    @FindBy(xpath = "//div[contains(@class,'appLauncher') and @aria-label='App']//button")
    //@FindBy(xpath = "//*[contains(@class,'salesforceIdentityAppLauncherHeader')]")
    WebElement appLauncherIcon;
    public boolean clickSubmit(){
        btnlogin.click();
        GZWait(3,appLauncherIcon,60);
        return appLauncherIcon.isDisplayed();
    }

    public String getLoginError(){return loginerror.getText();}
}
