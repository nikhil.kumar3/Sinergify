package GZProducts.Sinergify.SinergifyUtilities.POM;

import CommonUtilities.GZBrowser.GZBrowser;
import CommonUtilities.GZCommonFuncs.GZCommonElementFuncs;
import CommonUtilities.GZLogger.GZLogger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ConfigurationSettingsPage extends GZCommonElementFuncs {

    public ConfigurationSettingsPage(){
        PageFactory.initElements(GZBrowser.getDriver(), this);
    }

    @FindBy(xpath = "//span[contains(text(),'Enable Edit Jira in Salesforce')]/following-sibling::label/input")
    WebElement editJiraToggle;
    @FindBy(xpath = "//span[normalize-space()='Configuration Settings']")
    WebElement configurationSettingsOption;

    public boolean clickConfigSettingOption(){
        try{
            GZWait(3,configurationSettingsOption,10);
            if (configurationSettingsOption.isEnabled()){
                configurationSettingsOption.click();
                GZLogger.ActivityLog("Clicked on the Configuration Setting Page");
                GZWait(3,editJiraToggle,10);
                if (editJiraToggle.isDisplayed()){
                    GZLogger.ActivityLog("Navigated to the Configuration Settings Page");
                    return true;
                }
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to navigate to the Configuration Settings page");
            return false;
        }
        return false;
    }

    public boolean checkStatusOfConfigurationSettingOption(WebElement ele){
        try{
            GZWait(3,ele,5);
            if (ele.isDisplayed()){
                String status=ele.getAttribute("value");
                if (status.equalsIgnoreCase("false"))
                    return false;
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,e.getMessage());
            return false;
        }
        return false;
    }

    public boolean enableToggle(WebElement ele){
        try {
            GZWait(3,ele,10);
            if (ele.isDisplayed()){
                if (!checkStatusOfConfigurationSettingOption(ele)){
                    //editJiraToggle.click();
                    GZJSEleClick(ele);
                }
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,e.getMessage());
            return false;
        }
        return false;
    }

    public boolean disableToggle(WebElement ele){
        try {
            GZWait(3,ele,10);
            if (ele.isDisplayed()) {
                if (checkStatusOfConfigurationSettingOption(ele)) {
                    //editJiraToggle.click();
                    GZJSEleClick(ele);
                    return true;
                }
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,e.getMessage());
            return false;
        }
        return false;
    }

    public boolean enableEditJiraToggle(){
        return enableToggle(editJiraToggle);
    }
    public boolean disableEditJiraToggle(){
        return disableToggle(editJiraToggle);
    }

    @FindBy(xpath = "//div[contains(@class,'cofigcontainer')]//button[contains(text(),'Save')]")
    WebElement saveBtn;
    @FindBy(xpath = "//div[@data-key='success']//div//span[contains(text(),'Success')]")
    WebElement successToast;

    public boolean clickOnSaveBtn(){
        try {
            GZScrollBtm(saveBtn);
            if (saveBtn.isEnabled()){
                saveBtn.click();
                GZWait(3,successToast,10);
                if (successToast.isDisplayed()){
                    GZScrollTop();
                    return true;
                }
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to save the Edit Jira toggle value");
            return false;
        }
        return false;
    }

    @FindBy(xpath = "//lightning-button-group/slot/lightning-button/button[contains(text(),'Edit')]")
    WebElement editBtn;

    public boolean isEditButtonVisible(){
        try {
            GZWait(3,editBtn,15);
            if (editBtn.isDisplayed()){
                GZLogger.ActivityLog("Edit button is visible on the page");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Edit button is not visible on the page");
            return false;
        }
        return false;
    }

    @FindBy(xpath = "//span[contains(text(),'Enable Jira Transition')]/following-sibling::label/input")
    WebElement jiraTransitionToggle;

    public boolean enableJiraTransition(){
       return enableToggle(jiraTransitionToggle);
    }

    public boolean disableJiraTransition(){
        return disableToggle(jiraTransitionToggle);
    }

    @FindBy(xpath = "//div[contains(text(),'Status')]/following::div[1]//lightning-primitive-icon/*[name()='svg' and @data-key='edit']")
    WebElement editIconViewJira;

    @FindBy(xpath = "//span[contains(text(),'Jira Details')]")
    WebElement jiraDetailsSpan;

    public boolean isEditIconVisibleTransition(){
        try{
            GZWait(3,editIconViewJira,10);
            if (editIconViewJira.isDisplayed()){
                GZLogger.ActivityLog("Jira Transition is enabled on view jira page");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Jira Transition is enabled on view jira page");
            return false;
        }
        return false;
    }


























}
