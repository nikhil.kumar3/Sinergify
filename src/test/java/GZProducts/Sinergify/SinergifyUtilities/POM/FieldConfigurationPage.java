package GZProducts.Sinergify.SinergifyUtilities.POM;

import CommonUtilities.GZBrowser.GZBrowser;
import CommonUtilities.GZCommonFuncs.GZCommonElementFuncs;
import CommonUtilities.GZCommonFuncs.GZCommonFuncs;
import CommonUtilities.GZLogger.GZLogger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class FieldConfigurationPage extends GZCommonElementFuncs {
    public FieldConfigurationPage() {
        PageFactory.initElements(GZBrowser.getDriver(), this);
    }

    GZCommonElementFuncs elefuncs = null;
    GZBrowser gb = new GZBrowser();
    /* All selectors are listed here*/
    @FindBy(xpath = "//label[text()='Choose a SFDC Object']")
    WebElement chooseSFDCObjectLabel;
    @FindBy(xpath = "//label[text()='Choose a Jira Project']")
    WebElement chooseJiraProjectLabel;
    @FindBy(xpath = "//div[@class='ObjectSelect']//input[@placeholder='Select an Option']")
    WebElement sfdcObjectDropdown;
    @FindBy(xpath = "//label[text()='Choose a Jira Project']/following::lightning-combobox")
    WebElement jiraProjectDropdown;
    @FindBy(xpath = "//div[@id='selectDiv']/select")
    WebElement selectObjDrpDwn;
    @FindBy(xpath = "//label[contains(text(),'Choose a Jira Project')]/following::select[1]")
    WebElement selectJiraProjectDrpDwn;
    @FindBy(id = "editreq_0")
    WebElement editIcon;
    @FindBy(id = "savreq_0")
    WebElement saveIcon;
    @FindBy(xpath = "//select[@accesskey='Reqcasefield_0']")
    WebElement selectObjectField;
    @FindBy(className = "addimg")
    WebElement addMoreField;
    @FindBy(xpath = "//tr[@id='rowop_0']//input[@placeholder='Add Label']")
    WebElement labelBox;
    @FindBy(xpath = "//tr[@class='fornew']/td[2]/span/span/select")
    WebElement selectJiraField;
    @FindBy(xpath = "//tr[@class='fornew']/td[3]/span/select")
    WebElement selectOptionalObjField;
    @FindBy(xpath = "//div[@id='mainSave']/a[@class='btn' and contains(text(),'Save')]")
    WebElement saveBtn;
    @FindBy(css = "#leftPanelDiv #fieldValidationtab")
    WebElement validationTab;

    /*These all are the Action methods below*/

    public void selectObject() {
        elefuncs = new GZCommonElementFuncs();
        elefuncs.selectDdVal(selectObjDrpDwn, "Case");
    }

//    public void selectJiraProject() {
//        elefuncs = new GZCommonElementFuncs();
//        elefuncs.selectDdValByIndex(selectJiraProjectDrpDwn, 1);
//    }


    public void clickEdit() {
        editIcon.click();
    }

    public boolean clickSaveIcon() {
        saveIcon.click();
        return true;
    }

    public void selectObjectField() {
        elefuncs = new GZCommonElementFuncs();
        selectObjectField.click();
        elefuncs.selectDdVal(selectObjectField, "Subject");
    }

    public void clickMore() {
        addMoreField.click();
    }

    public void setLabel(String label) {
        labelBox.sendKeys(label);
    }

    public void selectJiraField() {
        elefuncs = new GZCommonElementFuncs();
        elefuncs.selectDdVal(selectJiraField, "Assignee");
    }

    public void selectObjFieldMore() {
        elefuncs = new GZCommonElementFuncs();
        elefuncs.selectDdVal(selectOptionalObjField, "Created By ID");
    }

    public boolean clickSaveBtn() {
        boolean flag = false;
        try {
            saveBtn.click();
            flag = true;
        } catch (Exception e) {
            GZLogger.ErrorLog(e, e.getMessage());
        }
        return flag;
    }

    public void clickValidationTab() {
        validationTab.click();
    }

    @FindBy(xpath = "//div[text()='Field Configuration -']")
    WebElement fieldConfigHeader;

    public boolean fieldConfigHeaderVisible() {
        try {
            GZWait(3, fieldConfigHeader, 5);
            String header = fieldConfigHeader.getText();
            if (header.contains("Field Configuration")) {
                GZLogger.ActivityLog("Admin user navigates to the Field Configuration - Project Mapping screen");
                return true;
            } else
                return false;
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Unable to navigate to the Field Configuration page");
            return false;
        }
    }

    @FindBy(xpath = "//span[@data-target-id='fieldExpand']")
    WebElement fieldConfigOptionInSideMenu;

    public boolean clickFieldConfigLink() {
        boolean status = false;
        try {
            GZWait(3, fieldConfigOptionInSideMenu, 5);
            if (status = fieldConfigOptionInSideMenu.isEnabled()) {
                fieldConfigOptionInSideMenu.click();
                GZLogger.ActivityLog("Clicked on Field Configuration option in side menu");
                return true;
            }
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Unable to view Field Configuration option in side menu");
            return status;
        }
        return false;
    }

    public boolean findChooseSFDCObjectLabel() {
        try {
            GZWait(3, chooseSFDCObjectLabel, 5);
            boolean status = chooseSFDCObjectLabel.isDisplayed();
            if (status) {
                GZLogger.ActivityLog("Able to see the Choose a SFDC object label");
                return true;
            }
            return false;
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Not able to see the Choose a SFDc object");
            return false;
        }
    }

    public boolean findChooseJiraProjectLabel() {
        try {
            GZWait(3, chooseJiraProjectLabel, 5);
            boolean status = chooseJiraProjectLabel.isDisplayed();
            if (status) {
                GZLogger.ActivityLog("Able to see the Choose a Jira Project label");
                return true;
            }
            return false;
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Not able to see the Choose a Jira Project label");
            return false;
        }
    }

    public boolean findChooseJiraProjectDropdown() {
        try {
            GZWait(3, jiraProjectDropdown, 5);
            boolean status = jiraProjectDropdown.isDisplayed();
            if (status) {
                GZLogger.ActivityLog("Able to see the Choose a Jira Project dropdown");
                return true;
            }
            return false;
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Not able to see the Choose a Jira Project dropdown");
            return false;
        }
    }

    public boolean findChooseSFDCObjectDropdown() {
        try {
            GZWait(3, sfdcObjectDropdown, 5);
            boolean status = sfdcObjectDropdown.isDisplayed();
            if (status) {
                GZLogger.ActivityLog("Able to see the Choose a SFDC object dropdown");
                return true;
            }
            return false;
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Not able to see the Choose a SFDC object dropdown");
            return false;
        }
    }

//    @FindBy(xpath = "//*[@class='slds-grid']/div[1]/div[2]/lightning-combobox")
//    WebElement sfdcObjectDropdown;
//    @FindBy(xpath = "//*[@class='slds-grid']/div[2]/div/div[2]/div/lightning-combobox")
//    WebElement jiraProjectDropdown;

    @FindBy(xpath = "//div[contains(@class,'ProjectMappingContainer slds-col')]/div[4]/div[2]/table/tbody/tr[1]/td[1]/lightning-input/div/input")
    WebElement label1;
    @FindBy(xpath = "//div[contains(@class,'ProjectMappingContainer slds-col')]/div[4]/div[2]/table/tbody/tr[2]/td[1]/lightning-input/div/input")
    WebElement label2;
    @FindBy(xpath = "//div[contains(@class,'ProjectMappingContainer slds-col')]/div[4]/div[2]/table/tbody/tr[3]/td[1]/lightning-input/div/input")
    WebElement label3;
    @FindBy(xpath = "//div[contains(@class,'ProjectMappingContainer slds-col')]/div[4]/div[2]/table/tbody/tr[4]/td[1]/lightning-input/div/input")
    WebElement label4;
    @FindBy(xpath = "//div[contains(@class,'ProjectMappingContainer slds-col')]/div[4]/div[2]/table/tbody/tr[1]/td[7]")
    WebElement editIcon1;
    @FindBy(xpath = "//div[contains(@class,'ProjectMappingContainer slds-col')]/div[4]/div[2]/table/tbody/tr[2]/td[7]")
    WebElement editIcon2;
    @FindBy(xpath = "//div[contains(@class,'ProjectMappingContainer slds-col')]/div[4]/div[2]/table/tbody/tr[3]/td[7]")
    WebElement editIcon3;
    @FindBy(xpath = "//div[contains(@class,'ProjectMappingContainer slds-col')]/div[4]/div[2]/table/tbody/tr[4]/td[7]")
    WebElement editIcon4;
    @FindBy(xpath = "//div[contains(text(),'Save')]")
    WebElement saveButtonProject;
    @FindBy(xpath = "//label[contains(text(),'Choose a Jira Project')]/following::lightning-base-combobox-item/span[2]/span")
    WebElement jiraProjectAtFirstPlace;
    @FindBy(xpath = "//lightning-base-combobox-item/span[2]/span[@title='Case']")
    WebElement caseObject;
    @FindBy(xpath = "//div[@class='clickColor Menuoption-Expanded']/div/div[3]")
    WebElement defaultValueOption;

    public void clickDefaultValueMenu() {
        GZWait(2, defaultValueOption, 6);
        defaultValueOption.click();
    }

    @FindBy(xpath = "//table[@class='defValueTable slds-table_bordered slds-table_fixed-layout borderTable']/tbody/tr[1]/td[3]")
    WebElement defaultEditIcon1;
    @FindBy(xpath = "//table[@class='defValueTable slds-table_bordered slds-table_fixed-layout borderTable']/tbody/tr[2]/td[3]")
    WebElement defaultEditIcon2;
    @FindBy(xpath = "//table[@class='defValueTable slds-table_bordered slds-table_fixed-layout borderTable']/tbody/tr[3]/td[3]")
    WebElement defaultEditIcon3;

    public void clickDefaultValueEditIcons() {
        GZWait(2, defaultEditIcon1, 10);
        defaultEditIcon1.click();
        GZWait(2, defaultEditIcon2, 10);
        defaultEditIcon2.click();
        GZWait(2, defaultEditIcon3, 10);
        defaultEditIcon3.click();
    }

    @FindBy(xpath = "//table[@class='defValueTable slds-table_bordered slds-table_fixed-layout borderTable']/tbody/tr[1]/td[2]/div/div/lightning-input/div/input")
    WebElement defaultValue1;
    @FindBy(xpath = "//lightning-input[@data-target-id='user']/div/input")
    WebElement defaultValue2;
    @FindBy(xpath = "//table[@class='defValueTable slds-table_bordered slds-table_fixed-layout borderTable']/tbody/tr[3]/td[2]/div/div/lightning-input/div/input")
    WebElement defaultValue3;
    @FindBy(xpath = "//div[@role='listbox']/ul/div/li/div")
    WebElement defaultValueSearchResult;
    @FindBy(xpath = "//div[contains(text(),'Save')]")
    WebElement saveButtonDefaultValue;

    public void clickSaveButtonToDefaultValues() {
        GZWait(2, saveButtonDefaultValue, 20);
        saveButtonDefaultValue.click();
    }

    public void setDefaultValues() {
        GZWait(2, defaultValue1, 20);
        defaultValue1.sendKeys("Default 1");
        defaultValue3.sendKeys("Default 2");
        GZWait(2, defaultValue2, 20);
        defaultValue2.click();
        defaultValue2.sendKeys("Suraj");
        GZWait(2, defaultValueSearchResult, 20);
        defaultValueSearchResult.click();
        clickSaveButtonToDefaultValues();
    }

    @FindBy(xpath = "//div[@class='clickColor Menuoption-Expanded']/div/div[4]")
    WebElement reportingMenuOption;

    public boolean clickReportingMenu(){
        try{
            GZWait(2,reportingMenuOption,5);
            if(reportingMenuOption.isDisplayed() && reportingMenuOption.isEnabled()){
                reportingMenuOption.click();
                GZLogger.ActivityLog("Clicked on the Reporting option in side menu");
                return true;
            }
            return false;
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Reporting option in side menu");
            return false;
        }
    }

    @FindBy(xpath = "//div[@class='reportingContainer']/div[4]/div/lightning-icon")
    WebElement reportingEditIcon;

    public boolean clickReportingEditIcon() {
        try{
            GZWait(3, reportingEditIcon, 8);
            if(reportingEditIcon.isDisplayed() && reportingEditIcon.isEnabled()){
                reportingEditIcon.click();
                GZLogger.ActivityLog("Clicked on the Edit icon - Reporting Page");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Edit icon- Reporting Page");
            return false;
        }
        return false;
    }

    @FindBy(xpath = "//div[@class='tableContainer']/div/table/tbody/tr[1]/td[2]/div/lightning-input/div/span/input")
    WebElement reportingCheckbox1;
    @FindBy(xpath = "//div[@class='clickColor Menuoption-Expanded']/div/div[5]")
    WebElement syncConfigurationMenuOption;

    public boolean clickSyncConfigurationMenu(){
        try{
            GZWait(2,syncConfigurationMenuOption,5);
            if(syncConfigurationMenuOption.isDisplayed() && syncConfigurationMenuOption.isEnabled()){
                syncConfigurationMenuOption.click();
                GZLogger.ActivityLog("Clicked on the Sync Configuration option in side menu");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Sync Configuration in side menu");
            return false;
        }
        return false;
    }

    @FindAll(
            @FindBy(xpath = "//div[contains(@class,'ProjectMappingContainer slds-col')]/div[4]/div[2]/table/tbody/tr")
    )
    List<WebElement> allElementsOfProjectTable;

    public boolean clickChooseSfdcObjectDropdown() throws Exception {
        try {
            GZWait(0);
            sfdcObjectDropdown.click();
            GZLogger.ActivityLog("Clicked on the Choose a SFDC object dropdown");
            return true;
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Choose a SFDC object dropdown");
            return false;
        }
    }

    public boolean clickChooseJiraProject() throws Exception {
        try{
            GZWait(0);
            jiraProjectDropdown.click();
            GZLogger.ActivityLog("Clicked on the Choose a Jira project dropdown");
            return true;
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on Choose a Jira project dropdown");
            return  false;
        }

    }

    public boolean selectJiraProject() {
        try {
            if (clickChooseJiraProject()){
                WebDriver driver = GZBrowser.getDriver();
                JavascriptExecutor executor = (JavascriptExecutor) driver;
                executor.executeScript("arguments[0].click();", jiraProjectAtFirstPlace);
                GZLogger.ActivityLog("Selected a Jira project from the picklist");
                return true;
            }else
                return false;
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to select a Jira project from the picklist");
            return false;
        }
    }

    public boolean selectCaseObject() {
        try{
            if (clickChooseSfdcObjectDropdown()){
                WebDriver driver = GZBrowser.getDriver();
                JavascriptExecutor executor = (JavascriptExecutor) driver;
                executor.executeScript("arguments[0].click();", caseObject);
                GZLogger.ActivityLog("Selected an SFDC object from the picklist");
                return true;
            }else
                return false;
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to select an SFDC object from the picklist");
            return false;
        }

    }

    public void clickEditIcon() {
        GZWait(2, editIcon1, 10);
        editIcon1.click();
        GZWait(2, editIcon2, 10);
        editIcon2.click();
        GZWait(2, editIcon3, 10);
        editIcon3.click();
        GZWait(2, editIcon4, 10);
        editIcon4.click();
    }

    public void setLabel() {
        GZWait(2, label1, 10);
        label1.sendKeys("Label 1");
        GZWait(2, label2, 10);
        label2.sendKeys("Label 2");
        GZWait(2, label3, 10);
        label3.sendKeys("Label 3");
        GZWait(2, label4, 10);
        label4.sendKeys("Label 4");
    }

    @FindBy(xpath = "//button[contains(text(),'Sync')]")
    WebElement syncButton;

    public boolean clickSyncButton() {
        try{
            GZWait(2, syncButton, 10);
            GZJSEleClick(syncButton);
            GZWait(3,instanceSuccessMessage,10);
            String smsg= instanceSuccessMessage.getText();
            if(smsg.contains("Success")){
                GZLogger.ActivityLog("Clicked on the Sync button");
                return true;
            }
            return false;
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on Sync button on Sync page");
            return false;
        }
    }



    public boolean clickSaveProjectMapping() {
        try{
            GZWait(3, saveButtonProject, 10);
            saveButtonProject.click();
            GZWait(3,instanceSuccessMessage,10);
            String smsg= instanceSuccessMessage.getText();
            if(smsg.contains("Success")){
                GZLogger.ActivityLog("Clicked on the Save button");
                return true;
            }
        return false;
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on Save button on Field mapping page");
            return false;
        }

    }

    public void sizeOfList() {
        int size = allElementsOfProjectTable.size();
        System.out.println(size);
    }

    @FindBy(xpath = "//*[@class='slds-global-header__item']/ul/li[9]/span/button")
    WebElement profileIcon;
    @FindBy(xpath = "//div[@class='profile-card-indent']/div/a[2][contains(text(),'Log Out')]")
    WebElement logout;

    public void logout() {
        GZWait(2, profileIcon, 30);
        profileIcon.click();
        GZWait(2, logout, 30);
        logout.click();
    }


//    @FindBy(xpath = "//lightning-base-combobox-item/span[2]/span")
//    List<WebElement> objects;
//    public boolean clickAndSelectSFDCObject(){
//        boolean status=false;
//        try{
//
//
//        }catch (Exception e){
//
//        }
//
//    }


    @FindBy(xpath = "//div[@class='ProjectMappingContainer slds-col']//table//tbody/tr")
    List<WebElement> fieldMappingList;

    @FindBy(xpath = "//label[text()='Required Jira fields for Mapping With']")
    WebElement projectHeaderLabel;
    @FindBy(xpath = "//lightning-spinner[not(contains(@style,'display:none'))]")
    WebElement loaderIcon;

    @FindBy(xpath = "//lightning-spinner/div[@role='status']/span[contains(@class,'slds-assistive')]")
    WebElement loaderIconReporting;



    public boolean clickEditIcons() {
        try {
            GZWait(3,projectHeaderLabel,5);
            for (int i = 1; i <= fieldMappingList.size(); i++) {
                GZBrowser.getDriver().findElement(By.xpath("//div[@class='ProjectMappingContainer slds-col']//table//tbody/tr[" + i + "]/td[7]")).click();
                GZWait(4,loaderIcon,20);
            }
            GZLogger.ActivityLog("Clicked on the Edit icons in each row of the Field mapping table");
            return true;
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on Edit icons in each row of the Field mapping table");
            return false;
        }
    }


    @FindBy(xpath = "//table/tbody/tr/td[2]/lightning-combobox/div//input")
    List<WebElement> jiraFieldsList;

    public boolean getSetJiraFieldNameFromTable(){
        try{
            GZWait(3,fieldConfigHeader,7);
            if (fieldConfigHeader.isDisplayed()) {
                for (int i = 1; i <= jiraFieldsList.size(); i++) {
                    WebElement jiraFieldName = jiraFieldsList.get(i - 1); /*Fetching the web element from Jira Field column one by one*/
                    String text = getHiddenElementValue(jiraFieldName);   /*getText*/
                    GZBrowser.getDriver().findElement(By.xpath("//table/tbody/tr[" + i + "]/td[1]/lightning-input/div/input")).sendKeys(text);
                }
                GZLogger.ActivityLog("Filled in the Jira field value's in Label Field input");
                return true;
            }
            return false;
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to fill in the Jira field value's in Label Field input");
            return false;
        }
    }
    @FindBy(xpath = "//div[@data-key='success']")
    WebElement instanceSuccessMessage;

    public boolean waitAndVerifyForSuccessMsg(){
        try{
            GZWait(3,instanceSuccessMessage,20);
            if (instanceSuccessMessage.getText().contains("Success")){
                GZWait(4,instanceSuccessMessage,20);
                GZLogger.ActivityLog("Success message is displayed");
                return true;
            }
            return false;
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to see the Success message");
            return false;
        }
    }

    @FindBy(xpath = "//div[@class='ProjectMappingContainer slds-col']//table//tbody/tr[1]/td[3]//div[@role='listbox']/lightning-base-combobox-item")
    List<WebElement> caseFieldValueList;

    @FindBy(xpath = "//table/tbody/tr/td[3]/lightning-combobox/div//input")
    List<WebElement> caseFieldList;



    public boolean setCaseFieldValueInTable(){
        try{
            GZWait(3,fieldConfigHeader,7);
            if (fieldConfigHeader.isDisplayed()) {
                for (int i = 1; i <= caseFieldList.size(); i++) {
                    WebElement caseFieldInput = caseFieldList.get(i - 1);
                    caseFieldInput.click();
                    int caseFieldSize=GZBrowser.getDriver().findElements(By.xpath("//div[@class='ProjectMappingContainer slds-col']//table//tbody/tr[" + i+ "]/td[3]//div[@role='listbox']/lightning-base-combobox-item")).size();
                    if(caseFieldSize>1){
                        WebElement caseFieldVal=GZBrowser.getDriver().findElement(By.xpath("//div[@class='ProjectMappingContainer slds-col']//table//tbody/tr[" + i+ "]/td[3]//div[@role='listbox']/lightning-base-combobox-item//span[@title='Contact Email']"));
                        GZScrollBtm(caseFieldVal);
                        caseFieldVal.click();
                    }
                    else
                        caseFieldInput.click();
                }
                GZLogger.ActivityLog("Filled in the Case field value's in Case Field input");
                return true;
            }
            return false;
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to fill in the Case field value's in Case Field input");
            return false;
        }
    }


    @FindBy(xpath = "//div[@class='ProjectMappingContainer slds-col']//table//tbody/tr[1]/td[7]/lightning-icon[2]")
    WebElement gearIcon;

    public boolean isGearIconVisibleOnProjectFieldMapping(){
        try {
            GZWait(3, defaultValueOption, 5);
            if (gearIcon.isDisplayed()) {
                if (defaultValueOption.isDisplayed()) {
                    defaultValueOption.click();
                    GZWait(5000);
                    GZLogger.ActivityLog("The gear icons are visible on the Project-Field mapping page");
                }else
                    return false;
            }else
                return false;
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Project-Field Mapping seems not done");
            return false;
        }
        return true;
    }

    @FindBy(xpath = "//table[contains(@class,'defValueTable')]/tbody/tr")
    List<WebElement> defaultValueTableRowList;
    @FindBy(xpath = "//div[@class='mainHeading']")
    WebElement defaultValueHeader;

    public boolean clickEditIconsDefaultValue() {
        try {
            GZScrollTop(defaultValueHeader);
            GZWait(3,defaultValueHeader,5);
            for (int i = 1; i <= defaultValueTableRowList.size(); i++) {
                GZBrowser.getDriver().findElement(By.xpath("//table[contains(@class,'defValueTable')]/tbody/tr[" +i +"]/td[3]")).click();
            }
//            GZLogger.ActivityLog("Clicked on the Edit icons in each row of the Field mapping table");
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on Edit icons in each row of the Field mapping table");
            return false;
        }
        return true;
    }

    @FindBy(xpath = "//table/tbody/tr/td[1]")
    List<WebElement> listOfLabelsInDefaultValueTable;

    public boolean setDefaultValue(){
        try{
            GZScrollTop(defaultValueHeader);
            GZWait(3,defaultValueHeader,7);
            if (defaultValueHeader.isDisplayed()) {
                for (int i = 1; i <= listOfLabelsInDefaultValueTable.size(); i++)
                {
                    WebElement jiraFieldName = listOfLabelsInDefaultValueTable.get(i - 1); /*Fetching the web element from Jira Field column one by one*/
                    String text = jiraFieldName.getText();  /*getText*/
                    System.out.println(text +"and value of i is:" +" "+ +i);
                    if (text.equalsIgnoreCase("Summary"))
                    {
                        GZBrowser.getDriver().findElement(By.xpath("//table/tbody/tr[" + i + "]/td[2]//input")).sendKeys(text.toUpperCase());
                        if(clickSaveProjectMapping())
                            return true;
                    }
                }
            }else
                    return false;
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to save value in Default value table");
            return false;
        }
        return true;
    }

    @FindBy(xpath = "//div[@class='reportingContainer']/div[@class='mainHeading']")
    WebElement reportingPageHeader;
    @FindBy(xpath = "//label[contains(text(),'Choose a SFDC Object')]")
    WebElement reportingPageSFDCLabel;
    @FindBy(xpath = "//label[contains(text(),'Choose a Jira Project')]")
    WebElement reportingPageJiraLabel;

    public boolean reportingPageHeaderVisible() {
        try {
            GZWait(3, reportingMenuOption, 5);
            GZWait(3,reportingPageHeader,5);
            String header = reportingPageHeader.getText();
            if (header.contains("Reporting")) {
                GZLogger.ActivityLog("Admin user navigates to the Field Configuration - Reporting screen");
                return true;
            } else
                return false;
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Unable to navigate to the Field Configuration page");
            return false;
        }
    }

    public boolean reportingPageLabelsVisible(){
        try {
            GZWait(3,reportingPageSFDCLabel,5);
            if(reportingPageSFDCLabel.isDisplayed() && reportingPageJiraLabel.isDisplayed()){
                GZLogger.ActivityLog("Both Choose a SFDC Object & Choose a Jira Project label are visible - Reporting screen");
                return true;
                }
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Unable to navigate to the Field Configuration page");
            return false;
        }
        return false;
    }

//    @FindBy(xpath = "//div[@class='editContainer']/lightning-icon")
//    WebElement reportingEditIcon;
    @FindBy(xpath = "//table[contains(@class,'reportingtable')]/thead/tr/th[2]//input[@type='checkbox'][@name='Reporting']")
    WebElement reportingEditCheckbox;
    @FindBy(xpath = "//table[contains(@class,'reportingtable')]/thead/tr/th[3]//input[@type='checkbox'][@name='Sync']")
    WebElement reportingSyncCheckbox;
    @FindBy(xpath = "//button[contains(text(),'OK')]")
    WebElement okButton;

    public boolean clickReportingSyncCheckbox(){
        try{

            GZWait(3,reportingEditCheckbox,5);
            if(reportingEditCheckbox.isDisplayed() && reportingSyncCheckbox.isEnabled()){
                GZJSEleClick(reportingEditCheckbox);
                GZLogger.ActivityLog("Clicked on the Reporting Edit Checkbox");
                GZJSEleClick(reportingSyncCheckbox);
                GZLogger.ActivityLog("Clicked on the Reporting Sync Checkbox");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on Reporting & Sync checkboxes-  Reporting Page");
            return false;
        }
        return false;
    }

    public boolean clickOKButtonReportingPage(){
        try{
            GZWait(3,okButton,10);
            if(okButton.isDisplayed()){
                okButton.click();
                GZWait(3,instanceSuccessMessage,10);
                if(instanceSuccessMessage.getText().contains("Success")){
                    GZLogger.ActivityLog("Clicked on the OK button -Reporting Modal Popup");
                    return true;
                }
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the OK button - Reporting Modal Popup");
            return false;
        }
        return false;
    }
    @FindBy(xpath = "//label[contains(@class,'slds-checkbox_toggle slds-grid')]")
    WebElement syncToggle;

    public boolean clickSyncConfigurationToggleIcon(){
        try{
            GZWait(3, syncToggle, 8);
            if(syncToggle.isDisplayed()){
//                syncToggle.click();
                GZLogger.ActivityLog("Clicked on the Sync Daily Basis icon - Sync Configuration Page");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to Click on the Sync Daily Basis icon - Sync Configuration Page");
            return false;
        }
        return false;
    }

    @FindBy(xpath = "//div[contains(text(),'Field Configuration - Sync Configuration')]")
    WebElement syncConfigurationHeader;

    @FindBy(xpath = "//span[contains(text(),'Sync on a daily basis')]")
    WebElement syncLabel;

    @FindBy(xpath = "//span[contains(@class,'slds-checkbox_faux')]/span[contains(text(),'Disabled')]")
    WebElement syncToggleDisabled;

    @FindBy(xpath = "//span[contains(@class,'slds-checkbox_faux')]/span[contains(text(),'Enabled')]")
    WebElement syncToggleEnabled;

    @FindBy(xpath = "//input[@placeholder='HH:MM AM/PM']")
    WebElement selectTimeInput;

    public boolean clickSelectSyncTime(){
        try{
            GZWait(2,selectTimeInput,5);
            if(selectTimeInput.isDisplayed()){
                GZJSEleClick(selectTimeInput);
                selectValueInPicklist(selectTimeInput);
                GZLogger.ActivityLog("Clicked and Selected time in the Sync on a daily basis toggle-disabled");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Sync on a daily basis toggle-disabled");
            return false;
        }
        return false;
    }



    public boolean clickSyncToggleDisable(){
        try{
            GZWait(2,syncToggleDisabled,5);
            if(syncToggleDisabled.isDisplayed()){
                GZJSEleClick(syncToggleDisabled);
                GZLogger.ActivityLog("Clicked on the Sync on a daily basis toggle-disabled");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Sync on a daily basis toggle-disabled");
            return false;
        }
        return false;
    }

    public boolean clickSyncToggleEnabled(){
        try{
            GZWait(3,syncToggleEnabled,5);
            if(syncToggleEnabled.isDisplayed()){
                syncToggleEnabled.click();
                GZLogger.ActivityLog("Clicked on the Sync on a daily basis toggle-Enabled");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Sync on a daily basis toggle-Enabled");
            return false;
        }
        return false;
    }

    public boolean syncConfigurationPageHeaderVisible() {
        try {
            GZWait(3, syncConfigurationHeader, 5);
            String header = syncConfigurationHeader.getText();
            if (header.contains("Sync Configuration")){
                GZLogger.ActivityLog("Admin user navigates to the Sync Configuration - Field Config screen");
                if (syncLabel.getText().contains("Sync")){
                    GZLogger.ActivityLog("See the Sync on a daily basis label");
                    return true;
                }
            }
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Unable to navigate to the Sync Configuration - Field Config screen");
            return false;
        }
        return false;
    }










}

//    @FindBy(xpath = "//div[@class='ProjectMappingContainer slds-col']//table//tbody/tr["+i+"]/td[6]/lightning-icon[@title='Edit']")
//    WebElement editIconInTable;



