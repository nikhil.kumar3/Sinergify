package GZProducts.Sinergify.SinergifyUtilities.POM;

import CommonUtilities.GZBrowser.GZBrowser;
import CommonUtilities.GZCommonFuncs.GZCommonElementFuncs;
import CommonUtilities.GZLogger.GZLogger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class ValidationPage extends GZCommonElementFuncs{
    public ValidationPage (){
        PageFactory.initElements(GZBrowser.getDriver(), this);
    }
    GZCommonElementFuncs eleFunc= null;

    public String descText="This is a text description";

    @FindBy(xpath = "//div[@id='fieldValidation']//select[@class='picklistsize1']")
    WebElement selectObj;
    @FindBy(id = "toselect_0")
    WebElement availField;
    @FindBy(id = "fromselect_1")
    WebElement reqField;
    @FindBy(xpath = "//div[@class='add']")
    WebElement add;
    @FindBy(xpath = "//div[@class='remove']")
    WebElement remove;
    @FindBy(xpath = "//div[@id='fieldValidation']//a[contains(text(),'Save')]")
    WebElement saveBtn;
    @FindBy(xpath = "//div[@id='fieldValidation']//a[contains(text(),'Cancel')]")
    WebElement cancelBtn;
    @FindBy(css = "#leftPanelDiv #RuleSettab")
    WebElement ruleSetTab;

    /*ALL VALIDATION PAGE ELEMENTS AND PAGE METHODS SEPTEMBER 21*/

    @FindBy(xpath = "//span[normalize-space()='Validation']")
    WebElement validationOptionInMenu;

    public boolean isValidationOptionVisibleInSideMenu(){
        try{
            GZWait(3,validationOptionInMenu,3);
            if(validationOptionInMenu.isDisplayed()){
                GZLogger.ActivityLog("See the Validation in side menu");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to see Validation option in side menu");
            return false;
        }
        return false;
    }
    public boolean clickValidationOptionInSideMenu(){
        try{
            GZWait(3,validationOptionInMenu,3);
            if(validationOptionInMenu.isDisplayed()){
                GZJSEleClick(validationOptionInMenu);
                GZLogger.ActivityLog("Clicked on the Validation in side menu");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on Validation option in side menu");
            return false;
        }
        return false;
    }

    @FindBy(xpath = "//img[@class='norulesimg']")
    WebElement noValidationImg;

    @FindBy(xpath = "//button[contains(text(),'Add New Validation')]")
    WebElement addValidationBtn;

    public boolean clickAddValidationButton(){
        try{
            GZWait(3,addValidationBtn,3);
            if(addValidationBtn.isDisplayed()){
                addValidationBtn.click();
                GZLogger.ActivityLog("Click on the Add New Validation button");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Add New Validation button");
            return false;
        }
        return false;
    }

    @FindBy(xpath = "//div[contains(@class,'instancechange ')]")
    WebElement instanceChangeIcon;

    public boolean isAddValidationButtonVisible(){
        try{
            GZWait(3,addValidationBtn,5);
            if(addValidationBtn.isDisplayed() || noValidationImg.isDisplayed() || instanceChangeIcon.isDisplayed() ){
                GZLogger.ActivityLog("Able to see the Add new Validation button");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to see the Add Validation button ");
            return false;
        }
        return false;
    }

    public boolean clickAddNewValidationButton(){
        try{
            GZWait(3,addValidationBtn,3);
            if(addValidationBtn.isDisplayed()){
                GZJSEleClick(addValidationBtn);
                GZWait(0);
                GZLogger.ActivityLog("Clicked on the Add new Validation button");
                return true;
                }
            }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Add Validation button ");
            return false;
        }
        return false;
    }


    /*All action methods are below*/

    public void selectObjDrpDwn(){
        eleFunc=new GZCommonElementFuncs();
        selectObj.click();
        eleFunc.selectDdValByIndex(selectObj,1);
    }
    public void addAvailField(){
        availField.click();
        add.click();
    }
    public void removeReqField(){
        reqField.click();
        remove.click();
    }
    public boolean clickSaveBtn(){
        boolean flag=false;
        try {
            saveBtn.click();
            flag = true;
        }catch (Exception e){
            GZLogger.ErrorLog(e,e.getMessage());
        }
        return flag;
    }
    public boolean clickRulesetTab() {
        boolean flag = false;
        try {
            ruleSetTab.click();
            flag = true;
        } catch (Exception e) {
            GZLogger.ErrorLog(e,e.getMessage());
        }
        return flag;
    }
    public void clickCancelBtn(){cancelBtn.click();}





    @FindBy(xpath = "//span[contains(@class,'slds-checkbox_faux')]/span[contains(text(),'Disabled')]")
    WebElement activeToggleDisabled;

    @FindBy(xpath = "//span[contains(@class,'slds-checkbox_faux')]/span[contains(text(),'Enabled')]")
    WebElement activeToggleEnabled;

    public boolean clickActiveToggleDisable(){
        try{
            GZWait(2,activeToggleDisabled,5);
            if(activeToggleDisabled.isDisplayed()){
                GZJSEleClick(activeToggleDisabled);
                GZLogger.ActivityLog("Clicked on the Active toggle-disabled");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to Click on the Active toggle-disabled\"");
            return false;
        }
        return false;
    }

    public boolean clickActiveToggleEnabled(){
        try{
            GZWait(3,activeToggleEnabled,5);
            if(activeToggleEnabled.isDisplayed()){
                GZJSEleClick(activeToggleEnabled);
                GZLogger.ActivityLog("Clicked on the Active toggle-Enabled");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click Active toggle-Enabled");
            return false;
        }
        return false;
    }

    @FindBy(xpath = "//div[contains(@id,'modal-content-id')]//textarea[@name='input2']")
    WebElement descriptionBox;

    @FindBy(xpath = "//div[@class='addscrol']//input[@placeholder='Select an Option']")
    WebElement projectDropdown;

    @FindBy(xpath = "//c-addrule//lightning-base-combobox//input[@role='combobox'][@placeholder='Select an Option']")
    WebElement objectDropdown;

    @FindBy(xpath = "//label[contains(text(),'Filter Logic')]/following::input[1]")
    WebElement filterLoginInput;

    @FindBy(xpath = "//label[contains(text(),'Filter Logic')]/following::input[2]")
    WebElement errMsgInput;

    @FindBy(xpath = "//button[contains(@class,'savebutton')]")
    WebElement saveBtnValidation;

    @FindBy(xpath = "//button[@name='Close']")
    WebElement closeBtnValidation;


    @FindBy(xpath = "//ul[@role='presentation']//span[contains(text(),'All')]")
    WebElement allProject;

    @FindBy(xpath = "//div[@data-key='success']")
    WebElement instanceSuccessMessage;


    public boolean clickSaveBtnValidation(){
        try{
            GZWait(3,saveBtnValidation,5);
            if(saveBtnValidation.isDisplayed() || saveBtnValidation.isEnabled()){
                GZJSEleClick(saveBtnValidation);
                GZLogger.ActivityLog("Clicked on the Save button - Validation Page");
                GZWait(3,instanceSuccessMessage,9);
                if(instanceSuccessMessage.isDisplayed())
                    if(instanceSuccessMessage.getText().contains("Success"))
                        GZLogger.ActivityLog("Saved the new validation rule");
                    if(findSnInValidationTable())
                        return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Save button- Validation Page");
            return false;
        }
        return false;
    }

    public boolean enterFilterLogic(){
        try{
            GZWait(3,filterLoginInput,5);
            if(filterLoginInput.isDisplayed()){
                filterLoginInput.sendKeys("1");
                GZLogger.ActivityLog("Entered value in the Filter logic input");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e," Not able to add value in Filter login input box");
            return false;
        }
        return false;
    }

    public boolean enterErrMsg(){
        try{
            GZWait(3,errMsgInput,5);
            if(errMsgInput.isDisplayed()){
                GZJSEleClick(errMsgInput);
                errMsgInput.sendKeys("Please fix the errors!");
                GZLogger.ActivityLog("Entered value in the Error Message input");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e," Not able to add value in Error Message input box");
            return false;
        }
        return false;
    }


    public boolean clickSelectProject(){
        try{
            GZWait(2,projectDropdown,5);
            if(projectDropdown.isDisplayed()){
                GZJSEleClick(projectDropdown);
                GZWait(3,allProject,5);
                GZLogger.ActivityLog("Clicked on the Project dropdown - Validation Page");
                GZJSEleClick(allProject);
                GZLogger.ActivityLog("Selected the value 'All' from the Project dropdown - Validation Page");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click & Select 'All' the Project- Validation Page");
            return false;
        }
        return false;
    }

    public boolean clickSelectObject(){
        try{
            GZWait(3,objectDropdown,5);
            if(objectDropdown.isDisplayed()){
                GZJSEleClick(objectDropdown);
                GZWait(3,objectDropdown,5);
                GZLogger.ActivityLog("Clicked on the Project dropdown - Validation Page");
                enterKeyPress(objectDropdown);
                GZLogger.ActivityLog("Selected the value from the Project dropdown - Validation Page");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click & Select the Project- Validation Page");
            return false;
        }
        return false;
    }

    @FindBy(xpath = "//table/tbody/tr[1]/td[2]//input[@name='field']")
    WebElement fieldNameDrpDwn;

    @FindBy(xpath = "//table/tbody/tr[1]/td[3]//input[@name='1']")
    WebElement operatorDrpDwn;

    @FindBy(xpath = "//table/tbody/tr[1]/td[2]//lightning-base-combobox-item//span[contains(text(),'Description')]")
    WebElement fieldNameVal;

    @FindBy(xpath = "//table/tbody/tr[1]/td[3]//lightning-base-combobox-item//span[contains(text(),'IsBlank')]")
    WebElement operatorVal;

    @FindBy(xpath = "//table/tbody/tr[1]/td[4]//input[@value='checkbox']")
    WebElement valueCheckbox;

    public boolean clickSelectFieldName(){
        try{
            GZWait(3,fieldNameDrpDwn,3);
            if(fieldNameDrpDwn.isDisplayed()){
                GZJSEleClick(fieldNameDrpDwn);
                GZWait(3,fieldNameVal,5);
                GZScrollBtm(fieldNameVal);
                GZJSEleClick(fieldNameVal);
                GZLogger.ActivityLog("Clicked and Selected the Field name value");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click & select a Field name value");
            return false;
        }
        return false;
    }

    public boolean clickSelectOperator(){
        try{
            GZWait(3,operatorDrpDwn,3);
            if(operatorDrpDwn.isDisplayed()){
                GZWait(0);
                GZJSEleClick(operatorDrpDwn);
                GZScrollBtm(operatorVal);
                GZJSEleClick(operatorVal);
                GZLogger.ActivityLog("Clicked and Selected the Operator value");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click & select a Operator value");
            return false;
        }
        return false;
    }


    public boolean clickValueCheckBox(){
        try{
            GZWait(3,valueCheckbox,3);
            if(valueCheckbox.isDisplayed()){
                GZJSEleClick(valueCheckbox);
                GZLogger.ActivityLog("Clicked on the Value checkbox - Validation page");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the value check box - Validation page");
            return false;
        }
        return false;
    }

    public boolean enterDescription(){
        try{
            GZWait(3,descriptionBox,3);
            if(descriptionBox.isDisplayed()){
                descriptionBox.sendKeys(descText);
                GZLogger.ActivityLog("Entered the description - Validation page");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to the description - Validation page");
            return false;
        }
        return false;
    }

    @FindBy(xpath = "//div[@class='mainLabel']//table")
    WebElement valTable;

    @FindBy(xpath = "//div[@class='mainLabel']//table/tbody/tr/td[5]/div")
    List<WebElement> descList;

    public boolean findSnInValidationTable(){
        try{
            GZWait(3,valTable,5);
            if(valTable.isDisplayed()){
                GZLogger.ActivityLog("Able to see the table of recently added validation rule");
                for(int i=0;i<descList.size();i++){
                    WebElement desc=descList.get(i);
                    if(desc.getText().contains(descText)){
                        GZLogger.ActivityLog("Able to see the recently added validation rule");
                        return true;
                    }
                }
            }
            GZLogger.FailLog("Not able to find the recently added Validation rule");
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to see the table of added validation rules");
            return false;
        }
        return false;
    }


    @FindBy(xpath = "//div[@class='mainLabel']//table/tbody/tr[1]/td[6]//span[contains(@title,'Edit rule')]/img")
    WebElement editRuleIcon;

    public boolean clickEditRuleBtn(){
        try{
            GZWait(3,editRuleIcon,5);
            if(editRuleIcon.isDisplayed()){
                GZJSEleClick(editRuleIcon);
                GZLogger.ActivityLog("Clicked on the Edit Rule icon");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Edit Rule button");
            return false;
        }
        return false;
    }

    public boolean editDescriptionInValidationRule() {
        try {
            GZWait(3, descriptionBox, 5);
            if (descriptionBox.isDisplayed()) {
                descriptionBox.clear();
                GZWait(0);
                descriptionBox.sendKeys(descText + "" + "edited");
                if(saveBtnValidation.isDisplayed()) {
                    GZScrollBtm(saveBtnValidation);
                    GZJSEleClick(saveBtnValidation);
                    GZWait(3, instanceSuccessMessage, 8);
                    if (instanceSuccessMessage.isDisplayed()) {
                        GZLogger.ActivityLog("Edited the description of the Validation rule");
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "Not able to update the Validation rule");
            return false;
        }
        return false;
    }


    @FindBy(xpath = "//div[@class='mainLabel']//table/tbody/tr[1]/td[6]//span[contains(@title,'Delete')]/img")
    WebElement deleteRuleIcon;

    @FindBy(xpath = "//div[contains(@id,'modal-content-id')]/p[contains(text(),'Do you want to delete Validation.')]")
    WebElement delValRuleModal;

    @FindBy(xpath = "//div[contains(@id,'modal-content-id')]//button[contains(text(),'OK')]")
    WebElement okBtnDelValModal;

    public boolean clickDeleteRuleBtn(){
        try{
            GZWait(3,deleteRuleIcon,5);
            if(deleteRuleIcon.isDisplayed()){
                GZJSEleClick(deleteRuleIcon);
                GZLogger.ActivityLog("Clicked on the Delete Rule icon");
                return true;
            }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to click on the Delete Rule button");
            return false;
        }
        return false;
    }

    public boolean clickOkBtnValidationRule() {
        try {
            GZWait(3, delValRuleModal, 5);
            if (delValRuleModal.isDisplayed() & delValRuleModal.getText().contains("delete Validation")) {
                GZJSEleClick(okBtnDelValModal);
                GZWait(0);
                GZWait(3,addValidationBtn,5);
                if(addValidationBtn.isDisplayed()) {
                        GZLogger.ActivityLog("Deleted the Validation rule");
                        return true;
                    }

                }
            }catch (Exception e) {
            GZLogger.ErrorLog(e, "Not able to delete the Validation rule");
            return false;
        }
        return false;
    }






} /*End of the page class*/
