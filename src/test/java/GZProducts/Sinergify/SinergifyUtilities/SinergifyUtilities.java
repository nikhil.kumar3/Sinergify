package GZProducts.Sinergify.SinergifyUtilities;

import CommonUtilities.DataProvider.ReadConfig;
import CommonUtilities.GZBrowser.BrowserType;
import CommonUtilities.GZBrowser.GZBrowser;
import CommonUtilities.GZLogger.GZLogger;
import GZProducts.Sinergify.SinergifyUtilities.POM.AdminSettingPage;
import GZProducts.Sinergify.SinergifyUtilities.POM.CreateJiraPage;
import GZProducts.Sinergify.SinergifyUtilities.POM.SearchJiraPage;
import GZProducts.Sinergify.SinergifyUtilities.POMUtilities.*;

import java.util.Map;
import java.util.Set;

public class SinergifyUtilities {

    /**
     * Utilities
     */
    GZBrowser gb = new GZBrowser();
    ReadConfig config = new ReadConfig();
    SFDCLoginPageUtility lputility = new SFDCLoginPageUtility();
    AdminSettingsPageUtility asputility = new AdminSettingsPageUtility();
    ProjectsPageUtility projutill = new ProjectsPageUtility();
    FieldConfigPageUtility fieldutill = new FieldConfigPageUtility();
    ValidationPageUtility valutil=new ValidationPageUtility();
    RulesetPageUtility ruleutil=new RulesetPageUtility();
    AdminSettingPage asp= new AdminSettingPage();
    CreateJiraPage cjp=new CreateJiraPage();
    CreateJiraPageUtility cjpUtil=new CreateJiraPageUtility();
    SearchJiraPageUtility sjpUtil=new SearchJiraPageUtility();
    //SearchJiraPage sjp = new SearchJiraPage();
    ConfigurationSettingsPageUtility cspUtil = new ConfigurationSettingsPageUtility();
    PluginAuthenticationPageUtility papUtil = new PluginAuthenticationPageUtility();


    /**
     * Common Methods
     */
    public void LaunchUrl(String browser) {
        try {
            String baseURl = config.getApplicationPath();
            gb.Launch(baseURl, BrowserType.valueOf(browser));
            GZLogger.ActivityLog("Browser Launched");
        } catch (Exception ex) {
            GZLogger.ErrorLog(ex, "Not able to launch browser");
        }
    }
    public void LaunchJiraUrl(String browser,String url) {
        try {
            //String baseURl = config.getJiraApplicationPath();
            gb.Launch(url, BrowserType.valueOf(browser));
            GZLogger.ActivityLog("Browser Launched");
        } catch (Exception ex) {
            GZLogger.ErrorLog(ex, "Not able to launch browser");
        }
    }

    public void CloseBrowser() {
        try {
            gb.Close();
            GZLogger.ActivityLog("Browser closed successfully");
        } catch (Exception ex) {
            GZLogger.ErrorLog(ex, "Not able to dispose browser");
        }
    }

    public boolean LogInIntoSinergify() {
        boolean successLogin=false;
        try {
            String username = config.getUsername();
            String password = config.getPassword();
            successLogin= lputility.LogIn(username, password);
            GZLogger.ActivityLog("Logged into the application");
        } catch (Exception ex) {
            GZLogger.ErrorLog(ex, "Not able to launch browser");
        }
        return successLogin;
    }
    public boolean LogInIntoJira() {
        boolean successLogin=false;
        try {
            String username = config.getJiraUsername();
            String password = config.getJiraPassword();
            successLogin= papUtil.loginJira(username,password);
            if (successLogin){
                GZLogger.ActivityLog("Logged into the application");
                return true;
            }
        } catch (Exception ex) {
            GZLogger.ErrorLog(ex, "Not able to launch browser");
        }
        return successLogin;
    }

    public boolean loginAtJiraInstance(String uname,String passwd){
        try{
            CreateJiraPage cjp=new CreateJiraPage();
            if (cjp.enterJiraUname(uname))
                if (cjp.clickLoginSubmitBtnAtJiraPage())
                    if (cjp.enterJiraPasswd(passwd))
                        if (cjp.clickLoginSubmitBtnAtJiraPage()){
                            GZLogger.ActivityLog("Logged in into JIRA");
                            if(cjp.isJiraProfileIconVisible())
                                return true;
                        }
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Not able to login into JIRA");
            return false;
        }
        return false;
    }

    public boolean manageJiraApps(String app, String pwd){
        try{
            if (papUtil.searchApp(app,pwd))
                return true;
        }catch (Exception e){
            return false;
        }
        return false;
    }

    public boolean authenticationSF(Map<String,String> map){
        return papUtil.authenticateSF(map);
    }
    public boolean loginInSF(Map<String,String> map){
        return papUtil.loginInSFForJiraPlugin(map);
    }

    public String InvalidLogInIntoSinergify(Map<String, String> map) {
        String error = "";
        try {
            String username = map.get("username");
            String password = map.get("password");
            error = lputility.InvalidLogIn(username, password);
            GZLogger.ActivityLog("Logged into the application");
        } catch (Exception ex) {
            error = "";
            GZLogger.ErrorLog(ex, "Not able to launch browser");
        }
        return error;
    }

    public boolean searchAppStore(Map<String, String> map) {
        boolean result = false;
        try {
            String appname = map.get("appname");
            result=asputility.searchAndOpenApp(appname);
            if(result){
                GZLogger.ActivityLog("The app is found in the org");
            }
        } catch (Exception e) {
            GZLogger.ActivityLog("Not able to find the app in the org");
        }
        return result;
    }




    public void navigateAdminSetting(){
        asputility.openAdminSettings();
    }

    public boolean addBasicAuthJiraCloud(Map<String, String> map) {
        boolean status = false;
        try {
            status = asputility.searchAndOpenApp(map.get("appname")); /*Check if App is installed in SF org*/
            if (status) { /*If Yes, app is installed*/

                status = asputility.openAdminSettings(); /*Check if navigated to Admin Setting tab*/

                if (status) { /*if Yes,navigated to Admin Setting*/

                    status = asputility.checkIfJiraInstanceAdded(); /*Check if Jira instance is added*/

                    if (status) { /*if Yes, Jira instance is added already*/

                        status = asputility.checkIfJiraInstanceActive(map); /*Check if Jira instance is Active*/

                        if (status) { /*if Yes, Jira instance is Active*/
                            return true;
                        } else
                            return false; /*Open the instance and Active it*/

                    } else { /*If Jira instance is not added*/
                        status = asputility.setAuthentication(map);
                        if (status){
                            GZLogger.ActivityLog("Admin user is able to add a Jira cloud with Basic auth");
                            return true;
                        }else{
                            GZLogger.FailLog("Not able to add a Jira cloud with Basic auth");
                            return false;

                        }



                    }
                }else /*If not navigated to Admin Setting*/
                    return false;

            }else/*If No app is installed*/
                return false;

        } catch (Exception e) {
            GZLogger.ErrorLog(e, e.getMessage());
            return false;
        }
    }

    public boolean addOauthJiraCloud(Map<String,String> map){
        boolean status=false;
        try{
            asputility.searchAndOpenApp(map.get("appname"));
            asputility.openAdminSettings();
            status=asputility.setOauth(map);
        }catch (Exception e){
            GZLogger.ErrorLog(e,"Admin user is not able to add a Jira cloud with OAuth successfully ");
        }
        return status;
    }

    public boolean addJiraInstance(Map<String, String> map) {
        //boolean result = false;
        try {
            asputility.searchAndOpenApp(map.get("appname"));
            asputility.openAdminSettings();
            asputility.fillAddInstanceForm(map);
            return true;
        } catch (Exception e) {
            GZLogger.ErrorLog(e, "No able to save the Admin Settings");
        }
        return false;
    }

    public boolean verifyProjects() {
        boolean flag = false;
        try {
            flag=projutill.checkIfProjectsPresent();
        }catch (Exception e) {
            GZLogger.ErrorLog(e, "Error Fetching Projects List" + " \n");
        }
        if (flag)
            GZLogger.ActivityLog("There are some projects found on Projects page");
        else
            GZLogger.ActivityLog("There is no project found on the Projects page.");
        return flag;
    }

    public boolean checkDefaultProjectDropdown(){
        boolean status=false;
        try{
            status=projutill.checkDefaultProjectDropdown();
            GZLogger.PassLog("Able to see the Default project dropdown on the Projects screen");
        }catch (Exception e)
        {
            GZLogger.FailLog("Not able to see the Default project dropdown on the Projects screen");
        }
        return status;
    }

    public boolean saveProject() {
        boolean flag = false;
        try {
            verifyProjects();
            flag = projutill.setProjectPermission();
        } catch (Exception e) {
            GZLogger.ErrorLog(e, e.getMessage());
        }
        return flag;
    }



    /*Methods related to FIELD CONFIGURATION*/



    public boolean setReqField() {
        boolean flag = false;
        try {
            flag = fieldutill.mapReqField();
        } catch (Exception e) {
            GZLogger.ErrorLog(e, e.getMessage());
        }
        return flag;
    }

    public boolean setOptField(Map<String, String> map) {
        boolean flag = false;
        try {
            flag = fieldutill.mapOptFields(map);
        } catch (Exception ex) {
            GZLogger.ErrorLog(ex, ex.getMessage());
        }
        return flag;
    }

    public boolean validateAddField(){
        boolean flag=false;
        try{
           flag= valutil.addFields();
        }catch (Exception e){
            GZLogger.ErrorLog(e,e.getMessage());
        }
        return flag;
    }

    public boolean validateRemoveField(){
        boolean flag=false;
        try{
            flag=valutil.removeFields();
        }catch (Exception e){
            GZLogger.ErrorLog(e,e.getMessage());
        }
        return flag;
    }

    public boolean makeNewRule(){
        boolean status=false;
        try{
            status=ruleutil.addNewRule();
        }catch (Exception e){
            GZLogger.ErrorLog(e,e.getMessage());
        }
        return status;
    }
    public boolean openOauthTab(){
        boolean status=false;
        try{
            status=asputility.isOauthTabOpen();
        }catch (Exception e){
            GZLogger.ErrorLog(e,e.getMessage());
        }
        return status;
    }

        public boolean openProjects(){
        asp=new AdminSettingPage();
        boolean status=asp.projectsOptionVisible();
        if (status)
            asp.clickProjectTab();
            GZLogger.ActivityLog("Admin user clicks on Projects and see projects in the Grid");
        return status;
    }

    public boolean searchProjects() throws InterruptedException {
        return projutill.checkSearchProject();
    }

    public  boolean isJiraInstanceActive(){
        boolean status=false;
        try{
            status=asp.checkIfInstanceActive();

        }catch (Exception e){
            GZLogger.ErrorLog(e,"Instance seems not configured properly");
        }
        return status;
    }

    public boolean refreshAllProjects(){
        try{
            projutill.refreshProjects();
        }catch (Exception e){
            return false;
        }
        return true;
    }
    public boolean changePageNumber(){
        try{
            projutill.selectPageNumber();
            GZLogger.ActivityLog("Admin is able to change page number");
            return true;
        }catch (Exception e){
            GZLogger.FailLog("Admin is not able to change page number");
            return false;
        }
    }

    public boolean saveSyncWriteProject(){
        boolean status=false;
        try{
            status=asputility.saveProjects();
//            status=projutill.findClickSyncCheckbox();
//            status=projutill.findClickWriteCheckbox();
//            status=projutill.findDefaultIssueType();
//            status=projutill.findIssueTypeToSync();
            return status;
        }catch (Exception e){
            GZLogger.ActivityLog("Admin is not able to save a project with Sync & Write permission");
            return false;
        }
    }

    public boolean addSharingSetting(){
        try{
            return projutill.setSharingSetting();
        }catch (Exception e){
            return false;
        }
    }
    public boolean addWriteSharingSetting(){
        boolean status=false;
        try{
            status= projutill.setWriteSharingSetting();
            if(status){
                GZLogger.PassLog("Admin is able to add a user successfully for Project sharing setting with Read/Write permission");
                return status;
            }else
                return status;
        }catch (Exception e){
            GZLogger.FailLog("Admin is not able to add a user successfully for Project sharing setting with Read/Write permission");
            return false;
        }
    }
    public boolean editSharingSetting(){
        try{
            return projutill.editSharingSetting();
        }catch (Exception e){
            return false;
        }
    }


    /*Methods Related to Field Configuration*/

    public boolean navigateToFieldConfigurationPage(){
        try{
            return fieldutill.checkIfFieldConfigPageIsVisible();
        }catch (Exception e){
            GZLogger.FailLog("Field configuration page seems not found");
            return false;
        }
    }

    public boolean saveDefaultValues(){
        return fieldutill.setDefaultValue();
    }


    public boolean searchMenuItem(Map<String ,String > map){
        asp = new AdminSettingPage();
        cjp= new CreateJiraPage();
        try{
            if(asp.clickAppLauncher())
                if(asp.setAppName(map.get("itemName")))
                    if(cjp.clickMenuItem())
                        return true;
        }catch (Exception e){
            GZLogger.FailLog("Not able to searchMenuItem : App Launcher");
            return false;
        }
        return false;
    }

    public boolean searchOpenCase(Map<String,String> map){
        try{
            String  caseNum= map.get("caseNumber");
            if(cjp.enterAndSearchCase(caseNum))
                if(cjpUtil.openCaseNumber(map))
                    return true;
        }catch (Exception e){
            return false;
        }
        return false;
    }

    public boolean checkIfCreateJiraBtnVisible(){
        try{
            if(cjpUtil.createJiraBtnVisible()){
                return true;
            }
        }catch (Exception e){
            return false;
        }
        return false;
    }

    public boolean openCreateJiraForm() {
        try {
            if (cjpUtil.checkUICreateJiraForm())
                return true;
            return false;
        } catch (Exception e) {
            return false;
        }
    }
    public boolean matchPotentialResults(Map<String,String> map){
        try{
            if (cjpUtil.potentialResults(map))
                return true;
            return false;
        }catch (Exception e){
            return false;
        }
    }

    public boolean closeJiraForm() {
        try {
            if (cjpUtil.checkUIAndCloseJiraForm())
                    return true;
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean createJiraIssue(Map<String,String> map){
        if (cjpUtil.fillCreateJiraForm(map))
            if(cjpUtil.enterSaveBtn())
                return true;
            return false;
    }

    public boolean createSubtask(Map<String,String> map){
        try{
            if(cjpUtil.openJiraTask(map))
                if(cjpUtil.fillSubtaskForm(map))
                    if(cjpUtil.enterSaveBtn())
                        return true;
        }catch (Exception e){
            return false;
        }
        return false;
    }

    public boolean editJiraIssue(Map<String,String> map){
                if (cjpUtil.editJira(map))
                    return true;
                return false;
    }

    public boolean openJiraRelatedList(Map<String,String> map){
        SearchJiraPage sjp=new SearchJiraPage();
        try{
            if (cjpUtil.captureJiraNumberFromNavBar()!=null)
                if(searchMenuItem(map))
                    if (searchOpenCase(map))
                        if(sjp.clickJiraIssueTab())
                            if (cjpUtil.verifyCapturedJiraKeyInRelatedList()){
                                GZLogger.PassLog("The Jira is found in the Related list");
                                return true;
                            }
        }catch (Exception e){
            GZLogger.FailLog("The Jira was not found in the Related list");
            return false;
        }
            return false;
    }




    public boolean createJiraIssueWithComment(Map<String,String> map){
        if (cjpUtil.fillCreateJiraForm(map))
            if(cjpUtil.saveCommentAtCreateJira(map))
                return true;
        return false;
    }

    public boolean createJiraIssueWithFile(Map<String,String> map){
        if (cjpUtil.fillCreateJiraForm(map))
            if(cjpUtil.saveFileAtCreateJira(map))
                return true;
        return false;
    }



    public boolean verifyElementsOnAddCommentModal(Map<String,String> map) {
        if (cjpUtil.fillCreateJiraForm(map))
            if (cjpUtil.verifyUIOnAddCommentModal())
                return true;
        return false;
    }

    public boolean searchJira(String jiraKey){
        try{
            if(sjpUtil.searchJiraIssueKey(jiraKey)){
                GZLogger.PassLog("Able to search the entered Jira"+" "+jiraKey+" "+"successfully");
                return true;
            }else {
                GZLogger.FailLog("Not able to search the entered Jira"+" "+jiraKey+" "+"successfully");
                return false;
            }
        }catch (Exception e){
            return false;
        }
    }

    public boolean openSearchJira(){
        try{
            if(sjpUtil.openSearchJiraPage()){
                GZLogger.PassLog("Search Jira page is opened successfully");
                return true;
            }else {
                GZLogger.FailLog("Search Jira page didn't open successfully");
                return false;
            }
        }catch (Exception e){
            return false;
        }
    }
    public boolean openSearchJiraAndVerifyInstance(String instanceName){
        if (sjpUtil.matchJiraInstanceValue(instanceName)){
            GZLogger.PassLog("Jira instance is available on the Search Jira page");
            return true;
        }else {
            GZLogger.FailLog("Jira instance is not available on Search Jira page");
            return false;
        }
    }

    public boolean lookForProject(String project){
        try{
            if (sjpUtil.openSearchJiraPage())
                if (sjpUtil.checkProjectName(project))
                    return true;
                return false;
        }catch (Exception e){
            GZLogger.FailLog("Not able to find the same project name");
            return false;
        }
    }

    public boolean linkJiraIssue(Map<String,String> map){
        try{
            if(cjpUtil.findAndLinkIssue(map)!=null) {
                    return true;
            }
        }catch (Exception e){
            GZLogger.FailLog("Not able to link the Jira");
            return false;
        }
        return false;
    }

    public boolean unlinkJiraIssue(Map<String,String> map){
        try{
            if(cjpUtil.unlinkJiraFromRelatedList()){
                String jiraNum1=cjp.getUnlinkedJiraNumber();
                /*This method need to be renamed as getUnlinkedJiraNumber()*/
                if (cjpUtil.verifyUnlinkedJiraFromRelatedList(jiraNum1)){
                    GZLogger.PassLog("Jira is unlinked successfully");
                    return true;
                }
            }
        }catch (Exception e){
            GZLogger.FailLog("Jira was not unlinked successfully");
            return false;
        }
        return false;
    }

    public boolean updateJiraStatusTransition(Map<String,String> map){
        try{
            if (cjpUtil.openJiraIssueFromRelatedList())
                if (cjpUtil.changeJiraStatusTransition()){
                    GZLogger.PassLog("Able to change the Jira status successfully");
                    return true;
                }
        }catch (Exception e){
            GZLogger.FailLog("Not able to update the Jira transition status");
            return false;
        }
        return false;
    }

    public boolean createJiraAndVerifyErrorMsg(String err){
        try{
            if (cjpUtil.openCreateJiraFormAndVerifyErrMsg(err)){
                GZLogger.PassLog("One-One SF Jira relation is working fine at Create Jira");
                return true;
            }
        }catch (Exception e){
            GZLogger.FailLog("Either the SF-Jira relation config setting is not update or Jira is already not linked with the case");
            return false;
        }
        return false;
    }

    public boolean linkJiraAndVerifyErrorMsg(String key){
        try{
                 if (sjpUtil.searchJiraIssueKey(key))
                     if (sjpUtil.linkAtSearchJira()){
                         GZLogger.PassLog("One-One SF Jira relation is working fine at Link Jira");
                         return true;
                     }
        }catch (Exception e){
            GZLogger.FailLog("Either the SF-Jira relation config setting is not updated or a Jira is already not linked with the case");
            return false;
        }
        return false;
    }

    public boolean searchAndLinkJira(String key){
        try{
            if (sjpUtil.searchJiraIssueKey(key))
                if (sjpUtil.linkJiraIssue())
                    if (cjpUtil.openRelatedListAndVerifyJiraKey(key))
                    return true;
        }catch (Exception e){
            GZLogger.FailLog("Unable to link the the Jira key:"+" "+key+" ");
            return false;
        }
        return false;
    }

    public boolean setEditJiraEnableInConfigurationSettings(Map<String,String> map){
        try{
            if (cspUtil.saveEnableEditJiraToggleAsEnabled())
                if (searchMenuItem(map))
                    if (searchOpenCase(map))
                        if (openCreateJiraForm())
                            if (createJiraIssue(map))
                                if (cspUtil.verifyEditButton())
                                return true;
        }catch (Exception e){
            return false;
        }
        return false;
    }

    public boolean setEditJiraDisableInConfigurationSettings(Map<String,String> map){
        try{
            if (cspUtil.saveDisableEditJiraToggleAsEnabled())
                if (searchMenuItem(map))
                    if (searchOpenCase(map))
                        if (openCreateJiraForm())
                            if (createJiraIssue(map))
                                if (!cspUtil.verifyEditButton())
                                    return true;
        }catch (Exception e){
            return false;
        }
        return false;
    }



    public boolean setJiraTransitionEnable(Map<String,String> map){
        try{
            if (cspUtil.enableJiraTransitionAndSave())
                if (searchMenuItem(map))
                    if (searchOpenCase(map))
                        if (openCreateJiraForm())
                            if (createJiraIssue(map))
                                if (cspUtil.verifyJiraTransitionButton())
                                    return true;
        }catch (Exception e){
            return false;
        }
        return false;
    }

    public boolean setJiraTransitionDisable(Map<String,String> map){
        try{
            if (cspUtil.disableJiraTransitionAndSave())
                if (searchMenuItem(map))
                    if (searchOpenCase(map))
                        if (openCreateJiraForm())
                            if (createJiraIssue(map))
                                if (!cspUtil.verifyJiraTransitionButton())
                                    return true;
        }catch (Exception e){
            return false;
        }
        return false;
    }

    public boolean cloneJiraIssue(){
        try {
            if (sjpUtil.viewAndCloneJiraIssue())
                return true;
          }catch (Exception e){
            return false;
        }
        return false;

    }

    public boolean viewJiraInJiraInstance(Map<String,String> map){
        try{
            if(cjpUtil.openViewInJira())
                if (switchToJiraAndLogin())
                    if (loginAtJiraInstance(map.get("jiraUname"),map.get("jiraPasswd")))
                            return true;
        }catch (Exception e){
            return false;
        }
        return false;
    }

    public boolean switchToJiraAndLogin(){
        try {
            String currentHandle=GZBrowser.getDriver().getWindowHandle();
            GZLogger.ActivityLog("The current window is:"+" "+currentHandle);
            String title=GZBrowser.getDriver().getTitle();
            GZLogger.ActivityLog("The current window title is:"+" "+title);
            Set<String> handles=GZBrowser.getDriver().getWindowHandles();
            for (String h:handles){
                if (!h.equals(currentHandle)){
                    GZBrowser.getDriver().switchTo().window(h);
                    GZLogger.ActivityLog("Switched to the next Window");
                    String newTitle=GZBrowser.getDriver().getTitle();
                    GZLogger.ActivityLog("The  window title is:"+" "+newTitle);
                    return true;
                }
            }
        }catch (Exception e){
            GZLogger.ActivityLog("Not able to switch and login into JIRA");
            return false;
        }
        return false;
    }

    public boolean verifyCaseCommentOnJira(Map<String,String> map){
        try {
            if (cjpUtil.saveCaseCommentInSalesforce(map.get("expectedComment")))
                if (viewJiraInJiraInstance(map))
                    if (cjpUtil.findCaseCommentOnJiraSide(map.get("expectedComment")))
                        return true;
        }catch (Exception e){
            return false;
        }
        return false;
    }

    public boolean saveJiraCommentAtSf(Map<String,String> map){
        try{
            if (cjpUtil.openJiraIssue(map))
                if (cjpUtil.saveJiraIssueCommentAtSFSide(map.get("jiraComment")))
                    return true;
        }catch (Exception e){
            return false;
        }
        return false;
    }

    public boolean addCommentOnJIRA(String comment, String jiraKey){
        try{
            if (cjpUtil.commentOnJiraIssueAtJiraPage(comment,jiraKey))
                return true;
        }catch (Exception e){

            return false;
        }
        return false;
    }


}
