package CommonUtilities.GZBrowser;

import CommonUtilities.GZLogger.GZLogger;
import CommonUtilities.DataProvider.ReadConfig;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.time.Duration;
import java.util.concurrent.TimeUnit;


public class GZBrowser {
    private static WebDriver driver;
//    ReadConfig config = new ReadConfig();

    /**
     * Use this method only after Launch
     * */
    public static WebDriver getDriver(){
        return driver;
    }

    public void Launch(String baseURL, BrowserType type){
        try{
//            ChromeOptions chromeOptions = new ChromeOptions();
//            chromeOptions.setPageLoadStrategy(PageLoadStrategy.NORMAL);
//            WebDriver driver = new ChromeDriver(chromeOptions);
//            chromeOptions.addArguments("--headless", "--disable-gpu","--ignore-certificate-errors");
            GZLogger.ActivityLog("Launching browser of type "+ type);
            switch(type){
                case Firefox:
//                    System.setProperty("webdriver.gecko.driver", config.getFirefoxDriver());
//                    driver = new FirefoxDriver();
                    WebDriverManager.firefoxdriver().setup();
                    driver = new FirefoxDriver();
                    //driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
                    break;
                default:
                    ChromeOptions options = new ChromeOptions();
                    options.addArguments("--no-sandbox");
                    options.addArguments("start-maximized");
                    options.addArguments("--disable-infobars");
                    options.addArguments("--disable-extensions");
                    options.addArguments("chrome.switches", "--disable-extensions");
                    options.addArguments("--disable-gpu");
                    options.addArguments("--disable-dev-shm-usage");
                    options.addArguments("--disable-notifications");
                    options.addArguments("--disable-popup-blocking");
                    //options.addArguments("enable-automation");
                    //options.setExperimentalOption("excludeSwitches", new String[]{"enable-automation"});
                    //set up the chromedriver using WebDriverManager
                    WebDriverManager.chromedriver().setup();
                    //options.setExperimentalOption("useAutomationExtension", false);
                    options.addArguments("--disable-blink-features=AutomationControlled");
                    options.setHeadless(true);
                    //Create driver object for Chrome
                    driver = new ChromeDriver(options);
            }
            GZLogger.ActivityLog(type + " Driver Initiated");
            if(!baseURL.equals("")){
                driver.get(baseURL);
                driver.manage().window().maximize();
                GZLogger.ActivityLog("Navigated to Url ( " + baseURL + ")");
            }
        }catch(Exception ex){
            GZLogger.ErrorLog(ex, "Error Launching browser - " + ex.getMessage());
        }
    }

    public void Close(){
        try{
            if (driver !=null) {
                driver.quit();
            }
        } catch( Exception ex){
            GZLogger.ErrorLog(ex,"Error closing driver " + ex.getMessage());
        }
    }

}
