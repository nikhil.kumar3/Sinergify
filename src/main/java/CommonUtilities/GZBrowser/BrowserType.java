package CommonUtilities.GZBrowser;

public enum BrowserType {
        Chrome,
        Firefox,
        Edge,
        InternetExplorer,
        Safari
}
