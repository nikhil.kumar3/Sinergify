package CommonUtilities.GZCommonFuncs;

import org.apache.commons.lang3.RandomStringUtils;

public class GZCommonFuncs {

    public String randomAlphaNumString(int strLen){
        String generateRandString = RandomStringUtils.randomAlphanumeric(strLen);
        return generateRandString;
    }

    public String randomString(int strLen){
        String generateRandString = RandomStringUtils.randomNumeric(strLen);
        return generateRandString;
    }

    public String randomNum(int strLen){
        String generateRandString = RandomStringUtils.randomAlphabetic(strLen);
        return generateRandString;
    }

}
