package CommonUtilities.GZCommonFuncs;

import CommonUtilities.GZBrowser.GZBrowser;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import CommonUtilities.GZLogger.GZLogger;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Collections;
import java.util.List;

public class GZCommonElementFuncs {

    WebDriver driver = GZBrowser.getDriver();


    public void selectDdVal(WebElement ele, String Val) {
        try {
            Select select = new Select(ele);
            select.selectByVisibleText(Val);
            GZLogger.ActivityLog("Dropdown value selected " + Val);
        } catch (Exception e) {
            GZLogger.ErrorLog(e, e.getMessage());
        }
    }
    public void selectOptionFromDropdown(WebElement element, String enterOptionName) {
//        for (WebElement ele : element) {
//            //System.out.println(ele.getText());
//            String option = ele.getText();
//            if (option.contains(enterOptionName)) {
//                System.out.println(ele.getText());
//                ele.click();
//                break;
//            }
//        }

    }

    public void selectDdValByIndex(WebElement ele, int index) {
        try {
            Select select = new Select(ele);
            select.selectByIndex(index);
            GZLogger.ActivityLog("Dropdown value selected at position--" + index);
        } catch (Exception e) {
            GZLogger.ErrorLog(e, e.getMessage());
        }
    }

    public void GZWait(int TimeOut) throws InterruptedException {
        if(TimeOut == 0){
            Thread.sleep(3000);
        }
        else{
            Thread.sleep(TimeOut);
        }
    }

    public void GZWait(int type, WebElement ele, int timeout) {
        switch(type){
            case 1:
                new WebDriverWait(driver, timeout).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(ele));
                break;
            case 2:
                new WebDriverWait(driver, timeout).until(ExpectedConditions.elementToBeClickable(ele));
                break;
            case 3:
                new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOf(ele));
                break;
            case 4:
                new WebDriverWait(driver,timeout).until(ExpectedConditions.invisibilityOf(ele));
                break;
            case 5:
                new WebDriverWait(driver,timeout).until(ExpectedConditions.textToBePresentInElement(ele,"success"));
                break;
        }  
    }

    public void waitTillPageLoads(){
        JavascriptExecutor js = (JavascriptExecutor)driver;
        js.executeScript("return document.readyState").equals("complete");
    }

    public void GZScrollBtm(WebElement element) {
        try {
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("arguments[0].scrollIntoView();",element);
            //js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
        } catch (Exception e) {
            GZLogger.ErrorLog(e, e.getMessage());
        }
    }

    public void GZScrollTop() {
        try {
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("window.scrollTo(0,0)");
        } catch (Exception e) {
            GZLogger.ErrorLog(e, e.getMessage());
        }
    }
    public void GZScrollBottom() {
        try {
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("scroll(0,400)");

        } catch (Exception e) {
            GZLogger.ErrorLog(e, e.getMessage());
        }
    }
    public void GZScrollTop(WebElement element) {
        try {
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("arguments[0].scrollIntoView();",element);
        } catch (Exception e) {
            GZLogger.ErrorLog(e, e.getMessage());
        }
    }

    public void GZJSEleClick(WebElement element){
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("arguments[0].click();", element);
    }
    public String  getHiddenElementValue(WebElement ele){
        JavascriptExecutor js=(JavascriptExecutor) GZBrowser.getDriver();
        String text=js.executeScript("return arguments[0].value",ele).toString();
//        String[]  li=text.split(",");
//        int sizeOfIssueType=li.length;
//        System.out.println("The size of the Issue Type To Sync is" +sizeOfIssueType);
//      return jiraNameInBasicAuth.getText(); This getText() does not support for the hidden element
        return text;
    }
    public List<String> getHiddenElementValueInList(WebElement ele){
        JavascriptExecutor js=(JavascriptExecutor) GZBrowser.getDriver();
        List<String> text= Collections.singletonList(js.executeScript("return arguments[0].value", ele).toString());
//      return jiraNameInBasicAuth.getText(); This getText() does not support for the hidden element
        return (text);
    }

    public void selectValueInPicklist(WebElement picklist){
        picklist.sendKeys(Keys.ARROW_DOWN);
        picklist.sendKeys(Keys.ARROW_UP);
        picklist.sendKeys(Keys.RETURN);
    }

    public void enterKeyPress(WebElement dropdown){
        dropdown.sendKeys(Keys.RETURN);
    }

    public void clickSecondOptionInList(WebElement picklist1,WebElement picklist2){
        picklist1.sendKeys(Keys.ARROW_DOWN);
        picklist2.sendKeys(Keys.RETURN);
        picklist1.sendKeys(Keys.ARROW_DOWN);
        picklist2.sendKeys(Keys.RETURN);
    }







}