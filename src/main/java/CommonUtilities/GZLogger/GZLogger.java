package CommonUtilities.GZLogger;

import com.aventstack.extentreports.Status;
import org.apache.logging.log4j.Logger;

import java.io.PrintStream;
import java.util.Arrays;

import static CommonUtilities.GZReporting.ReportingListener.extentTest;

/**
 The logger.
 */
public class GZLogger {
    private static final PrintStream objOutWriter;

    static
    {
        objOutWriter = System.out;
    }

    private static void SetOutWriter()
    {
        if (System.out != objOutWriter)
        {
            System.setOut(objOutWriter);
        }
    }

    /**
     The pass log.
     @param passMessage
     The pass message.
     */
    public static void PassLog(String passMessage)
    {
        SetOutWriter();
        System.out.println(passMessage);
        Logger logger = LoggerFactory.getPassLogger();
        logger.info(passMessage);
        reporterLog(Status.PASS,passMessage);
    }

    /**
     The fail log.
     @param failMessage
     The fail message.
     */
    public static void FailLog(String failMessage)
    {
        SetOutWriter();
        System.out.println(failMessage);
        Logger logger = LoggerFactory.getFailLogger();
        logger.error(failMessage);
        reporterLog(Status.FAIL,failMessage);
    }

    /**
     The warning log.
     @param warningMessage
     The warning message.
     */
    public static void WarningLog(String warningMessage)
    {
        SetOutWriter();
        System.out.println(warningMessage);
        Logger logger = LoggerFactory.getIterationLogger();
        logger.warn(warningMessage);
    }

    /**
     The error log.
     @param exception
     The exception.
     @param errorMessage
     The error message.
     */
    public static void ErrorLog(Exception exception, String errorMessage)
    {
        SetOutWriter();
        if (!errorMessage.equals(exception.getMessage()))
        {
            System.out.println(errorMessage);
        }
        System.out.println("Exception: " + exception.getMessage());
        System.out.println("Stack trace:: " + Arrays.toString(exception.getStackTrace()));
        Logger logger = LoggerFactory.getFailLogger();
        logger.error(errorMessage, exception);
        reporterLog(Status.WARNING,errorMessage);
    }

    /**
     The user interface activity log.
     @param activity
     The user interface activity.
     */
    public static void ActivityLog(String activity)
    {
        SetOutWriter();
//        System.out.println(activity);
        Logger logger = LoggerFactory.getActivityLogger();
        logger.info(activity);
        reporterLog(Status.INFO,activity);
    }

    /**
     The log.
     @param genericLogging
     The generic logging.
     */
    public static void Log(String genericLogging)
    {
        SetOutWriter();
//        System.out.println(genericLogging);
        Logger logger = LoggerFactory.getIterationLogger();
        logger.info(genericLogging);
        reporterLog(Status.INFO, genericLogging);
    }

    public static void reporterLog(Status status, String message){
        if(extentTest != null){
            extentTest.log(status, message);
        }
    }
}
