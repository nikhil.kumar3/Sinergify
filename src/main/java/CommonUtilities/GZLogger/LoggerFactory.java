package CommonUtilities.GZLogger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 The logger factory.
 */
public class LoggerFactory {
    /**
     Gets the pass logger.
     */
    public static Logger getPassLogger()
    {
        return LogManager.getLogger("Pass");
    }

    /**
     Gets the fail logger.
     */
    public static Logger getFailLogger()
    {
        return LogManager.getLogger("Fail");
    }

    /**
     Gets the iteration logger.
     */
    public static Logger getIterationLogger()
    {
        return LogManager.getLogger("Iteration");
    }

    /**
     Gets the User interface activity logger.
     */
    public static Logger getActivityLogger()
    {
        return LogManager.getLogger("Activity");
    }

    /**
     Gets the automation logger.
     */
    public static Logger getAutomationLogger()
    {
        return LogManager.getLogger("Automation");
    }

}
