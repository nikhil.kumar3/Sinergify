package CommonUtilities.GZAssertion;

import CommonUtilities.GZBrowser.GZBrowser;
import CommonUtilities.GZLogger.GZLogger;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import java.io.File;
import java.io.IOException;

public class TestListener implements ITestListener {
    @Override
    public void onTestFailure(ITestResult result) {
        GZLogger.FailLog("***** Error " + result.getName() + " test has failed *****");
        String methodName=result.getName().trim();
        WebDriver driver = GZBrowser.getDriver();
        try {
            captureScreenshot(driver, methodName);
        } catch (IOException e) {
            GZLogger.ErrorLog(e,e.getMessage());
        }
    }

    public void captureScreenshot(WebDriver driver, String tname) throws IOException {
        TakesScreenshot ts = (TakesScreenshot) driver;
        File source = ts.getScreenshotAs(OutputType.FILE);
        File target = new File("./AutomationReport/Screenshots/"+ tname + ".png");
        FileUtils.copyFile(source, target);
    }

    public void onFinish(ITestContext context) { }

    public void onTestStart(ITestResult result) { }

    public void onTestSuccess(ITestResult result) { }

    public void onTestSkipped(ITestResult result) {   }

    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {   }

    public void onStart(ITestContext context) {   }

}
