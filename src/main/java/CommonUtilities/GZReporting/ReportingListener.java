package CommonUtilities.GZReporting;

import CommonUtilities.GZLogger.GZLogger;
import com.aventstack.extentreports.*;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.aventstack.extentreports.reporter.configuration.ViewName;
import org.apache.commons.lang3.StringUtils;
import org.testng.*;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;


public class ReportingListener extends TestListenerAdapter {
    public static ExtentTest extentTest;
    public static ExtentSparkReporter sparkReporter;
    public static ExtentReports extentReport;
    String timeStamp = new SimpleDateFormat("yyy.MM.dd.HH.mm.ss").format(new Date());
    String repName = "Test-Report-"+timeStamp+".html";
//    String repName = "Test-Report.html";
    public String reportPath = System.getProperty("user.dir") + "/AutomationReport/"+repName;

    public void onStart(ITestContext testContext){
        try{
            deleteFilesAndFolders(System.getProperty("user.dir") + "/AutomationReport/");
            sparkReporter = new ExtentSparkReporter(reportPath)
                    .viewConfigurer()
                    .viewOrder()
                    .as(new ViewName[] { ViewName.DASHBOARD, ViewName.TEST })
                    .apply();
            sparkReporter.loadXMLConfig("./src/main/java/CommonUtilities/GZReporting/extent-config.xml");
            sparkReporter.config().setDocumentTitle("Sinergify Automation");
            sparkReporter.config().setReportName("Automation Execution Report");
            sparkReporter.config().setTheme(Theme.STANDARD);
            sparkReporter.config().setTimelineEnabled(true);

            extentReport = new ExtentReports();
            extentReport.attachReporter(sparkReporter);
            extentReport.setSystemInfo("Application Name", "Sinergify v3.0");
            extentReport.setSystemInfo("Host name", "localhost");
            extentReport.setSystemInfo("Environment", "Developer box");
        }catch(Exception e) {
            GZLogger.FailLog(Arrays.toString(e.getStackTrace()));
        }
    }

    public void onTestSuccess(ITestResult tr){
//        extentTest = extentReport.createTest(tr.getInstanceName());
//        ExtentTest node = extentTest.createNode(tr.getName());
        extentTest.log(Status.PASS, MarkupHelper.createLabel(tr.getName(), ExtentColor.GREEN));
        setLogFile();
//        GZLogger.PassLog("Test Case ["+ tr.getName()+ "] Passed successfully");
    }

    public void onTestFailure(ITestResult tr){
//        extentTest = extentReport.createTest(tr.getName());
        setLogFile();
        extentTest.log(Status.FAIL, tr.getThrowable().getMessage());//MarkupHelper.createLabel(tr.getName(), ExtentColor.RED));
        String screenshotPath = "./Screenshots/"+ tr.getName() + ".png";
        try{
            extentTest.fail("Screenshot is below: " + extentTest.addScreenCaptureFromPath(screenshotPath));
        }catch(Exception e) {
            GZLogger.FailLog(Arrays.toString(e.getStackTrace()));
        }
        GZLogger.FailLog(tr.getThrowable().toString());
    }

    public void onTestSkipped(ITestResult tr){
        extentTest = extentReport.createTest(tr.getName());
        extentTest.log(Status.SKIP, MarkupHelper.createLabel(tr.getName(), ExtentColor.ORANGE));
    }

    public void onFinish(ITestContext testContext){
        extentReport.flush();
    }

    public void onTestStart(ITestResult tr){
        String classname = StringUtils.substring(tr.getInstanceName(),tr.getInstanceName().lastIndexOf("."),tr.getInstanceName().length()).replace(".","");
        extentTest = extentReport.createTest(classname + "-" + tr.getName());
        System.setProperty("logFilename", tr.getName());
        GZLogger.Log("["+ classname +"--"+tr.getName()+ "] Started");
    }

    public static void setLogFile(){
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        String dateapdr = dateFormat.format(date);
        String logfile = "./Logs/TestExecution-"+ dateapdr + ".log";
        String str = "Execution Log : <a href= '"+ logfile +"'target='_blank'>Open Log File</a>";
        extentTest.log(Status.INFO,str);
    }

    private void deleteFilesAndFolders(String folderPath){
        File index = new File(folderPath);
        if (index.exists()){
            File[] entries = index.listFiles();
            if(entries.length>0){
                for(File s: entries){
                    if(s.isDirectory()){
                        deleteFilesAndFolders(s.getAbsolutePath());
                    }else{
                        s.delete();
                    }
                }
            }
        }
    }
}
