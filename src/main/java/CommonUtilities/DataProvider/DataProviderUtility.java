package CommonUtilities.DataProvider;

import CommonUtilities.GZLogger.GZLogger;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class DataProviderUtility {
//    @DataProvider(name = "testdataconfig")
//    public Object[][] provideMap(Method testcase) {
//        Map<String, String> map = readMap(testcase.getName());
//        return new Object[][] { { map } };
//    }

    public Map<String, String> readMap(String testcase) {
        Properties prop = new Properties();
        InputStream input = null;
        Map<String, String> map = new HashMap<String, String>();

        try {
            input = new FileInputStream("./src/test/java/GZProducts/Sinergify/UITest/TestData/"+ testcase + ".properties");

            prop.load(input);

            Set<String> keys = prop.stringPropertyNames();
            for (String key : keys) {
                map.put(key, prop.getProperty(key));
            }
        } catch (Exception e) {
            GZLogger.ErrorLog(e, e.getMessage());
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    GZLogger.ErrorLog(e, e.getMessage());
                }
            }
        }
        return map;
    }
}