package CommonUtilities.DataProvider;

import CommonUtilities.GZLogger.GZLogger;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class ReadConfig {
    Properties pro;

    public ReadConfig(){
        File src = new File("./src/test/java/GZProducts/Sinergify/UITest/Configuration/Config.properties");
        try{
            FileInputStream fis = new FileInputStream(src);
            pro = new Properties();
            pro.load(fis);
        }catch(Exception e){
            GZLogger.ErrorLog(e,"Exception loading properties file" + e.getMessage());
        }
    }

    public String getApplicationPath(){
        String url = pro.getProperty("baseURL");
        return url;
    }
    public String getJiraApplicationPath(){
        String url = pro.getProperty("baseJiraURL");
        return url;
    }

    public String getUsername(){
        String uname = pro.getProperty("username");
        return uname;
    }

    public String getPassword(){
        String pwd = pro.getProperty("password");
        return pwd;
    }

    public String getJiraUsername(){
        String uname = pro.getProperty("usernameJira");
        return uname;
    }

    public String getJiraPassword(){
        String pwd = pro.getProperty("passwordJira");
        return pwd;
    }

//    public String getChromeDriver(){
//        String chromepath = pro.getProperty("chromepath");
//        return chromepath;
//    }
//
//    public String getFirefoxDriver(){
//        String firefoxpath = pro.getProperty("firefoxpath");
//        return firefoxpath;
//    }

    /*We are using WebDriverManager instead of these two methods*/

}
